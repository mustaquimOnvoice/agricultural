function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
var redirectURL="district_head";
var updated_redirect=base_url+'department/district_head';
	 $("#from_add_district_admin").on("submit", function (e) {
        e.preventDefault();
        $('.add_district_firstn_error').html('');
		$('.add_district_lastn_error').html('');
		$('.add_district_error').html('');
         $('.add_district_mob_error').html('');
         $('.add_district_dob_error').html('');
         $('.add_district_email_error').html('');
         $('.add_district_address_error').html('');
          $('.add_district_designation_error').html('');
         $('.district_admin_role_error').html('');
         $('.district_admin_assembly_error').html('');
        var district_admin_first_name=$("#district_admin_first_name").val().trim();
        var district_admin_last_name=$("#district_admin_last_name").val().trim();
        var district_admin_mobile=$("#district_admin_mobile").val().trim();
        var district_admin_email=$("#district_admin_email").val().trim();
		var selected_admin_district=$("#selected_admin_district").val().trim();
        var district_admin_address=$("#district_admin_address").val().trim();
        var district_admin_dob=$('#district_admin_dob').val();
        var district_admin_designation=$("#district_admin_designation").val().trim();
            var district_admin_role=$("#district_admin_role").val().trim();
            var district_admin_assembly=$("#district_admin_assembly").val().trim();
          var currentDate = new Date();

         var year = currentDate.getFullYear(); 

           var spitted_district_admin_dob=district_admin_dob.split("-");
 
  var calculated_year= year - spitted_district_admin_dob[2];
 var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (district_admin_first_name == '') {
            $('.add_district_firstn_error').html('Please enter first name.');
            $('#district_admin_first_name').focus();
            return false;
        }
        else
			 if (district_admin_last_name == '') {
            $('.add_district_lastn_error').html('Please enter last name.');
            $('#district_admin_last_name').focus();
            return false;
        }
         else
             if (district_admin_mobile == '') {
            $('.add_district_mob_error').html('Please enter mobile number.');
            $('#district_admin_mobile').focus();
            return false;
        }
         else
             if (district_admin_mobile != '' && district_admin_mobile.length < 10) {
            $('.add_district_mob_error').html('Mobile number must have 10 digits.');
             $('#district_admin_mobile').focus();
            return false;
        }
        else
            
            if (district_admin_email == '') {
            $('.add_district_email_error').html('Please enter email address.');
             $('#district_admin_email').focus();
            return false;
        } 
        else
            if (!filter.test(district_admin_email)) {
                $('.add_district_email_error').html('Please enter valid email address.');
                 $('#district_admin_email').focus();
            return false;
        }
        else
            
            if (district_admin_address == '') {
            $('.add_district_address_error').html('Please enter address.');
             $('#district_admin_address').focus();
            return false;
        } 
        else
            
            if (district_admin_dob == '') {
            $('.add_district_dob_error').html('Please select date of birth.');
             $('#district_admin_dob').focus();
            return false;
        } 
        else
            
            if (calculated_year < 18) {
           $('.add_district_dob_error').html('Age cannot be less than 18.');
            $('#district_admin_dob').focus();
            return false;
        } 
        else
            
            if (district_admin_designation == '') {
            $('.add_district_designation_error').html('Please enter designation.');
             $('#district_admin_designation').focus();
            return false;
        } 
         else
            
            if (district_admin_role == '') {
            $('.district_admin_role_error').html('Please enter role.');
            $('#district_admin_role').focus();
            return false;
        } 
         else
            
            if (district_admin_assembly == '') {
            $('.district_admin_assembly_error').html('Please enter assembly.');
            $('#district_admin_assembly').focus();
            return false;
        } 
        else
			 if (selected_admin_district == '') {
            $('.add_district_error').html('Please select atleast one district.');
            return false;
        }
        
        
        
        $.ajax({
            url: CompleteURL('addNewDistrictAdmin'),
            method: "POST",
            data: $(this).serialize()
        }).done(function (result) {  
                result = JSON.parse(result);
           if(result['count'] == 1){
            $('#add_district_admin_btn').prop('disabled',false); 
                $('.add_district_mob_error').html('Mobile number already exist.');
                
            }
            else
                if(result['count'] == 2){
            $('#add_district_admin_btn').prop('disabled',false); 
                $('.add_district_email_error').html('Email already exist.');
                
            }
            else
            {
               $("#add_district_admin_success").show();        
               $("#add_district_admin_success").html('District admin added successfully').fadeIn(2000).delay(2000).fadeOut(2000);
                setTimeout(function () {
             window.location.assign(redirectURL); 
                }, 1300);
                $('#add_district_admin_btn').prop('disabled',true);
            }
// $("#add_district_admin_success").show();		
// 		      $("#add_district_admin_success").html('District admin added successfully').fadeIn(2000).delay(2000).fadeOut(2000);
//                 setTimeout(function () {
//                 window.location.assign(redirectURL); 
//                 }, 1300);
        });
    });
  
         $("#frm_district_admin_delete").on("submit", function (e) {
        e.preventDefault();
        var delete_district_admin_id=$('#delete_district_admin_id').val();
             $.ajax({
            url: base_url+'department/deleteDistrict',
            method: "POST",
            data: {"delete_district_admin_id":delete_district_admin_id}
        }).done(function (result) {  
          $('#delete_district_admin_modal [data-dismiss="modal"]').trigger('click');
          $("#delete_district_admin_success").show();      
              $("#delete_district_admin_success").html('District admin deleted sucessfully').fadeIn(2000).delay(2000).fadeOut(2000);
                setTimeout(function () {
                window.location.assign(updated_redirect); 
                }, 1300);

        });
       
    });
         $("#frm_edit_district_admin").on("submit", function (e) {
        e.preventDefault();
        $('.edit_district_firstn_error').html('');
        $('.edit_district_lastn_error').html('');
        $('.edit_district_error').html('');
         $('.edit_district_mob_error').html('');
         $('.edit_district_email_error').html('');
         $('.edit_district_address_error').html('');
         $('.edit_district_dob_error').html('');
          $('.edit_district_designation_error').html('');
         $('.edit_district_admin_role_error').html('');
         $('.edit_district_admin_assembly_error').html('');
        var edit_district_admin_first_name=$("#edit_district_admin_first_name").val().trim();
        var edit_district_admin_last_name=$("#edit_district_admin_last_name").val().trim();
        var edit_exist_district=$("#edit_exist_district").val().trim();
        var update_district_admin_id=$('#update_district_admin_id').val();
        var edit_district_admin_mobile=$("#edit_district_admin_mobile").val().trim();
        var edit_district_admin_email=$("#edit_district_admin_email").val().trim();
        var edit_district_admin_address=$("#edit_district_admin_address").val().trim();
         var edit_district_admin_dob=$("#edit_district_admin_dob").val().trim();
         var edit_district_admin_designation=$("#edit_district_admin_designation").val().trim();
        var edit_district_admin_role=$("#edit_district_admin_role").val().trim();
         var edit_district_admin_assembly=$("#edit_district_admin_assembly").val().trim();
         var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
         var currentDateEdit = new Date();

         var edityear = currentDateEdit.getFullYear(); 

           var spitted_edit_district_admin_dob=edit_district_admin_dob.split("-");
 
  var edit_calculated_year= edityear - spitted_edit_district_admin_dob[2];
        if (edit_district_admin_first_name == '') {
            $('.edit_district_firstn_error').html('Please enter first name.');
            $('#edit_district_admin_first_name').focus();
            return false;
        }
        else
             if (edit_district_admin_last_name == '') {
            $('.edit_district_lastn_error').html('Please enter last name.');
             $('#edit_district_admin_last_name').focus();
            return false;
        }
        else
             if (edit_district_admin_mobile == '') {
            $('.edit_district_mob_error').html('Please enter mobile number.');
            $('#edit_district_admin_mobile').focus();
            return false;
        }
        else
             if (edit_district_admin_mobile != '' && edit_district_admin_mobile.length < 10) {
            $('.edit_district_mob_error').html('Mobile number must have 10 digits.');
            $('#edit_district_admin_mobile').focus();
            return false;
        }
         else
            
            if (edit_district_admin_email == '') {
            $('.edit_district_email_error').html('Please enter email address.');
            $('#edit_district_admin_email').focus();
            return false;
        } 
        else
            if (!filter.test(edit_district_admin_email)) {
                $('.edit_district_email_error').html('Please enter valid email address.');
                $('#edit_district_admin_email').focus();
            return false;
        }
         else
            
            if (edit_district_admin_address == '') {
            $('.edit_district_address_error').html('Please enter address.');
             $('#edit_district_admin_address').focus();
            return false;
        } 
         else
            
            if (edit_district_admin_dob == '') {
            $('.edit_district_dob_error').html('Please select date of birth.');
             $('#edit_district_admin_dob').focus();
            return false;
        } 
        else
            
            if (edit_calculated_year < 18) {
           $('.edit_district_dob_error').html('Age cannot be less than 18.');
           $('#edit_district_admin_dob').focus();
            return false;
        } 
        else
            
            if (edit_district_admin_designation == '') {
            $('.edit_district_designation_error').html('Please enter designation.');
            $('#edit_district_admin_designation').focus();
            return false;
        } 
        else
            
            if (edit_district_admin_role == '') {
            $('.edit_district_admin_role_error').html('Please enter role.');
            $('#edit_district_admin_role').focus();
            return false;
        } 
        else
            
            if (edit_district_admin_assembly == '') {
            $('.edit_district_admin_assembly_error').html('Please enter assembly.');
            $('#edit_district_admin_assembly').focus();
            return false;
        } 
        else
             if (edit_exist_district == '') {
            $('.edit_district_error').html('Please select atleast one district.');
            return false;
        }
        
        
        //console.log(update_district_admin_id);
        $.ajax({
            url: base_url+'department/updateDistrict',
            method: "POST",
            data: $(this).serialize()
        }).done(function (result) {  
           // $("#edit_district_admin_success").show();     
           //    $("#edit_district_admin_success").html('District admin updated successfully').fadeIn(2000).delay(2000).fadeOut(2000);
           //      setTimeout(function () {
           //      window.location.assign(updated_redirect); 
           //      }, 1300);
                result = JSON.parse(result);
           if(result['count'] == 1){
            $('#update_district_admin_btn').prop('disabled',false); 
                $('.edit_district_mob_error').html('Mobile number already exist.');
           }
           else
            if(result['count'] == 2){
            $('#update_district_admin_btn').prop('disabled',false); 
                $('.edit_district_email_error').html('Email already exist.');
           }
           else
           {
                $("#edit_district_admin_success").show();        
               $("#edit_district_admin_success").html('District admin updated successfully').fadeIn(2000).delay(2000).fadeOut(2000);
                setTimeout(function () {
             window.location.assign(updated_redirect); 
                }, 1300);
                $('#update_district_admin_btn').prop('disabled',true);
            }
            

        });
    });
	function get_selected_district_admin(){
    var coffee = document.getElementsByName("get_district_admin[]");
var txt = "";
var i;
var count ="";
for (i = 0; i < coffee.length; i++) {
  if (coffee[i].checked) {
    txt = txt + coffee[i].value + ",";
    count++;
  }
}
if(count == ""){
   // $('#edit_btn').hide();
}else{
    // $('#edit_btn').show(); 
}
$('#selected_admin_district').val(txt);
}
    function get_selected_edit_district_admin(){
    var coffee = document.getElementsByName("get_edit_district_admin[]");
var txt = "";
var i;
var count ="";
for (i = 0; i < coffee.length; i++) {
  if (coffee[i].checked) {
    txt = txt + coffee[i].value + ",";
    count++;
  }
}
if(count == ""){
   // $('#edit_btn').hide();
}else{
    // $('#edit_btn').show(); 
}
$('#edit_exist_district').val(txt);
}
function getDistrictAdminId(id) {
    //alert('hi');
        $("#delete_district_admin_id").val(id);
        }
 function CompleteURL(url){
return url;
    }