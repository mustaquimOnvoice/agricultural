
var redirectDistrictURL="district";
var updated_district_redirect=base_url+'department/district';
	        $("#from_add_district").on("submit", function (e) {
        e.preventDefault();
        $('.add_district_name_error').html('');
        $('.add_district_marathi_error').html('');
        var add_district_name=$("#add_district_name").val().trim();
        var add_district_name_marathi=$("#add_district_name_marathi").val().trim();
        if (add_district_name == '') {
            $('.add_district_name_error').html('Please enter district name in english.');
            return false;
        }
        else
             if (add_district_name_marathi == '') {
            $('.add_district_marathi_error').html('Please enter district name in marathi.');
            return false;
        }
       
        
        
        $.ajax({
            url: CompleteURL('addNewDistrict'),
            method: "POST",
            data: $(this).serialize()
        }).done(function (result) {  
             result = JSON.parse(result);
           if(result['count'] == 0){
                $("#add_district_success").show();        
              $("#add_district_success").html('District added successfully').fadeIn(2000).delay(2000).fadeOut(2000);
                setTimeout(function () {
             window.location.assign(redirectDistrictURL); 
                }, 1300);
                $('#add_district_btn').prop('disabled',true);
            }
            else if(result['count'] == 1)
            {
               $('#add_district_btn').prop('disabled',false); 
                $('.add_district_name_error').html('District already added.');
            }
            else if(result['count'] == 2)
            {
               $('#add_district_btn').prop('disabled',false); 
                $('.add_district_marathi_error').html('District already added.');
            }

        });
    });
               $("#from_edit_district").on("submit", function (e) {
        e.preventDefault();
        $('.edit_district_name_error').html('');
        $('.edit_district_marathi_error').html('');
        var edit_district_name=$("#edit_district_name").val().trim();
        var edit_district_name_marathi=$("#edit_district_name_marathi").val().trim();
        if (edit_district_name == '') {
            $('.edit_district_name_error').html('Please enter district name in english.');
            return false;
        }
        else
            if (edit_district_name_marathi == '') {
            $('.edit_district_marathi_error').html('Please enter district name in marathi.');
            return false;
        }
       
        
        
        $.ajax({
            url: base_url+'department/updateDistrictData',
            method: "POST",
            data: $(this).serialize()
        }).done(function (result) {  
             result = JSON.parse(result);
             //alert(result['count']);
           if(result['count'] == 1){
            $('#edit_district_btn').prop('disabled',false); 
                $('.edit_district_name_error').html('District already added.');
           }
            else
                if(result['count'] == 2){
            $('#edit_district_btn').prop('disabled',false); 
                $('.edit_district_marathi_error').html('District already added.');
           }
            else
           {
                $("#edit_district_success").show();        
              $("#edit_district_success").html('District updated successfully').fadeIn(2000).delay(2000).fadeOut(2000);
                setTimeout(function () {
             window.location.assign(redirectDistrictURL); 
                }, 1300);
                $('#edit_district_btn').prop('disabled',true);
            }
            

        });
    });
                 $("#frm_district_delete").on("submit", function (e) {
        e.preventDefault();
        var delete_district_id=$('#delete_district_id').val();
             $.ajax({
            url: base_url+'department/deleteSingleDistrict',
            method: "POST",
            data: {"delete_district_id":delete_district_id}
        }).done(function (result) {  
          $('#delete_district_data_modal [data-dismiss="modal"]').trigger('click');
          $("#delete_district_admin_success").show();      
              $("#delete_district_admin_success").html('District deleted sucessfully').fadeIn(2000).delay(2000).fadeOut(2000);
                setTimeout(function () {
                window.location.assign(updated_district_redirect); 
                }, 1300);

        });
       
    });
               function getDistrictId(id) {
    //alert('hi');
        $("#delete_district_id").val(id);
        }
 function CompleteURL(url){
return url;
    }