<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class news extends Base_Controller {
	public function __construct() {
		parent::__construct ();
	}
	function index($id = null){

        // if ($_SESSION ['role'] != "Admin") {
		// 	exit ( "<script>history.back();</script>" );
		// }
		$_SESSION['active_tag']="news";

		$data ['display_contents'] = array (
			"id" => "ID",
            "title" => "Title",
            "description" => "Desciption",
            "action" => "Action"			
		);
		$data ['table_data'] =$this->Base_Models->GetAllValues ( "news_alerts" );

		foreach ( $data ['table_data'] as $key => $val ) {
			$data ['table_data'] [$key] ['action']="";
			$edit = " <button onclick='window.location=\"./add/" . $data ['table_data'] [$key] ['id'] . "\"' class='btn btn-outline-info'><i class='fa fa-edit'  data-toggle='tooltip' data-placement='top' title='Edit News'></i></button>";
			$activate = " <button onclick='$(\"#myModalActionBtn\").attr(\"value\",$(this).attr(\"data-url\")); $(\"#myModalBody\").html(\"Are you sure to activate this record?\"); $(\"#myModalLabel\").html(\"Activate Record\"); $(\"#myModalLabel\").parent().parent().css(\"background-color\",\"#fff5f5\");' class='btn btn-outline-success'  data-url='" . base_url ( "/news/activate_news/" . $data ['table_data'] [$key] ['id'] ) . "' data-target='#myModal' data-toggle='modal'><i class='fa fa-unlock-alt' data-toggle='tooltip' data-placement='top' title='Activate News' ></i></button>";
			$deactivate = " <button onclick='$(\"#myModalActionBtn\").attr(\"value\",$(this).attr(\"data-url\")); $(\"#myModalBody\").html(\"Are you sure to deactivate this record?\"); $(\"#myModalLabel\").html(\"Deactivate Record\"); $(\"#myModalLabel\").parent().parent().css(\"background-color\",\"#fff5f5\");' class='btn btn-outline-danger'  data-url='" . base_url ( "/news/deactivate_news/" . $data ['table_data'] [$key] ['id'] ) . "' data-target='#myModal' data-toggle='modal'><i class='fa fa-lock'  data-toggle='tooltip' data-placement='bottom' title='Deactivate News'></i></button>";
			$delete = " <button data-toggle='tooltip' data-placement='top' title='Delete News' onclick='$(\"#myModalActionBtn\").attr(\"value\",$(this).attr(\"data-url\")); $(\"#myModalBody\").html(\"Are you sure to delete this expert?\"); $(\"#myModalLabel\").html(\"Delete Record\"); $(\"#myModalLabel\").parent().parent().css(\"background-color\",\"#fff5f5\");' class='btn btn-outline-danger'  data-url='" . base_url ( "/news/delete_news/" . $data ['table_data'] [$key] ['id'] ) . "' data-target='#myModal' data-toggle='modal'><i class='fa fa-trash'></i></button>";
				if ($_SESSION ['role'] == "Admin") {
				$data ['table_data'] [$key] ['action'] .= $edit;
				if ($data ['table_data'] [$key] ['status'] == 1) {
					$data ['table_data'] [$key] ['action'] .= $deactivate;
				} elseif ($data ['table_data'] [$key] ['status'] == 0 || $data ['table_data'] [$key] ['status'] == 2) {
					$data ['table_data'] [$key] ['action'] .= $activate;
				}

				
			} else
				$data ['table_data'] [$key] ['action'] .= $delete;

			}

		if ($id != null)
			$this->load->view ( "common/table-view", $data );
		else
            $this->view ( "common/table-view", $data );
	}
	
	function add($id=null){
		$data= null;
		$data ['sub_title'] = "News & Alerts";
		if(isset($id)){
			 $temp=$this->Base_Models->GetAllValues ( "news_alerts", array("id" => $id ) );
			 $data=$temp[0];			 
			 $data['action']=base_url("news/accept/".$id);

		}else
			 $data['action']=base_url("news/accept/");

		$this->view("forms/news",$data);

	}
	function accept($id=null){
		
		$response ['message'] = "fail";
		$post = $_POST;
		// echo '<pre>';
		// print_r($post);
		// die();
		$data=null;
			$temp = $this->generate_random_string ( 5 ) . "-" . time ();
			$path = "uploads/events/unitglo-event-" . $temp;
			$image_folder = APPPATH . "../" . $path;

			  // if (!file_existes ( APPPATH ."../uploads/events")){
     //    			mkdir (APPPATH ."../uploads/events", 0777, true );
			  //   }

			if (isset($_FILES['image']) && $_FILES ['image'] ['error'] == 0 && $_FILES ['image'] ['size']>0 ) {
				
				list ( $a, $b ) = explode ( '.', $_FILES ['image'] ['name'] );
				//$this->imageCompress ( $_FILES ['blog_img_file'] ['tmp_name'], $image_folder . "." . $b, 80 );
				if( $this->imageCompress ( $_FILES ['image'] ['tmp_name'], $image_folder . "." . $b, 80 )==""){
					if($_FILES ['image']['type']=='image/jpeg' ||$_FILES ['image']['type'] == 'image/jpg'){
					 $up = move_uploaded_file ( $_FILES ['image'] ['tmp_name'], $image_folder . "." . $b );
					 unlink($post['image_path']);
					}
				}
				// $up = move_uploaded_file ( $_FILES ['blog_img_file'] ['tmp_name'], $image_folder . "." . $b );
				// $data ['blog_img'] = "uploads/blog_images/blog_images-" . "-" . $temp . "." . $b;
				$data ['image'] = $path . "." . $b;
			} else{
				$data ['image'] = $post['image_path'];			
			}
			//$data ['image'] = $post ['image_path'];
			$data ['title'] = $post ['title'];
			$data ['description'] = $post ['description'];
			$data ['status'] = 1;

		if(isset($id)){
			
			$temp = $this->Base_Models->UpadateValue ( "news_alerts", $data, array (
					"id" => $id 
			) );

			if ($temp != 0) {
				$response ['message'] = "done";
				$response ['url'] = "../";
			}

		}else{
			$temp=$this->Base_Models->AddValues ( "news_alerts",$data );

			//	$data_return = $this->pushNotification ( $temp, $message, 'info', $notification_id, $category_id );
			if ($temp != 0) {
				$response ['message'] = "done";
				$response ['url'] = "./";
			}

		}
		$data['action']="";
	echo json_encode ( $response );

	}
	
	function delete_news($id){

		$response ['message'] = "fail";
		if(isset($id)){
			$data['status']="3";
			$temp = $this->Base_Models->UpadateValue ( "news_alerts", $data, array (
					"id" => $id 
			) );
			if ($temp != 0) {
				$response ['message'] = "done";
				$response ['url'] = "./news";
			}
			echo json_encode ( $response );
		}

	}
	
	function deactivate_news($id){
		$response ['message'] = "fail";
		if(isset($id)){
			$data['status']="2";
			$temp = $this->Base_Models->UpadateValue ( "news_alerts", $data, array (
					"id" => $id 
			) );
			if ($temp != 0) {
				$response ['message'] = "done";
				$response ['url'] = "./news";
			}
			echo json_encode ( $response );
		}

	}

	function activate_news($id){
		$response ['message'] = "fail";
		if(isset($id)){
			$data['status']="1";
			$temp = $this->Base_Models->UpadateValue ( "news_alerts", $data, array (
					"id" => $id 
			) );
			if ($temp != 0) {
				$response ['message'] = "done";
				$response ['url'] = "./news";
			}
			echo json_encode ( $response );
		}

	}

}
?>