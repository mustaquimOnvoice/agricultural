<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class farm extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		if(!isset($_POST['user_mobile_no']) && !isset($_POST['device_token'])){
			$response ['message'] = "fail";
			$response ['result'] =  "Provide Moblie no. & Token";
			echo json_encode($response);
			die();
		}
		//check_token
		if(!empty($_POST['type']) && ($_POST['type'] == 'vendor' ||$_POST['type'] == 'Vendor' ||$_POST['type'] == 'VENDOR') ){
			$this->api_model->check_token('vendors',$_POST['user_mobile_no'],$_POST['device_token']);
		}else if(!empty($_POST['type']) && ($_POST['type'] == 'farmer' || $_POST['type'] == 'Farmer' || $_POST['type'] == 'FARMER') ){
			$this->api_model->check_token('farmer',$_POST['user_mobile_no'],$_POST['device_token']);
		}else{
			$response ['message'] = "fail";
			$response ['result'] =  "User type not found";
			echo json_encode($response);
			die();
		}
    }
	
	function farm_add(){
		$response ['message'] = "fail";
		$response ['result']="Unable to access";
		if( isset($_POST['device_token']) && 
			isset($_POST['farmer_id'])  && 
			isset($_POST['farmer_name'])  && 
			isset($_POST['rsp_name'])  && 
			isset($_POST['contact_no']) && 
			isset($_POST['alternate_no']) &&
			isset($_POST['survey_no']) &&
			isset($_POST['gat_no']) &&
			isset($_POST['pin'])         
		){
			$TableValues['farmer_id']=$_POST['farmer_id'];//otp verfy {user_id}
			$TableValues['farmer_name']=$_POST['farmer_name'];//farm {id}
			$TableValues['rsp_name']=$_POST['rsp_name'];
			$TableValues['contact_no']=$_POST['contact_no'];
			$TableValues['alternate_no']=$_POST['alternate_no'];
			$TableValues['survey_no']=$_POST['survey_no'];
			$TableValues['gat_no']=$_POST['gat_no'];
			$TableValues['pin']=$_POST['pin'];
			$TableValues['user_mobile_no']=$_POST['user_mobile_no'];
			$TableValues['device_token']=$_POST['device_token'];
			
			$id= $this->Base_Models->AddValues ( "farm", $TableValues );
			
			foreach ($_FILES as $key => $value) {
				$imgresponse = $this->uploadImageFile($value,$id );            
			}
						
			$response ['message'] = "done";
			$response ['result']="farm addded successfully";
		}
		// log_message('error', 'img  file: '.print_r($_POST,true));
		   echo json_encode($response);
    }
	
	function uploadImageFile($file,$user_id,$type='5') {
        $response ['message'] = "fail";
        if (isset ( $user_id) ) {
            if (isset ( $file ) && $file ['error'] == 0) {
                if (! file_exists ( APPPATH . "../uploads/farm/" . $user_id )) {
                    mkdir ( APPPATH . "../uploads/farm/" . $user_id, 0777, true );
                }
                    log_message('error', 'img  file: '.print_r($file,true));
                $temp = "uploads/farm/" . $user_id . "/images_unitglo_mobile-" . $this->generate_random_string ( 10 );
                if ($temp != "") {
                    $image_folder = APPPATH . "../" . $temp;
                    list ( $a, $b ) = explode ( '.', $file ['name'] );
                    $result = $this->imageCompress ( $file ['tmp_name'], $image_folder . "." . $b, 80 );
                    if ($result != '') {
                        $response ['message'] = "done";
                        $response ['image_url'] = base_url ( $temp . "." . $b );

                        $TableValues ['ref_id'] = $user_id;
                        $TableValues ['type'] = $type;
                        $TableValues ['image_url'] = $response ['image_url'];

                        $response ['upload_id'] = $this->Base_Models->AddValues ( "images", $TableValues );
                    }
                }
            }
        }
        //log_message('error', 'img : '.print_r($response,true));
        return  $response ;
    }
	
    function farm_list(){
        $response ['message'] = "fail";
        $response ['result'] =  "Unable to access";

        $data = null;
        if(isset($_POST['user_mobile_no']) && isset($_POST['device_token'])){
          if(isset($_POST['id'])){
                $data= $this->Base_Models->GetAllValues ( "farm" , array('id' =>$_POST['id']));										
				foreach ($data as $key => $value) {
						$images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"5"),'image_url');
						$data[$key]["images"]=$images;
					}
		  }else{
			  //pagination
					$r = $this->Base_Models->CustomeQuary("SELECT COUNT('id') as cnt FROM farm");
	
					$numrows = $r[0]['cnt'];
					// number of rows to show per page
					$rowsperpage = 20;
					 
					// find out total pages
					$totalpages = ceil($numrows / $rowsperpage);
					 
					// get the current page or set a default
					if (isset($_POST['currentpage']) && is_numeric($_POST['currentpage'])) {
						$currentpage = (int) $_POST['currentpage'];
					} else {
						$currentpage = 1;  // default page number
					}
					 
					// if current page is less than first page
					if ($currentpage < 1) {
						// set current page to first page
						$currentpage = 1;
					}
					 
					// the offset of the list, based on current page
					$offset = ($currentpage - 1) * $rowsperpage;
				//pagination end
				
					// if current page is greater than total pages
					if ($currentpage > $totalpages) {
						// set current page to last page
						// $currentpage = $totalpages;
						$data = array();
					}else{
						$data = $this->Base_Models->CustomeQuary("SELECT * FROM farm Where status = '1' ORDER BY id DESC LIMIT $offset, $rowsperpage");
										
						foreach ($data as $key => $value) {
								$images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"5"),'image_url');
								$data[$key]["images"]=$images;
							}
					}
            }
        $response ['data'] = $data;
        $response ['message'] = "done";
        $response ['result'] =  "farm List";        
        }
        echo json_encode($response);
    }

}
?>