<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class cropcat extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		if(!isset($_POST['user_mobile_no']) && !isset($_POST['device_token'])){
			$response ['message'] = "fail";
			$response ['result'] =  "Provide Moblie no. & Token";
			echo json_encode($response);
			die();
		}
		//check_token
		if(!empty($_POST['type']) && ($_POST['type'] == 'vendor' ||$_POST['type'] == 'Vendor' ||$_POST['type'] == 'VENDOR') ){
			$this->api_model->check_token('vendors',$_POST['user_mobile_no'],$_POST['device_token']);
		}else if(!empty($_POST['type']) && ($_POST['type'] == 'farmer' || $_POST['type'] == 'Farmer' || $_POST['type'] == 'FARMER') ){
			$this->api_model->check_token('farmer',$_POST['user_mobile_no'],$_POST['device_token']);
		}else{
			$response ['message'] = "fail";
			$response ['result'] =  "User type not found";
			echo json_encode($response);
			die();
		}
    }
    function cropcat_list(){
        $response ['message'] = "fail";
        $response ['result'] =  "Unable to access";

        $data = null;
        if(isset($_POST['user_mobile_no']) && isset($_POST['device_token'])){
          if(isset($_POST['id'])){
                $data= $this->Base_Models->GetAllValues ( "crop_cat" , array('id' =>$_POST['id']),"*, (SELECT image_url FROM  `images` where ref_id=crop_cat.id AND type = '3' ORDER BY id DESC LIMIT 1) as image_url" );
          }else{
			  //pagination
					$r = $this->Base_Models->CustomeQuary("SELECT COUNT('id') as cnt FROM crop_cat");
	
					$numrows = $r[0]['cnt'];
					// number of rows to show per page
					$rowsperpage = 20;
					 
					// find out total pages
					$totalpages = ceil($numrows / $rowsperpage);
					 
					// get the current page or set a default
					if (isset($_POST['currentpage']) && is_numeric($_POST['currentpage'])) {
						$currentpage = (int) $_POST['currentpage'];
					} else {
						$currentpage = 1;  // default page number
					}
					 
					// if current page is less than first page
					if ($currentpage < 1) {
						// set current page to first page
						$currentpage = 1;
					}
					 
					// the offset of the list, based on current page
					$offset = ($currentpage - 1) * $rowsperpage;
				//pagination end
				
					// if current page is greater than total pages
					if ($currentpage > $totalpages) {
						// set current page to last page
						// $currentpage = $totalpages;
						$data = array();
					}else{
						$data = $this->Base_Models->CustomeQuary("SELECT * FROM crop_cat Where status = '1' ORDER BY id ASC LIMIT $offset, $rowsperpage");
										
						foreach ($data as $key => $value) {
								$images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['id'] ,"type"=>"3"),'image_url');
								$data[$key]["images"]= implode('',array_column($images, 'image_url'));
							}
					}
                // $data= $this->Base_Models->GetAllValues ( "crop_cat" ,array("status"=>"1"),"*, (SELECT image_url FROM  `images` where ref_id=crop_cat.id AND type = '3' ORDER BY id DESC LIMIT 1) as image_url"  );
			}
        $response ['data'] = $data;
        $response ['message'] = "done";
        $response ['result'] =  "Crop Category List";        
        }
        echo json_encode($response);
    }

}
?>