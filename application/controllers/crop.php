<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class crop extends Base_Controller {
	function __construct() {
		parent::__construct ();
		if (! isset ( $_SESSION ['id'] )) {
			$this->logout ();
		}
		$_SESSION['active_tag']="crop";
		
	}
	function index($id=null) {
		$data ['display_contents'] = array (
			"id" => "Id",
            "catName" => "Category",
            "vdrFname" => "Vendor",
            "farFname" => "Farmer",
			"harvest_date" => "Harvest Date",
			"pickup" => "Pickup",
			"estimated_weight" => "Weight",
            "lati" => "Latitude",
            "longi" => "Longitude",
            "contact_no" => "Contact",
			"default_date" => "Date",
            "image" => "Image",
			"action" => "Actions"
		);
		
		$data['action'] = base_url('crop');
		if($_POST != NULL){
			$data['fdate'] = $_POST['fdate'];
			$data['tdate'] = $_POST['tdate'];
			// $data ['table_data'] = $this->Base_Models->CustomeQuary("SELECT *,'' as image FROM farmer  WHERE default_date BETWEEN '".date('Y-m-d',strtotime($data['fdate']))."' AND '".date('Y-m-d',strtotime($data['tdate']))."'");
			$data ['table_data'] = $this->Base_Models->CustomeQuary("SELECT c.id as pid, cc.id as catId, cc.name as catName, v.user_id as vdrId, v.fname as vdrFname, v.lname as vdrLname, f.user_id as farId, f.fname as farFname, f.lname as farLnam, c.harvest_date, c.pickup, c.estimated_weight, c.lati, c.longi, c.contact_no, c.default_date
																	FROM `crop` as c 
																	LEFT JOIN crop_cat as cc ON c.cropcat_id = cc.id 
																	LEFT JOIN vendors as v ON c.vender_id = v.user_id
																	LEFT JOIN farmer as f ON c.farmer_id = f.user_id
																	WHERE c.status ='1' AND c.harvest_date BETWEEN '".date('Y-m-d',strtotime($data['fdate']))."' AND '".date('Y-m-d',strtotime($data['tdate']))."'");
		}else{
			$data ['table_data'] =$this->Base_Models->CustomeQuary ( "SELECT c.id as pid, cc.id as catId, cc.name as catName, v.user_id as vdrId, v.fname as vdrFname, v.lname as vdrLname, f.user_id as farId, f.fname as farFname, f.lname as farLnam, c.harvest_date, c.pickup, c.estimated_weight, c.lati, c.longi, c.contact_no, c.default_date
																	FROM `crop` as c 
																	LEFT JOIN crop_cat as cc ON c.cropcat_id = cc.id 
																	LEFT JOIN vendors as v ON c.vender_id = v.user_id
																	LEFT JOIN farmer as f ON c.farmer_id = f.user_id
																	WHERE c.status ='1'");
		}
		$_POST = array();// unset post
// echo '<pre>';
// print_r($data ['table_data']);
// die();
		// $data ['table_data'] =$this->Base_Models->GetAllValues ( "farmer");
		foreach ( $data ['table_data'] as $key => $val ) {
			$data ['table_data'] [$key] ['id'] = $key + 1;
			// $edit = " <button  onclick='window.location=\"" . base_url("crop/user_form/".$val['pid']) . "\"' class='btn btn-sm btn-outline-info'><i class='fa fa-edit' data-toggle='tooltip' data-placement='top' title='Edit'></i></button> 
					// <button  data-uniqueid=".$val['pid']." data-url=".base_url('crop/delete_crop')." onclick='return delete_row(".$val['pid'].")' id='deleteRowBtn' class='btn btn-sm btn-outline-danger'><i class='fa fa-trash' data-toggle='tooltip' data-placement='top' title='Delete'></i></button> ";
			$edit = "<button  data-uniqueid=".$val['pid']." data-url=".base_url('crop/delete_crop')." onclick='return delete_row(".$val['pid'].")' id='deleteRowBtn' class='btn btn-sm btn-outline-danger'><i class='fa fa-trash' data-toggle='tooltip' data-placement='top' title='Delete'></i></button> ";
			$data ['table_data'] [$key] ['action'] = $edit;
		}
		if(isset($id))
			$this->load->view ( "crop/table-view", $data );
		else
            $this->view ( "crop/table-view", $data );
	}
	
	//load meeting form
	function user_form($id=null){
		$data= null;
		if(isset($id)){
			$data1=$this->Base_Models->GetAllValues ( "crop" ,array("user_id"=>$id));
            $data=$data1[0];
        }
        // $data ['districts'] = getAllDistrictData();
        $data ['cancle'] = base_url ('crop');
		$data ['action'] = ($id == null) ? '../accept_users' : '../accept_users/' . $id ;
		$data ['action_title'] = ($id == null ? "Add" : "Update") . " User";
		
        $this->view ( "crop/form", $data );
		
	}
	
	function get_assembly_byDist(){
		$response ['message'] = "fail";
		$response ['reason'] = "Assembly Not found";
		// $_POST = $this->formatFormValues ( $_POST ['district'] );
		if(isset ( $_POST ['district'] ) && $_POST ['district'] != null){
			// $districts = getAllDistrictData();
			$this->load->helper('assembly_helper');
			$response['assembly'] = getAssembly_helper(str_replace(' ', '_', trim($_POST ['district'])));
			$response ['message'] = "done";
		}
		
		echo json_encode ( $response );
	}
	
	//Add OR Update form
	function accept_users($id=null){		
		$response ['message'] = "fail";
		$response ['reason'] = "All * fields are required";
		$_POST = $this->formatFormValues ( $_POST ['data'] );
		
		if(isset ( $_POST ['fname'] ) && $_POST ['fname'] != null && 
			isset ( $_POST ['lname'] ) && $_POST ['lname'] != null&& 
			isset ( $_POST ['adhar_no'] ) && $_POST ['adhar_no'] != null&& 
			isset ( $_POST ['user_address'] ) && $_POST ['user_address'] != null&& 
			isset ( $_POST ['user_city'] ) && $_POST ['user_city'] != null&& 
			isset ( $_POST ['user_district'] ) && $_POST ['user_district'] != null&& 
			isset ( $_POST ['user_state'] ) && $_POST ['user_state'] != null&& 
			isset ( $_POST ['pin'] ) && $_POST ['pin'] != null&& 
			isset ( $_POST ['user_mobile_no'] ) && $_POST ['user_mobile_no'] != null)
		{
			$data['fname'] = $_POST['fname'];
			$data['lname'] = $_POST['lname'];
			$data['user_mobile_no'] = $_POST['user_mobile_no'];
			$data['adhar_no'] = $_POST['adhar_no'];
			$data['user_address'] = $_POST['user_address'];
			$data['user_city'] = $_POST['user_city'];
			$data['user_district'] = $_POST['user_district'];
			$data['user_state'] = $_POST['user_state'];
			$data['pin'] = $_POST['pin'];
			if(isset ( $_POST ['email'] )){
				$data['email'] = $_POST['email'];
			}			
			
			if(isset($id)){
				//update
				$this->Base_Models->UpadateValue( "crop", $data ,array("user_id"=>$id));
				$response ['message'] = "done";
				$response ['reason'] = "Updated Successfully";
			}else{
				//add
				$this->Base_Models->AddValues( "crop", $data);
				$response ['message'] = "done";
				$response ['reason'] = "Added successfully";	
			}
			
			
		}
		
		// $response ['url'] = base_url ('crop/temp_user');
		
		echo json_encode ( $response );
	}
	
	function delete_crop(){
		$response ['message'] = "fail";
		$response ['reason'] = "Not Deleted Try again..!";
		$response ['uniqueid'] = $_POST['id_'];
		// $response ['url'] = base_url ('crop/temp_user');
		// $_POST = $this->formatFormValues ( $_POST ['data'] );
		
		$response['res'] = $this->Base_Models->RemoveValues('crop',array('id' => $_POST['id_']));
		if($response['res'] == 1){
			$response ['message'] = "done";
			$response ['reason'] = "Deleted Successfully";
		}
		
		echo json_encode ( $response );
	}
	
	//-- Temp user 
	function temp_user($id=null){
        if ($_SESSION ['role'] != "Admin") {
			exit ( "<script>history.back();</script>" );
		}
		$_SESSION ['active_btn'] = "temp_user";
		$_SESSION ['active_tag'] = "temp_user";
		$data ['title'] = "Temp user list";
		$data ['add_url'] = base_url ( 'crop/add_user' );
		$data ['display_contents'] = array (
				"user_mobile_no" => "Mobile number",
				"action" => "Actions" 
		);
		$data ['table_data'] = $this->Base_Models->GetAllValues ( "mpyc_users_temp" );
		foreach ( $data ['table_data'] as $key => $val ) {
			$data ['table_data'] [$key] ['user_id'] = $key + 1;
			$edit = " <button  onclick='window.location=\"" . base_url("crop/add_user/".$val['user_id']) . "\"' class='btn btn-sm btn-outline-info'><i class='fa fa-edit' data-toggle='tooltip' data-placement='top' title='Edit'></i></button> 
					<button  data-uniqueid=".$val['user_id']." data-url=".base_url('crop/delete_temp_user')." onclick='return delete_row(".$val['user_id'].")' id='deleteRowBtn' class='btn btn-sm btn-outline-danger'><i class='fa fa-trash' data-toggle='tooltip' data-placement='top' title='Delete'></i></button> ";
			$data ['table_data'] [$key] ['action'] = $edit;
		}
		if ($id != null)
			$this->load->view ( "common/table-view", $data );
		else
            $this->view ( "common/table-view", $data );  
               
    }
	
	function delete_temp_user(){
		$response ['message'] = "fail";
		$response ['reason'] = "Not Deleted Try again..!";
		$response ['uniqueid'] = $_POST['id_'];
		// $response ['url'] = base_url ('crop/temp_user');
		// $_POST = $this->formatFormValues ( $_POST ['data'] );
		
		$response['res'] = $this->Base_Models->RemoveValues('mpyc_users_temp',array('user_id' => $_POST['id_']));
		if($response['res'] == 1){
			$response ['message'] = "done";
			$response ['reason'] = "Deleted Successfully";
		}
		
		echo json_encode ( $response );
	}
	
	function add_user($id=null){
        $data= null;
		if(isset($id)){
			$data1=$this->Base_Models->GetAllValues ( "mpyc_users_temp" ,array("user_id"=>$id));
            $data=$data1[0];
        }
        $data ['title'] = "Add user";
        $data ['cancle'] = base_url ('crop/temp_user');
		$data ['action'] = ($id == null) ? './accept_user' : '../accept_user/' . $id ;
		// $data ['sub_title'] = ($id == null ? "Add" : "Update") . " message";
		// $data ['done_message'] = "message " . ($id == null ? "Added..." : "Updated...");
        $this->view ( "forms/add_user", $data );
               
    }
	
	function accept_user($id=null){		
		$response ['message'] = "fail";
		$response ['reason'] = "Number is required";
		$_POST = $this->formatFormValues ( $_POST ['data'] );
		
		if(isset ( $_POST ['user_mobile_no'] ) && $_POST ['user_mobile_no'] != null){
			$data['user_mobile_no'] = $_POST['user_mobile_no'];
			$exist = $this->Base_Models->GetSingleDetails( "mpyc_users_temp", array('user_mobile_no' =>$data['user_mobile_no']), 'user_id');
			if(count($exist) == 0){
				if(isset($id)){
					//update
					$this->Base_Models->UpadateValue( "mpyc_users_temp", $data ,array("user_id"=>$id));
					$response ['message'] = "done";
					$response ['reason'] = "Number updated successfully";
				}else{
					//add
					$this->Base_Models->AddValues( "mpyc_users_temp", $data);
					$response ['message'] = "done";
					$response ['reason'] = "User added successfully";	
				}
			}else{
				$response ['reason'] = "This number is already added";
			}
			
		}
		
		$response ['url'] = base_url ('crop/temp_user');
		echo json_encode ( $response );
	}
	//End Temp user
}
?>