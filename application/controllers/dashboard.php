<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class dashboard extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		if (! isset ( $_SESSION ['id'] )) {
			$this->logout ();
		}
	}
	public function index() {
		
		$_SESSION ['active_tag'] = 'dashboard';
		$data ['title'] = "dashboard";
		$trending_days = 8;
		// View
		$load_view = "dashboard";
		if($_SESSION['role']=="Admin"){
			$table_where['department_status']=1;
		}else{
			$table_where['department_id']=$_SESSION['department_id'];
		}
			$temp [0] ['cnt']= 100;
			$temp = $this->Base_Models->CustomeQuary ( "select count(*) as cnt from  farmer where user_status=2" );
			$data ['mobile_user_count'] = $temp [0] ['cnt'];
			$table_where=array();			
			$table_where_string="";			
			if(isset($_SESSION['department_id'])){
				$table_where['department_id']=$_SESSION['department_id'];
				$table_where_string =" where department_id=".$_SESSION['department_id'];
			}

		// $data['departments'] = $this->Base_Models->CustomeQuary("select *,(select count(*) from department_application where FIND_IN_SET(department_id,department_ids)) as counts  from corporation_department ".$table_where_string);
		// 	$data['departments'] = $this->Base_Models->CustomeQuary("select *,(select count(*) from application_status WHERE application_status.department_id = corporation_department.department_id) as approved,(select count(*) from department_application where FIND_IN_SET(department_id,department_ids) ) as counts ,(
		// select  count(*) from department_application  where application_id not in(select application_id from application_status where department_id=corporation_department.department_id and  application_status.application_id=department_application.application_id ) and status=0 and FIND_IN_SET(corporation_department.department_id,department_application.department_ids)		
		// 	) as acounts  from corporation_department ".$table_where_string);

		$temp= $this->Base_Models->CustomeQuary ( "SELECT COUNT(*) as user_cnt,DATE_FORMAT(farmer.default_date,'%d/%m/%Y') as user_date FROM `farmer` WHERE farmer.user_status=2 AND default_date BETWEEN NOW() - INTERVAL ".$trending_days." DAY AND NOW() GROUP BY DATE_FORMAT(farmer.default_date,'%d/%m/%Y')" );			
		$data ['applications'] = array ();	
			//$temp=$temp[0];
			for($i = 0; $i < $trending_days; $i ++) {
				$date = date ( "d/m/Y", strtotime ( '-' . $i . ' days' ) );
				$flag = 0;
				foreach ( $temp as $val ) {
					if ($date === $val ['user_date']) {
						$data ['applications'] [$i] = array (
								"date" => $val ['user_date'],
								'cnt' => $val ['user_cnt'] 
						);
						$flag = 1;
					}
				}
				
				if ($flag == 0) {
					$data ['applications'] [$i] = array (
							"date" => $date,
							'cnt' => 0 
					);
				}
			}
			$data ['applications'] = array_reverse ( $data ['applications'] );
			
			$temp= $this->Base_Models->CustomeQuary ( "SELECT COUNT(*) as user_cnt,DATE_FORMAT(farmer.default_date,'%d/%m/%Y') as user_date FROM `farmer` WHERE farmer.user_status!=2 AND default_date BETWEEN NOW() - INTERVAL ".$trending_days." DAY AND NOW() GROUP BY DATE_FORMAT(farmer.default_date,'%d/%m/%Y')" );	
			$data ['pending_applications'] = array ();
			for($i = 0; $i < $trending_days; $i ++) {
				$date = date ( "d/m/Y", strtotime ( '-' . $i . ' days' ) );
				$flag = 0;
				foreach ( $temp as $val ) {
					if ($date === $val ['user_date']) {
						$data ['pending_applications'] [$i] = array (
								"date" => $val ['user_date'],
								'cnt' => $val ['user_cnt'] 
						);
						$flag = 1;
					}
				}
				if ($flag == 0) {
					$data ['pending_applications'] [$i] = array (
							"date" => $date,
							'cnt' => 0 
					);
				}
			}
			$data ['pending_applications'] = array_reverse ( $data ['pending_applications'] );

		
		$this->view ( $load_view, $data );
	}
	
}
?>