<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class survey extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		if (! isset ( $_SESSION ['id'] )) {
			$this->logout ();
		}
    }
    public function index($id = null,$param2 = null,$param3 = null) {
        
        // if ($_SESSION ['role'] != "Admin") {
		// 	exit ( "<script>history.back();</script>" );
		// }
		$_SESSION['active_tag']="survey";
		$data ['display_contents'] = array (
			"id" => "ID",
            "district" => "District",
            "assembly" => "Assembly",
			"location" => "Location",
			"date" => "Date",
			"user_mobile_no" => "Mobile",
			"image" => "Image",
			"action" => "Actions"			
		);

		$data['action'] = base_url('survey');
		if($_POST != NULL){
			$data['fdate'] = $_POST['fdate'];
			$data['tdate'] = $_POST['tdate'];
			$data ['table_data'] = $this->Base_Models->CustomeQuary("SELECT *, id as pid, from_unixtime((date)/1000, '%Y-%m-%d %h:%i:%s') as date, '' as image,(SELECT designation FROM mpyc_users where user_mobile_no=mpyc_survey.user_mobile_no ) as designation FROM (`mpyc_survey`) WHERE date BETWEEN '".strtotime($data['fdate'])."' AND '".strtotime($data['tdate'])."'");
		}else{
			$data ['table_data'] = $this->Base_Models->CustomeQuary("SELECT *, id as pid, from_unixtime((date)/1000, '%Y-%m-%d %h:%i:%s') as date, '' as image,(SELECT designation FROM mpyc_users where user_mobile_no=mpyc_survey.user_mobile_no ) as designation FROM (`mpyc_survey`) ");
		}
		$_POST = array();// unset post

		foreach ( $data ['table_data'] as $key => $val ) {
			$data ['table_data'] [$key] ['id'] = $key + 1;
			// $edit = " <button  onclick='window.location=\"" . base_url("events/event_form/".$val['id']) . "\"' class='btn btn-sm btn-outline-info'><i class='fa fa-edit' data-toggle='tooltip' data-placement='top' title='Edit'></i></button> 
					// <button  data-uniqueid=".$val['id']." data-url=".base_url('events/delete_event')." onclick='return delete_row(".$val['id'].")' id='deleteRowBtn' class='btn btn-sm btn-outline-danger'><i class='fa fa-trash' data-toggle='tooltip' data-placement='top' title='Delete'></i></button> ";
			$edit = "<button  data-uniqueid=".$val['id']." data-url=".base_url('survey/delete_survey')." onclick='return delete_row(".$val['id'].")' id='deleteRowBtn' class='btn btn-sm btn-outline-danger'><i class='fa fa-trash' data-toggle='tooltip' data-placement='top' title='Delete'></i></button> ";
			$data ['table_data'] [$key] ['action'] = $edit;
		}
		if ($id != null)
			$this->load->view ( "common/table-view", $data );
		else
            $this->view ( "common/table-view", $data );
	}
	
	//load event form
	function event_form($id=null){
		$data= null;
		if(isset($id)){
			$data1=$this->Base_Models->GetAllValues ( "events" ,array("id"=>$id));
            $data=$data1[0];
        }
        $data ['cancle'] = base_url ('events');
		$data ['action'] = ($id == null) ? '../accept_event' : '../accept_event/' . $id ;
		$data ['action_title'] = ($id == null ? "Add" : "Update") . " Event";
		
        $this->view ( "forms/event_form", $data );
		
	}
	
	//Add OR Update form
	function accept_event($id=null){		
		$response ['message'] = "fail";
		$response ['reason'] = "All fields are required";
		$_POST = $this->formatFormValues ( $_POST ['data'] );
		
		if(isset ( $_POST ['title'] ) && $_POST ['title'] != null && 
			isset ( $_POST ['desciption'] ) && $_POST ['desciption'] != null&& 
			isset ( $_POST ['rating'] ) && $_POST ['rating'] != null&& 
			isset ( $_POST ['location'] ) && $_POST ['location'] != null&& 
			isset ( $_POST ['user_mobile_no'] ) && $_POST ['user_mobile_no'] != null)
		{
				
			$data['title'] = $_POST['title'];
			$data['desciption'] = $_POST['desciption'];
			$data['rating'] = $_POST['rating'];
			$data['location'] = $_POST['location'];
			$data['user_mobile_no'] = $_POST['user_mobile_no'];
			
			
			if(isset($id)){
				//update
				$this->Base_Models->UpadateValue( "events", $data ,array("id"=>$id));
				$response ['message'] = "done";
				$response ['reason'] = "Updated Successfully";
			}else{
				//add
				$this->Base_Models->AddValues( "events", $data);
				$response ['message'] = "done";
				$response ['reason'] = "Added successfully";	
			}
			
			
		}
		
		// $response ['url'] = base_url ('users/temp_user');
		
		echo json_encode ( $response );
	}
	
	function delete_survey(){
		$response ['message'] = "fail";
		$response ['reason'] = "Not Deleted Try again..!";
		$response ['uniqueid'] = $_POST['id_'];
		// $response ['url'] = base_url ('users/temp_user');
		// $_POST = $this->formatFormValues ( $_POST ['data'] );
		
		$response['res'] = $this->Base_Models->RemoveValues('mpyc_survey',array('id' => $_POST['id_']));
		if($response['res'] == 1){
			$response ['message'] = "done";
			$response ['reason'] = "Deleted Successfully";
		}
		
		echo json_encode ( $response );
	}
}

?>