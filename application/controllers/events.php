<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class events extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		if (! isset ( $_SESSION ['id'] )) {
			$this->logout ();
		}
    }
    public function index($id = null,$param2 = null,$param3 = null) {
        
        // if ($_SESSION ['role'] != "Admin") {
		// 	exit ( "<script>history.back();</script>" );
		// }
		$_SESSION['active_tag']="Events";
		$data ['display_contents'] = array (
			"id" => "EventID",
            "title" => "Title",
            "desciption" => "Desciption",
			"c_start_date" => "Date",
			"user_mobile_no" => "Mobile",
			"designation" => "Designation",
			"rating" => "Rating",
			"location" => "Location",
			"image" => "Image",
			"action" => "Actions"
			
		);

		$data['action'] = base_url('events');
		if($_POST != NULL){
			$data['fdate'] = $_POST['fdate'];
			$data['tdate'] = $_POST['tdate'];
			$data ['table_data'] = $this->Base_Models->CustomeQuary("SELECT *, id as pid, from_unixtime((start_date)/1000, '%Y-%m-%d %h:%i:%s') as c_start_date, '' as image,(SELECT designation FROM mpyc_users where user_mobile_no=events.user_mobile_no ) as designation FROM (`events`) WHERE start_date BETWEEN '".strtotime($data['fdate'])."' AND '".strtotime($data['tdate'])."'");
		}else{
			$data ['table_data'] = $this->Base_Models->CustomeQuary("SELECT *, id as pid, from_unixtime((start_date)/1000, '%Y-%m-%d %h:%i:%s') as c_start_date, '' as image,(SELECT designation FROM mpyc_users where user_mobile_no=events.user_mobile_no ) as designation FROM (`events`) ");
		}
		$_POST = array();// unset post

	// 	$activate_applications="";
	// 	$edit ="";
	// 	$_SESSION ['active_btn'] = "Applications";
	// 	$_SESSION ['active_tag'] = "Applications";
	// 	$data ['title'] = "NOC Applications";
	// 	$edit_url="";
		
	// 	//$data ['add_url'] = base_url ( 'applications/add_applications' );
	// 	if($_SESSION['role']=="Admin"){
	// 	$data ['display_contents'] = array (
 //            "application_id" => "Application ID",
 //            "user_mobile_no" => "Mobile No",
 //            "registration_no" => "Registration No",
	// 			"mandal_name" => "Mandal Name",
	// 			"area" => "Area",
 //                "road" => "Road",
 //                "depth" => "Depth",
	// 			"width" => "Width",
	// 			"doc" => "Document",
				
	// 			"department_count" => "NOC Dept.",
	// 			"varify_department_count" => "Approved NOC",
 //                "suggestions" => "Suggestions",
	// 			"status" => "Status",
	// 			"action" => "Actions" 
	// 	);
	// 	$activate_applications="noc_applications/activate_applications";
	// 	$data ['table_data'] = $this->Base_Models->GetAllValues ( "department_application" ,null, " * ,(select count(*) from application_status where application_status.application_id=department_application.application_id) as varify_department_count " );
	// 	$edit_url="noc_applications/add_suggestion";

	// }else{
	
			
	// 	$data ['display_contents'] = array (
	// 		"application_id" => "Application ID",
 //            "user_mobile_no" => "Mobile No",
 //            "registration_no" => "Registration No",
	// 		"mandal_name" => "Mandal Name",
	// 			"area" => "Area",
 //                "road" => "Road",
 //                "depth" => "Depth",
	// 			"width" => "Width",
	// 			"doc" => "Document",
	// 		    "dpt_suggestions" => "Suggestions",
	// 			"status" => "Status",
	// 			"action" => "Actions" 
	// 	);
	// 	$activate_applications="noc_applications/aproved_dpt_applications";
	// 	//  where application_id not in(select application_id from application_status where department_id=".$_SESSION['department_id'].")) 
	// 	$data ['table_data'] = $this->Base_Models->CustomeQuary ( "select  * ,(select count(*) from application_status where application_status.application_id=department_application.application_id) as varify_department_count ,(select suggestions from application_status where application_status.application_id=department_application.application_id and department_id=".$_SESSION['department_id'].") as dpt_suggestions from department_application  where application_id not in(select application_id from application_status where department_id=".$_SESSION['department_id']." and  application_status.application_id=department_application.application_id ) and status=0 and FIND_IN_SET(".$_SESSION['department_id'].",department_application.department_ids)" );
	// 	$edit_url="noc_applications/add_department_suggestion";
	// }

	// $print="noc_applications/print_view";
	// 	foreach ( $data ['table_data'] as $key => $val ) {
	// 		$data ['table_data'] [$key] ['id'] = $key + 1;
	// 		//$edit = " <button  onclick='window.location=\"./noc_applications/department/" . $data ['table_data'] [$key] ['application_id'] . "\"' class='btn btn-outline-info'><i class='fa fa-edit' data-toggle='tooltip' data-placement='top' title='Suggestion'></i></button> ";
	// 		if(isset($val['document_copy_one'])){
	// 			$data ['table_data'] [$key] ['doc']="";
	// 			if($data ['table_data'] [$key] ['document_copy_one']!="")
	// 			$data ['table_data'] [$key] ['doc'] .= "<a href='#' onclick=window.open('".$data ['table_data'] [$key] ['document_copy_one']."')>Doc 1</a>" ;	
	// 			if($data ['table_data'] [$key] ['document_copy_two']!="")
	// 			$data ['table_data'] [$key] ['doc'] .= "<a href='#' onclick=window.open('".$data ['table_data'] [$key] ['document_copy_two']."')>Doc 2</a>" ;	
	// 			if($data ['table_data'] [$key] ['document_copy_three']!="")
	// 			$data ['table_data'] [$key] ['doc'] .= "<a href='#' onclick=window.open('".$data ['table_data'] [$key] ['document_copy_three']."')>Doc 3</a>" ;	
	// 		}
	// 		//	$print="noc_applications/";

	
	// 		$edit = " <button  onclick='window.location=\"./".$edit_url."/" . $data ['table_data'] [$key] ['application_id'] . "\"' class='btn btn-outline-info'><i class='fa fa-edit' data-toggle='tooltip' data-placement='top' title='Suggestion'></i></button> ";
			
		
	// 		$activate = " <button  onclick='$(\"#myModalActionBtn\").attr(\"value\",$(this).attr(\"data-url\")); $(\"#myModalBody\").html(\"Do you want to approve this application?\"); $(\"#myModalLabel\").html(\"Activate Record\"); $(\"#myModalLabel\").parent().parent().css(\"background-color\",\"#fff5f5\");' class='btn btn-outline-success'  data-url='" . base_url () .$activate_applications. "/" . $data ['table_data'] [$key] ['application_id'] . "' data-target='#myModal' data-toggle='modal'><i class='fa fa-unlock-alt' data-toggle='tooltip' data-placement='left' title='Approve Application'></i></button>";
	// 		$deactivate = " ";
	// 		$data ['table_data'] [$key] ['varify_department_count'] = "<a href='".base_url("noc_applications/application_status/".$data ['table_data'] [$key] ['application_id'])."'>".$data ['table_data'] [$key] ['varify_department_count'] ."</a>" ;
	// 		//$deactivate = " <button  onclick='$(\"#myModalActionBtn\").attr(\"value\",$(this).attr(\"data-url\")); $(\"#myModalBody\").html(\"Are you sure to deactivate this record?\"); $(\"#myModalLabel\").html(\"Deactivate Record\"); $(\"#myModalLabel\").parent().parent().css(\"background-color\",\"#fff5f5\");' class='btn btn-outline-danger'  data-url='" . base_url () . "noc_applications/deactivate_applications/" . $data ['table_data'] [$key] ['application_id'] . "' data-target='#myModal' data-toggle='modal'><i class='fa fa-lock' data-toggle='tooltip' data-placement='left' title='Deactivate Application'></i></button>";
	// 		$data ['table_data'] [$key] ['action'] = $edit;
	// 		if ($data ['table_data'] [$key] ['status'] == 0 && $data ['table_data'] [$key] ['application_id'] != '' && $data ['table_data'] [$key] ['application_id'] != '0') {
	// 			$data ['table_data'] [$key] ['action'] .= $activate ;
	// 		} elseif ($data ['table_data'] [$key] ['status'] == 1) {
				
	// 			$edit = " <button  onclick='window.location=\"./".$print."/" . $data ['table_data'] [$key] ['application_id'] . "\"' class='btn btn-outline-info'><i class='fa fa-eye' data-toggle='tooltip' data-placement='top' title='Print'></i></button> ";

	// 			$data ['table_data'] [$key] ['action'] .= $deactivate.$edit ;
	// 		} elseif ($data ['table_data'] [$key] ['status'] == 2) {
	// 			$data ['table_data'] [$key] ['action'] .= $activate ;
	// 		}
	// 		$data ['table_data'] [$key] ['status'] = ($val ['status'] == 0) ? "Pending" : ($val ['status'] == 1 ? "Active" : ($val ['status'] == 2 ? "Deactivated" : ""));
	// 	}
	
		foreach ( $data ['table_data'] as $key => $val ) {
			$data ['table_data'] [$key] ['id'] = $key + 1;
			$edit = " <button  onclick='window.location=\"" . base_url("events/event_form/".$val['id']) . "\"' class='btn btn-sm btn-outline-info'><i class='fa fa-edit' data-toggle='tooltip' data-placement='top' title='Edit'></i></button> 
					<button  data-uniqueid=".$val['id']." data-url=".base_url('events/delete_event')." onclick='return delete_row(".$val['id'].")' id='deleteRowBtn' class='btn btn-sm btn-outline-danger'><i class='fa fa-trash' data-toggle='tooltip' data-placement='top' title='Delete'></i></button> ";
			$data ['table_data'] [$key] ['action'] = $edit;
		}
		if ($id != null)
			$this->load->view ( "common/table-view", $data );
		else
            $this->view ( "common/table-view", $data );
	}
	
	function createxls(){
		$data ['data'] = $this->Base_Models->CustomeQuary("SELECT *, id as pid, from_unixtime((start_date)/1000, '%Y-%m-%d %h:%i:%s') as c_start_date, '' as image,(SELECT designation FROM mpyc_users where user_mobile_no=events.user_mobile_no ) as designation FROM (`events`) ");
		
		// create file name
			$fileName = 'Events-data-'.date('d-M-Y').'.xlsx';  
			// load excel library
			$this->load->library('excel');
			$info = $data['data'];
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			// set Header
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'EventID');
			$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Title');
			$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Desciption');
			$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Date');
			$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Mobile');       
			$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Designation');       
			$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Rating');       
			$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Location');       
			// $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Image');  
			// set Row
			$rowCount = 2;
			foreach ($info as $element) {
				// $imgs = getEventimages($element['pid']);
				// $cnt = count($imgs);
				// $image = '';
				// if($cnt>0){
					// $y = 1; 
					// for($x=0;$x<$cnt;$x++){
						// $image .= '<a href='.$imgs[$x]['image_url'].' target="_blank"> Img'.$y++.'</a>, ';
					// }
				// }else{ 
					// $image .= 'No images';
				// }
																
				$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['id']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['title']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['desciption']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['c_start_date']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['user_mobile_no']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['designation']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['rating']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['location']);
				// $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $image);
					// $pay_status = ($element['pay_status'] == '0') ? 'Pending' : 'Received';
				// $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $pay_status);
				$rowCount++;
			}
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save('uploads/admin/excel/'.$fileName);
			// download file
			header("Content-Type: application/vnd.ms-excel");
			redirect(base_url('uploads/admin/excel/'.$fileName));
	}
	
	//load event form
	function event_form($id=null){
		$data= null;
		if(isset($id)){
			$data1=$this->Base_Models->GetAllValues ( "events" ,array("id"=>$id));
            $data=$data1[0];
        }
        $data ['cancle'] = base_url ('events');
		$data ['action'] = ($id == null) ? '../accept_event' : '../accept_event/' . $id ;
		$data ['action_title'] = ($id == null ? "Add" : "Update") . " Event";
		
        $this->view ( "forms/event_form", $data );
		
	}
	
	//Add OR Update form
	function accept_event($id=null){		
		$response ['message'] = "fail";
		$response ['reason'] = "All fields are required";
		$_POST = $this->formatFormValues ( $_POST ['data'] );
		
		if(isset ( $_POST ['title'] ) && $_POST ['title'] != null && 
			isset ( $_POST ['desciption'] ) && $_POST ['desciption'] != null&& 
			isset ( $_POST ['rating'] ) && $_POST ['rating'] != null&& 
			isset ( $_POST ['location'] ) && $_POST ['location'] != null&& 
			isset ( $_POST ['user_mobile_no'] ) && $_POST ['user_mobile_no'] != null)
		{
				
			$data['title'] = $_POST['title'];
			$data['desciption'] = $_POST['desciption'];
			$data['rating'] = $_POST['rating'];
			$data['location'] = $_POST['location'];
			$data['user_mobile_no'] = $_POST['user_mobile_no'];
			
			
			if(isset($id)){
				//update
				$this->Base_Models->UpadateValue( "events", $data ,array("id"=>$id));
				$response ['message'] = "done";
				$response ['reason'] = "Updated Successfully";
			}else{
				//add
				$this->Base_Models->AddValues( "events", $data);
				$response ['message'] = "done";
				$response ['reason'] = "Added successfully";	
			}
			
			
		}
		
		// $response ['url'] = base_url ('users/temp_user');
		
		echo json_encode ( $response );
	}
	
	function delete_event(){
		$response ['message'] = "fail";
		$response ['reason'] = "Not Deleted Try again..!";
		$response ['uniqueid'] = $_POST['id_'];
		// $response ['url'] = base_url ('users/temp_user');
		// $_POST = $this->formatFormValues ( $_POST ['data'] );
		
		$response['res'] = $this->Base_Models->RemoveValues('events',array('id' => $_POST['id_']));
		if($response['res'] == 1){
			$response ['message'] = "done";
			$response ['reason'] = "Deleted Successfully";
		}
		
		echo json_encode ( $response );
	}
	
	function test_print_view($id){
			$data=$this->Base_Models->GetAllValues ( "department_application" ,array("application_id"=>$id));
			
			$this->load->view("application_print",$data[0]);
		}
	function print_view($id){
		$data=$this->Base_Models->GetAllValues ( "department_application" ,array("application_id"=>$id));
		
				$this->load->view("print_noc",$data[0]);
	}
	function add_department_suggestion($id){
  		$data= null;
        if(isset($id))
        {
			$data1=$this->Base_Models->GetAllValues ( "department_application" ,array("application_id"=>$id));
            $data=$data1[0];
        }
        $data ['title'] = "NOC Applications";
		$data ['action'] = $id == null ? base_url ( 'noc_applications/accept_department_suggestion' ) : base_url ( 'noc_applications/accept_department_suggestion/' . $id );
		$data ['sub_title'] = ($id == null ? "Add" : "Update") . " NOC Applications";
		$data ['done_message'] = "NOC Applications " . ($id == null ? "Added..." : "Updated...");
        $this->view ( "forms/noc_applications_suggestions", $data );

	}
	function aproved_dpt_applications($id){
		
		$response ['message'] = "fail";
        $post = $_POST;
        $data = array ();
        if ($id != null) {
           $data ['application_id'] = $id;
			$data ['department_id'] = $_SESSION['department_id'];
			$temp = $this->Base_Models->GetAllValues ("application_status",$data);
			if(count($temp)==1)
			$temp = $this->Base_Models->UpadateValue ( "application_status", $data,$data);
			else{
				$temp = $this->Base_Models->AddValues ( "application_status",$data);
			}
            if ($temp != 0) {
            	            if ($temp != 0) {
					$where ['department_id'] = $_SESSION['department_id'];
					$corporation_department = $this->Base_Models->GetAllValues ("corporation_department",$where);
					$corporation_department=$corporation_department[0];
					$data ['application_id'] = $id;
					//user_mobile_no / department_application
					$select_mobile_no = "select user_mobile_no from department_application where application_id=".$id;
					$select_device_token = "select device_token,user_mobile_no from corporation_mobile_user where user_mobile_no in (".$select_mobile_no.")";
					$device_token = $this->Base_Models->CustomeQuary ($select_device_token);
					/////////////
					//		$select_department_name = "select policeStationName from tbl_police_station where id='".$_SESSION ['user_type']."'";
					//$select_department_data = $this->Base_Models->CustomeQuary ($select_department_name);
					/////////////
					
					$message = "Application no (MAPA-".$id.") approved by ".$corporation_department ['department_name'];
					$mobile_message = "Application no (MAPA-".$id.") has been sucessfully submitted";
					$this->message_send ( $mobile_message, $device_token[0]['user_mobile_no']);
					$data_return = $this->pushNotification ($device_token[0]['device_token'], $message, 'message' );

                $response ['message'] = "done";
                $response ['url'] = "";
			}
			//redirect(base_url("noc_applications"));
		}
		echo json_encode($response );

	}
}
	function accept_department_suggestion($id){
		$response ['message'] = "fail";
        $post = $_POST;
        $data = array ();
        if ($id != null) {
           
			$data1 ['suggestions'] = $post ['suggestions'];
			$data ['application_id'] = $id;
			$data ['department_id'] = $_SESSION['department_id'];
			$temp = $this->Base_Models->GetAllValues ("application_status",$data);
			if(count($temp)==1)
			$temp = $this->Base_Models->UpadateValue ( "application_status", $data1,$data);
			else{
				$data['suggestions']=$data1 ['suggestions'] ;
				$temp = $this->Base_Models->AddValues ( "application_status",$data);
			}
            if ($temp != 0) {
            	$where ['department_id'] = $_SESSION['department_id'];
				$corporation_department = $this->Base_Models->GetAllValues ("corporation_department",$where);
				$corporation_department=$corporation_department[0];
				$data ['application_id'] = $id;
				//user_mobile_no / department_application
				$select_mobile_no = "select user_mobile_no from department_application where application_id=".$id;
				$select_device_token = "select device_token,user_mobile_no from corporation_mobile_user where user_mobile_no in (".$select_mobile_no.")";
				$device_token = $this->Base_Models->CustomeQuary ($select_device_token);
				//aa
				$mobile_message = "Application no (MAPA-".$id.") has been sucessfully submitted";
			$message = "Read suggestion for application no (MAPA-".$id.") and approved by ".$corporation_department ['department_name']." ";
	
				$this->message_send ( $mobile_message, $device_token[0]['user_mobile_no']);
				$data_return = $this->pushNotification ($device_token[0]['device_token'], $message, 'message' );
				//$this->message_send ( $message,  $select_mobile_no);
                $response ['message'] = "done";
                $response ['url'] = "";
			}
			//redirect(base_url("noc_applications"));
		}
		echo json_encode($response );
		
	}
	function tc($id){
		$data['department_id']=$id;
		$temp = $this->Base_Models->GetAllValues ( "corporation_department", $data );
		echo '<meta charset="utf-8">';
		echo $temp[0]['department_notice'];
	}
	function application_status($id=null){
		$_SESSION ['active_btn'] = "Applications";
		$_SESSION ['active_tag'] = "Applications";
		$data ['title'] = "NOC Applications Depatment Status ".$id;
		//$data ['add_url'] = base_url ( 'applications/add_applications' );
			$TableValues=array();
			$data ['table_data']=array();
		$data ['display_contents'] = array (
			"department_name"=>"Name",
			"department_suggetion"=>"Suggetion",
			"status" => "Status",
			"action" => "Actions" 
		);

		///

		$department_application = $this->Base_Models->GetAllValues ( "department_application" ,array("application_id"=>$id) );

		$application_status = $this->Base_Models->CustomeQuary ( " select * from application_status where FIND_IN_SET(department_id,'".$department_application[0]['department_ids']."') and application_id=".$id );
		$corporation_department = $this->Base_Models->CustomeQuary ( " select * from corporation_department where FIND_IN_SET(department_id,'".$department_application[0]['department_ids']."') " );
$url=base_url("noc_applications/tc");
foreach($corporation_department as $row){
	$TableValues['department_name']=$row['department_name'];
	
	//$a=array("a"=>"red","b"=>"green","c"=>"blue");
//echo array_search("red",$a);
//array_search(40489, array_column($userdb, 'uid'))
$TableValues['action']="<a href='".$url."/".$row['department_id']."'>View T & C</a>";
$TableValues['department_suggetion']="";
$TableValues['status']="Not Approved";
foreach($application_status as $key=>$row1){
	
	if($row['department_id']==$row1["department_id"]){		
		$TableValues['status']="Approved";
		$TableValues['department_suggetion']=$row1['suggestions'];
		unset($application_status[$key]);
	}
}

		$TbleValues['action']="";
array_push($data ['table_data'],$TableValues);		
}
		//$data ['table_data']=$TableValues;
		//$data ['table_data'] = $this->Base_Models->GetAllValues ( "corporation_department" ,array("application_id"=>$id) );
		foreach ( $data ['table_data'] as $key => $val ) {

				}
		
			$this->view ( "common/table-view", $data );
			

	}
    function add_suggestion($id=null){
        $data= null;
        if(isset($id))
        {
			$data1=$this->Base_Models->GetAllValues ( "department_application" ,array("application_id"=>$id));
            $data=$data1[0];
        }
        $data ['title'] = "NOC Applications";
		$data ['action'] = $id == null ? base_url ( 'noc_applications/accept_suggestion' ) : base_url ( 'noc_applications/accept_suggestion/' . $id );
		$data ['sub_title'] = ($id == null ? "Add" : "Update") . " NOC Applications";
		$data ['done_message'] = "NOC Applications " . ($id == null ? "Added..." : "Updated...");
        $this->view ( "forms/noc_applications_suggestions", $data );
    }
    function accept_suggestion($id=null){
	
        $response ['message'] = "fail";
        $post = $_POST;
        $data = array ();
        if ($id != null) {
           $data1 ['suggestions'] = $post ['suggestions'];
				//$data ['status'] = 1;
				$data ['application_id'] = $id;
				$data ['department_id'] = 1;
				$temp = $this->Base_Models->GetAllValues ("application_status",$data);
				if(count($temp)==1)
				$temp = $this->Base_Models->UpadateValue ( "application_status", $data1,$data);
				else{
					$data['suggestions']=$data1 ['suggestions'] ;
					$temp = $this->Base_Models->AddValues ( "application_status",$data);
				}

            // $data ['department_name'] = $post ['department_name'];
			$data1 ['status'] = 1;

            $temp = $this->Base_Models->UpadateValue ( "department_application", $data1, array (
                    "application_id" => $id 
            ) );
            if ($temp != 0) {
                $response ['message'] = "done";
                $response ['url'] = "";
			}
			$where ['department_id'] = 1;
	
				$corporation_department = $this->Base_Models->GetAllValues ("corporation_department",$where);
				$corporation_department=$corporation_department[0];
				
							//user_mobile_no / department_application
							$select_mobile_no = "select user_mobile_no from department_application where application_id=".$id;
							$select_device_token = "select device_token,user_mobile_no from corporation_mobile_user where user_mobile_no in (".$select_mobile_no.")";
							$device_token = $this->Base_Models->CustomeQuary ($select_device_token);
							//
							$corporation_department ['department_name']="Mahanagar palika";
							$message = "Application No (MAPA-".$id.") has been Approved by ".$corporation_department ['department_name']." with few suggestions, please Check out department suggestion in application status section.";
							$data_return = $this->pushNotification ($device_token[0]['device_token'], $message, 'message' );
							$this->message_send ( $message, $device_token[0]['user_mobile_no']);
		//	redirect(base_url("noc_applications"));
        } else {
         
            // $data ['department_notice'] = $post ['department_notice'];
            // $data ['department_name'] = $post ['department_name'];
           // $temp = $this->Base_Models->AddValues ( "department_application", $data );
            // if ($temp != 0) {
            //     $response ['message'] = "done";
            //     $response ['url'] = "./";
            // }
        }
        echo json_encode ( $response );
    }
    function activate_applications($id) {
        $response ['message'] = "fail";
		if (isset ( $id )) {
			$data ['application_id'] = $id;
				$data ['department_id'] = 1;
				$temp = $this->Base_Models->GetAllValues ("application_status",$data);
				if(count($temp)==1)
				$temp = $this->Base_Models->UpadateValue ( "application_status", $data1,$data);
				else{
					//$data['suggestions']=$data1 ['suggestions'] ;
					$temp = $this->Base_Models->AddValues ( "application_status",$data);
				}
			$temp = $this->Base_Models->UpadateValue ( "department_application", array (
					"status" => 1
			), array (
					"application_id" => $id 
			) );
			if ($temp != 0) {
				$response ['message'] = "done";
				$response ['url'] = "";
			}
			$where ['department_id'] = 1;
				$corporation_department = $this->Base_Models->GetAllValues ("corporation_department",$where);
				$corporation_department=$corporation_department[0];
				
							//user_mobile_no / department_application
							$select_mobile_no = "select user_mobile_no from department_application where application_id=".$id;
							$select_device_token = "select device_token,user_mobile_no from corporation_mobile_user where user_mobile_no in (".$select_mobile_no.")";
							$device_token = $this->Base_Models->CustomeQuary ($select_device_token);
							//
							$corporation_department ['department_name']="Mahanagar palika";
							$message = "Application no (MAPA-".$id.") approved by ".$corporation_department ['department_name'];
							$data_return = $this->pushNotification ($device_token[0]['device_token'], $message, 'message' );
							$this->message_send ( $message, $device_token[0]['user_mobile_no']);
		}
		echo json_encode ( $response );
    }
    function deactivate_applications($id) {
		$response ['message'] = "fail";
		if (isset ( $id )) {
			$temp = $this->Base_Models->UpadateValue ( "department_application", array (
					"status" => 0 
			), array (
					"application_id" => $id 
			) );
			if ($temp != 0) {
				$response ['message'] = "done";
				$response ['url'] = "./";
			}
		}
		echo json_encode ( $response );
	}
	
// department 



 function department($id = null) {
        
	// if ($_SESSION ['role'] != "Admin") {
	// 	exit ( "<script>history.back();</script>" );
	// }
	$_SESSION ['active_btn'] = "Applications";
	$_SESSION ['active_tag'] = "Applications";
	$data ['title'] = "NOC Applications";
	//$data ['add_url'] = base_url ( 'applications/add_applications' );
	$data ['display_contents'] = array (
		"application_id" => "ID",
		"registration_no" => "Registration No",
			"mandal_name" => "Mandal Name",
			"area" => "Area",
			"road" => "Road",
			"depth" => "Depth",
			"width" => "width",
			"department_count" => "Applied Dept.",
			"suggestions" => "suggestions",
			"status" => "Status",
			"action" => "Actions" 
	);
	$data ['table_data'] = $this->Base_Models->GetAllValues ( "department_application" );

	foreach ( $data ['table_data'] as $key => $val ) {
		$data ['table_data'] [$key] ['id'] = $key + 1;
		$edit = "";
		$activate = " <button  onclick='$(\"#myModalActionBtn\").attr(\"value\",$(this).attr(\"data-url\")); $(\"#myModalBody\").html(\"Are you sure to activate this record?\"); $(\"#myModalLabel\").html(\"Activate Record\"); $(\"#myModalLabel\").parent().parent().css(\"background-color\",\"#fff5f5\");' class='btn btn-outline-success'  data-url='" . base_url () . "noc_applications/activate_applications/" . $data ['table_data'] [$key] ['application_id'] . "' data-target='#myModal' data-toggle='modal'><i class='fa fa-unlock-alt' data-toggle='tooltip' data-placement='left' title='Activate Application'></i></button>";
		$deactivate = " <button  onclick='$(\"#myModalActionBtn\").attr(\"value\",$(this).attr(\"data-url\")); $(\"#myModalBody\").html(\"Are you sure to deactivate this record?\"); $(\"#myModalLabel\").html(\"Deactivate Record\"); $(\"#myModalLabel\").parent().parent().css(\"background-color\",\"#fff5f5\");' class='btn btn-outline-danger'  data-url='" . base_url () . "noc_applications/deactivate_applications/" . $data ['table_data'] [$key] ['application_id'] . "' data-target='#myModal' data-toggle='modal'><i class='fa fa-lock' data-toggle='tooltip' data-placement='left' title='Deactivate Application'></i></button>";
		$data ['table_data'] [$key] ['action'] = $edit;
		if ($data ['table_data'] [$key] ['status'] == 0 && $data ['table_data'] [$key] ['application_id'] != '' && $data ['table_data'] [$key] ['application_id'] != '0') {
			$data ['table_data'] [$key] ['action'] .= $activate ;
		} elseif ($data ['table_data'] [$key] ['status'] == 1) {
			$data ['table_data'] [$key] ['action'] .= $deactivate ;
		} elseif ($data ['table_data'] [$key] ['status'] == 2) {
			$data ['table_data'] [$key] ['action'] .= $activate ;
		}
		$data ['table_data'] [$key] ['status'] = ($val ['status'] == 0) ? "Pending" : ($val ['status'] == 1 ? "Active" : ($val ['status'] == 2 ? "Deactivated" : ""));
	}
	if ($id != null)
		$this->load->view ( "common/table-view", $data );
	else
		$this->view ( "common/table-view", $data );
}
function add_suggetion($id=null){
	$data= null;
	if(isset($id))
	{
		$data1=$this->Base_Models->GetAllValues ( "department_application" ,array("application_id"=>$id));
		$data=$data1[0];
	}
	$data ['title'] = "NOC Applications";
	$data ['action'] = $id == null ? base_url ( 'noc_applications/accept_applications' ) : base_url ( 'noc_applications/accept_applications/' . $id );
	$data ['sub_title'] = ($id == null ? "Add" : "Update") . " NOC Applications";
	$data ['done_message'] = "NOC Applications " . ($id == null ? "Added..." : "Updated...");
	$this->view ( "forms/noc_applications", $data );
}
function accept_applications($id=null){

	$response ['message'] = "fail";
	$post = $_POST;
	$data = array ();
	if ($id != null) {
	   
		// $data ['department_notice'] = $post ['department_notice'];
		// $data ['department_name'] = $post ['department_name'];
		$temp = $this->Base_Models->UpadateValue ( "department_application", $data, array (
				"application_id" => $id 
		) );
		if ($temp != 0) {
			$response ['message'] = "done";
			$response ['url'] = "";
		}
	} else {
	 
		// $data ['department_notice'] = $post ['department_notice'];
		// $data ['department_name'] = $post ['department_name'];
	   // $temp = $this->Base_Models->AddValues ( "department_application", $data );
		// if ($temp != 0) {
		//     $response ['message'] = "done";
		//     $response ['url'] = "./";
		// }
	}
	echo json_encode ( $response );
}

function getAssembly(){
	$district = $this->input->post('id');
	$result = getAssembly_helper($district);
	echo json_encode($result);
}

// function activate_applications1($id) {
// 	$response ['message'] = "fail";
// 	if (isset ( $id )) {
// 		$temp = $this->Base_Models->UpadateValue ( "department_application", array (
// 				"status" => 1
// 		), array (
// 				"application_id" => $id 
// 		) );
// 		if ($temp != 0) {
// 			$response ['message'] = "done";
// 			$response ['url'] = "";
// 		}
// 	}
// 	echo json_encode ( $response );
// }
// function deactivate_applications1($id) {
// 	$response ['message'] = "fail";
// 	if (isset ( $id )) {
// 		$temp = $this->Base_Models->UpadateValue ( "department_application", array (
// 				"status" => 0 
// 		), array (
// 				"application_id" => $id 
// 		) );
// 		if ($temp != 0) {
// 			$response ['message'] = "done";
// 			$response ['url'] = "./";
// 		}
// 	}
// 	echo json_encode ( $response );
// }


}

?>