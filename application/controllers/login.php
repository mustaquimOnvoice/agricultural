<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class login extends Base_Controller {
	function __construct() {
		parent::__construct ();
	}
	function index() {
		$this->load_header ();
		$this->load->view ( 'login_view' );
	}
	// Backend User Login
	function check_user_login() {
				
		$response ['message'] = "fail";
		$_POST = $this->formatFormValues ( $_POST ['data'] );
		
		if (isset ( $_POST ['loginUsername'] ) && $_POST ['loginUsername'] != null && isset ( $_POST ['loginPassword'] ) && $_POST ['loginPassword'] != null) {
			$temp_array = array ();
			if (is_numeric ( $_POST ['loginUsername'] )) {
				$temp_array = array (
						"user_mobile_no" => $_POST ['loginUsername'],
						"user_password" => $_POST ['loginPassword'],
						"user_status" => 1
				);				
			} else {
				$temp_array = array (
						"user_name" => $_POST ['loginUsername'],
						"user_password" => $_POST ['loginPassword'],
						"user_status" => 1
				);				
			}
			$data = $this->Base_Models->GetAllValues ( "users", $temp_array );
			
			if (count ( $data ) > 0) {
				if ($data [0] ['user_type'] == 0) {
					$response ['message'] = "done";
					$_SESSION ['role'] = "Admin";
					$_SESSION ['id'] = $data [0] ['department_user_id'];					
					$_SESSION ['name'] = $data [0] ['user_name'];
					$response ['url'] = 'dashboard';
				} elseif ($data [0] ['user_type'] == 1) {
					$response ['message'] = "done";
					$_SESSION ['role'] = "Other";
					$_SESSION ['id'] = $data [0] ['department_user_id'];					
					// $_SESSION ['department_id'] = $data [0] ['department_id'];
					$_SESSION ['department_id'] = $data [0] ['department_user_id'];
					$_SESSION ['name'] = $data [0] ['user_name'];
					$response ['url'] = 'dashboard';
				}
			} 
		}
		echo json_encode ( $response );
	}
	

	
}
?>