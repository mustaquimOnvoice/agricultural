<?php

/** PHPExcel root directory */
if (!defined('PHPEXCEL_ROOT')) {
    /**
     * @ignore
     */
    define('PHPEXCEL_ROOT', dirname(__FILE__) . '/../../');
    require(PHPEXCEL_ROOT . 'PHPExcel/Autoloader.php');
}

/**
 * PHPExcel_Calculation_TextData
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * @category    PHPExcel
 * @package        PHPExcel_Calculation
 * @copyright    Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license        http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt    LGPL
 * @version        ##VERSION##, ##DATE##
 */
class PHPExcel_Calculation_TextData
{
    private static $invalidChars;

    private static function unicodeToOrd($c)
    {
        if (ord($c{0}) >=0 && ord($c{0}) <= 127) {
            return ord($c{0});
        } elseif (ord($c{0}) >= 192 && ord($c{0}) <= 223) {
            return (ord($c{0})-192)*64 + (ord($c{1})-128);
        } elseif (ord($c{0}) >= 224 && ord($c{0}) <= 239) {
            return (ord($c{0})-224)*4096 + (ord($c{1})-128)*64 + (ord($c{2})-128);
        } elseif (ord($c{0}) >= 240 && ord($c{0}) <= 247) {
            return (ord($c{0})-240)*262144 + (ord($c{1})-128)*4096 + (ord($c{2})-128)*64 + (ord($c{3})-128);
        } elseif (ord($c{0}) >= 248 && ord($c{0}) <= 251) {
            return (ord($c{0})-248)*16777216 + (ord($c{1})-128)*262144 + (ord($c{2})-128)*4096 + (ord($c{3})-128)*64 + (ord($c{4})-128);
        } elseif (ord($c{0}) >= 252 && ord($c{0}) <= 253) {
            return (ord($c{0})-252)*1073741824 + (ord($c{1})-128)*16777216 + (ord($c{2})-128)*262144 + (ord($c{3})-128)*4096 + (ord($c{4})-128)*64 + (ord($c{5})-128);
        } elseif (ord($c{0}) >= 254 && ord($c{0}) <= 255) {
            // error
            return PHPExcel_Calculation_Functions::VALUE();
        }
        return 0;
    }

    /**
     * CHARACTER
     *
     * @param    string    $character    Value
     * @return    int
     */
    public static function CHARACTER($character)
    {
        $character = PHPExcel_Calculation_Functions::flattenSingleValue($character);

        if ((!is_numeric($character)) || ($character < 0)) {
            return PHPExcel_Calculation_Functions::VALUE();
        }

        if (function_exists('mb_convert_encoding')) {
            return mb_convert_encoding('&#'.intval($character).';', 'UTF-8', 'HTML-ENTITIES');
        } else {
            return chr(intval($character));
        }
    }


    /**
     * TRIMNONPRINTABLE
     *
     * @param    mixed    $stringValue    Value to check
     * @return    string
     */
    public static function TRIMNONPRINTABLE($stringValue = '')
    {
        $stringValue    = PHPExcel_Calculation_Functions::flattenSingleValue($stringValue);

        if (is_bool($stringValue)) {
            return ($stringValue) ? PHPExcel_Calculation::getTRUE() : PHPExcel_Calculation::getFALSE();
        }

        if (self::$invalidChars == null) {
            self::$invalidChars = range(chr(0), chr(31));
        }

        if (is_string($stringValue) || is_numeric($stringValue)) {
            return str_replace(self::$invalidChars, '', trim($stringValue, "\x00..\x1F"));
        }
        return null;
    }


    /**
     * TRIMSPACES
     *
     * @param    mixed    $stringValue    Value to check
     * @return    string
     */
    public static function TRIMSPACES($stringValue = '')
    {
        $stringValue = PHPExcel_Calculation_Functions::flattenSingleValue($stringValue);
        if (is_bool($stringValue)) {
            return ($stringValue) ? PHPExcel_Calculation::getTRUE() : PHPExcel_Calculation::getFALSE();
        }

        if (is_string($stringValue) || is_numeric($stringValue)) {
            return trim(preg_replace('/ +/', ' ', trim($stringValue, ' ')), ' ');
        }
        return null;
    }


    /**
     * ASCIICODE
     *
     * @param    string    $characters        Value
     * @return    int
     */
    public static function ASCIICODE($characters)
    {
        if (($characters === null) || ($characters === '')) {
            return PHPExcel_Calculation_Functions::VALUE();
        }
        $characters    = PHPExcel_Calculation_Functions::flattenSingleValue($characters);
        if (is_bool($characters)) {
            if (PHPExcel_Calculation_Functions::getCompatibilityMode() == PHPExcel_Calculation_Functions::COMPATIBILITY_OPENOFFICE) {
                $characters = (int) $characters;
            } else {
                $characters = ($characters) ? PHPExcel_Calculation::getTRUE() : PHPExcel_Calculation::getFALSE();
            }
        }

        $character = $characters;
        if ((function_exists('mb_strlen')) && (function_exists('mb_substr'))) {
            if (mb_strlen($characters, 'UTF-8') > 1) {
                $character = mb_substr($characters, 0, 1, 'UTF-8');
            }
            return self::unicodeToOrd($character);
        } else {
            if (strlen($characters) > 0) {
                $character = substr($characters, 0, 1);
            }
            return ord($character);
        }
    }


    /**
     * CONCATENATE
     *
     * @return    string
     */
    public static function CONCATENATE()
    {
        $returnValue = '';

        // Loop through arguments
        $aArgs = PHPExcel_Calculation_Functions::flattenArray(func_get_args());
        foreach ($aArgs as $arg) {
            if (is_bool($arg)) {
                if (PHPExcel_Calculation_Functions::getCompatibilityMode() == PHPExcel_Calculation_Functions::COMPATIBILITY_OPENOFFICE) {
                    $arg = (int) $arg;
                } else {
                    $arg = ($arg) ? PHPExcel_Calculation::getTRUE() : PHPExcel_Calculation::getFALSE();
                }
            }
            $returnValue .= $arg;
        }

        return $returnValue;
    }


    /**
     * DOLLAR
     *
     * This function converts a number to text using currency format, with the decimals rounded to the specified place.
     * The format used is $#,##0.00_);($#,##0.00)..
     *
     * @param    float    $value            The value to format
     * @param    int        $decimals        The number of digits to display to the right of the decimal point.
     *                                    If decimals is negative, number is rounded to the left of the decimal point.
     *                                    If you omit decimals, it is assumed to be 2
     * @return    string
     */
    public static function DOLLAR($value = 0, $decimals = 2)
    {
        $value        = PHPExcel_Calculation_Functions::flattenSingleValue($value);
        $decimals    = is_null($decimals) ? 0 : PHPExcel_Calculation_Functions::flattenSingleValue($decimals);

        // Validate parameters
        if (!is_numeric($value) || !is_numeric($decimals)) {
            return PHPExcel_Calculation_Functions::NaN();
        }
        $decimals = floor($decimals);

        $mask = '$#,##0';
        if ($decimals > 0) {
            $mask .= '.' . str_repeat('0', $decimals);
        } else {
            $round = pow(10, abs($decimals));
            if ($value < 0) {
                $round = 0-$round;
            }
            $value = PHPExcel_Calculation_MathTrig::MROUND($value, $round);
        }

        re�� ~amd64~~ 17134.40 7.1.6" u pdate="4 467702-8 638_neut ral_PACK AGE"/>
  <Instal l packag zP_for_ RollupFi x~31bf38 56ad364e�35'�9~�40A�1A2� ��43� �� 4� � 5� _��<46_����X�47�___Y��������4��__Y5��_�_Z��__Z��_�_Z��_/�z�����5�Q����������/��/�5�Q////�_��_�Q�5�Q////�//"�Q////�//"�Q////�//"�Q////�//!6�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q/////-�� 702-8669 _neutral _PACKAGE "/>
  <I nstall p ackage="P _for_R ollupFix ~31bf385 6ad364e3 5~amd64~ ~17134.4 07.1.6"  updat �44�67�70�1A�2A3� ��4� � �5� � 6� � 7_�������8___Y￣������8��__�Z��__Z��__�Z��__Z��__�/�z8�Q���������������8�Q///�/�/�/�Q�8�Q//�////"�Q//�////"�Q//�////!9�Q/�/////"�Q/�/////"�Q/�/////"�Q/�/////"�Q/�/////"�Q/�/////"�Q/�/////"�Q/�/////"�Q/�/////"�Q/?///// 70�Q'��  <Insta ll packa ge="P@_f or_Rollu pFix~31b f3856ad3 64e35~am d64~~171 34.407.1 .6" updat �446770 2-8701_n eutral_P ACKAGE"/\>
�_�2�3�A4A5� ��6� �� 7� � 8� _���9__�_�X�10___Y�����1���__Z��__Z���__Z��__Z��Q�z��������4�����1�Q////�/��/�!�1�Q////�//"�Q////�//!2�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///!3�Q//�////"�Q//&�� RollupFi x~31bf38 56ad364e 35~amd64 ~~17134. 407.1.6"  update= "4467702 -8732_ne utral_PA CKAGE"/>
  <Inst all packag zP_foTr_A�3�4A5mA6� ��7� � 8;� � 9� _��40�_��_�X�41__�_Y������4���__Z��__Z���__Z��__Z���/�z�������4�Q���������/�4/�.��4�Q///_�_�_��Q�5�Q/////�/"�Q/////�/"�Q/////�/"�Q/////�/"�Q/////�/"�Q/////�/"�Q/////�/"�Q/////�/"�Q/////�/"�Q/////�/!6�Q////�//"�Q////�//"�Q////%�� 7134.407 .1.6" up date="44 67702-87 63_neutr al_PACKA�GE"/>
   <Install� packag zP_for_R ollupFix ~31bf385 6ad364e3 5~amd64~T~1�4�5A6mA7� ��8� � 9�� � 70� � 1_�����X�72___�Y�������7��_�_Z��__Z��_�_Z��__Z��_�/�z����7�Q�����������4��.�8�Q////�/�_�Q�8��Q//////"��Q//////"��Q//////"��Q//////"��Q//////"��Q//////"��Q//////"��Q//////"��Q//////!�9�Q//////�"�Q//////�"�Q//////�"�Q//////"4_�� neutral_ PACKAGE"/>
  <In stall pa ckage="P _for_Ro llupFix~ 31bf3856 ad364e35 ~amd64~~ 17134.40 7.1.6" updat �446 7702-879T5_~�6A7A8� ��9� � 800ۿ � 1� � 2_�������803___�Y4___Y������80��__Z���__Z��__Z���__/�z81�Q�����������4�����81�Q////�/��/� �81�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///!2�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//////"�Q/s�� tall pac�kage="P� _for_Rol lupFix~3 1bf3856a d364e35~ amd64~~1 7134.407 .1.6" updat �4467 702-8826 _neutral _PACKAGE "/>
  <ITns[�7�8A9�~A30� ��31� �� 2� � 3� _��<34_��_�X�35�___Y�������3��__Z��__�Z��__Y4��_�_Z�Q�z�z�������4����4�Q///�/�/�/�!�4�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////!5�Q/�/////"�Q/�/////"�Q/�/////"�Q/�/////"�Q/�/////"�Q/�/////"�Q///�� ~31bf385 6ad364e3 5~amd64~ ~17134.4 07.1.6"  update=" 4467702- 8857_neu tral_PAC KAGE"/>
  <Insta ll packa
g zP_for _RollupFTix8�8�9~A6�0A1� ��62� �� 3� � 4� _��<65_����X�66�___Y��������6��__Z��__�Y7��__Z��_�_Z��/�z�z������7�Q��������/��/�/�7�Q////��_�_�Q�7�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///!8�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q////.�� 1.6" upd ate="446@7702-8  _ neutral_ PACKAGE"/>
  <In stall paPckag zP_ for_Roll upFix~31 bf3856ad 364e35~a md64~~17 134.407.��9~�90A1Af2� ��93� � 4ۿ � 5� � 6_�������97___Y�8___Y������90��__Z���__Z��__Z���_/�z�z�90�Q������������.�9�0�Q////�/�_��P�90�Q////�//"�Q////�//"�Q////�//"�Q////�//!1�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q//////"�Q�� ACKAGE"/>
  <Ins tall pac�kage="P  _for_Rol lupFix~3 1bf3856a d364e35~ amd64~~1 7134.407 .1.6" updat �4467 702-8920 _neutralT_Pu�1�2A3ۿ ��4� � 5� � �6� � 7_�������8___Y9__{_X3�����3���__Z��__Z���__Z��__/��z3�Q���������������3�Q////��/�/�!�3�Q///�///"�Q///�///"�Q///�///!4�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////!5�Q/'�� age="Pack �_for_R ollupFix ~31bf385 6ad364e3 5~amd64~ ~17134.4 07.1.6"  updat �44 67702-89 51_neutr al_PACKA�GE"/>
   <Install� p�O�2�3A�4A5� ��6� � v7� � 8� _��9�_��_�X�60__�_Y������6���__Z��__Z���__Z��__Z�Q�z�z���������6��Q//////"��Q//////"��Q//////!�7�Q//////�"�Q//////�"�Q//////�"�Q//////�"�Q//////�"�Q//////�"�Q//////�"�Q//////�"�Q//////�"�Q//////�!8�Q//////"�Q///&�� ad364e35 ~amd64~~ 17134.40 7.1.6" u pdate="4 467702-8 982_neut ral_PACK AGE"/>
  <Instal l packag zP_for_ RollupFi x~31bf38T56/�3�4A5mA6� ��7� � 8;� � 9� _��90�_����X�91__�_Y�������9���__Z��__Z���__Z��__Z���/�z�z�����9�Q�����������4/�.��9�Q////�_�_��2-900�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////!1�Q/�/////"�Q/�/////"�Q/////%�� te="4467 702-9013 _neutral _PACKAGE "/>
  <I nstall p�ackag �P _for_Rol lupFix~3 1bf3856a d364e35~ amd64~~1 7134.407 .1.6" upTda�4�5A6mA7� ��8� � 9�� � 20� � 1_�������22___�Y��������2��_�_Z��__Z��_�_Z��__Z��_�_/��2�Q�����������4����3�Q/�///�/�/�Q�3�Q�//////"�Q�//////"�Q�//////"�Q�//////"�Q�//////"�Q�//////"�Q�//////"�Q�//////"�Q//////!4��Q//////"��Q//////"��Q//////"��Q//////"�Q/>��
  <Inst all pack@age="P@_ for_Roll upFix~31 bf3856ad 364e35~a md64~~17 134.407. 1.6" updat �44677 02-9045_ neutral_ PACKAGE"T/>l�6�7A8�A9� ��50� � �1� � 2_�������53___Y4___Y�����5���__Z��__Z���__Z��__Y~6�Q�z��������4�����6�Q////��/�/�!�6�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///!7�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q//�////"�Q// k�� age_for_ RollupFi x~31bf38 56ad364e 35~amd64 ~~17134. 407.1.6"  update= "4467702 -9076_ne utral_PA CKAGE"/>
  <Inst all pack� �="PF�7��8A9~A80� ��l81� � 2� � 3� _��84_��_��X�85___Y�������8��__Z���__Z��__Y�9��__Z��/�z��������9�Q���������/�4/�.�9�Q///_�_�_�Q�9��Q//////"��Q//////"��Q//////"��Q//////"��Q//////"��Q////// �10�Q/////�/"�Q/////�/"�Q/////�/"�Q/////�/"�Q/////�/"�Q//////"�Q////�� amd64~~1 7134.407 .1.6" up date="44 67702-91 07_neutr al_PACKA�GE"/>
   <Install� packag zP_for_R ollupFix ~31bf385 6ad364e3T5~&�8�9~A1�0A1� ��12� �� 3� � 4� _��<15_����X�16�___Y��������1��__Z��__�Y2��__Z��_�_Z��_/�z�����2�Q����������/��/�2�Q////�/��_�Q�2�Q////�//"�Q////�//"�Q////�//"�Q////�//"�Q////�//!3�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q///�///"�Q/////.                                                               � 02-9138_ neutral_ PACKAGE"/>
  <In stall pa ckage="P _for_Ro llupFix~ 31bf3856 ad364e35 ~amd64~~ 17134.40 7.1.6" updat �446�77�9~�40A�1A2� ��43� m� 4� � 5� � 6�_������47__�_Y��������4���__Y5��__Z���__Z��__Z߿�__/�z5�Q���������������5�Q////�/�/�Q�5��Q//////"��Q//////"��Q//////"��Q//////!�6�Q//////�"�Q//////�"�Q//////�"�Q//////�"�Q//////�"�Q//////�"�Q//////"�Q//"7969�jKBdooe0.�0_�Q�192fA��_���_797�Or�4@������@��� ge="Pack age_798_ for_KB44 67702~31 bf3856ad 364e35~a md64~~10 .0.1.6" `updat �b- 1922_neu tral"/>
  <Insta�ll p�N�31y�9�y4�<800�<�5�<1�<6q2(927q392�8q4929qb5930?�y8�06?>�31?/�7??�n�80?�m��3?�/1?�?3�?�0�j?3�j0�j�?3�j0�j?4�j�0�j?4�j�1���������4�yԁ��5��81_5���_5��B�7�82_5����_5���y����y?���G���="��5_5��_5??};6_5��c_�4�?96_5��������6�j��4_5����7��� �&&��8c��￧??;9&���_r���&��_r����&���<����c��_r����c���<�<�<�<~9c��_�??t e%� ="446770 2-1996_n eutral"/>
  <Ins tall pac�kage="P  _868_for_KB�~31b f3856ad3 64e35~am d64~~10. 0.1.6" u�pdat ��71yJ9Ay8�<70�<9��<1�<2000�<�2001q3(002q400�3q5004n2�y�y L5?.�7>6<� �o�77�??�0����`��0��?0�/8���?1�0��?1��0��?1�0�����y201?Lԁ�����8�S�������?L��=�8�S���?L���?L���?L���?L���?L���S��4���p?L���S??<?L��9�?L���2�D���?L����D��?L�����D��?L�ߟ��D��9 ���w�Ϳ-�-3���1��<??;4_5����<�<�<�204_5�������5����������<�������&��3Q5"� r_KB4467 702~31bf 3856ad36 4e35~amd 64~~10.0 .1.6" up@date="�- 2068_neu tral"/>
  <Insta ll packa
g jP_934�_fo>�91y5@yT70�<6�<1�<9%�<4p4007�5q1076qR307q�4�2<07�j�y??�08�j?�p�8�j0?�ٿ�82?L��4?�?l83?/7[>�8�?��-8???�0��m�-8?�/5��?8�7�<~5�S�������8?Lԁ����52�?�4?�9�8?L�=��5[??;9?L���[���?L�������9?L����S??<?L��?L�����D��?L����6�<�_5W5?L�����D?=6?L�����D��?L�����D��?L����10�D��������S�S11�-��� n��������?�-�-�_쟁[�14�-??�S�S�S�S1�4�-�������-�� tral"/>
  <Insta ll packa ge="P _9 86_for_K B4467702 ~31bf385 6ad364e3 5~amd64~ ~10.0.1. 6" updat �b-2150_�neur�1vy2�<�3�<4t�5tv6t7?�y8�??�?�0�9???m/6���7����6��.1003_^�9��Q4__��Q~5__ߘQ?�?�9���Q�L_9�L$��a�1030��4���="��21_=�����������22o=?��__�_�Z�22�=������=�������=�������=�������=�������=���������������/���������L��4�c����4o&�������o&���k���o&ۯ�8�k�k4�k�k�4o&��9���9I�24o&��5%� 0_for_KB 4467702~ 31bf3856 ad364e35 ~amd64~~ 10.0.1.6 " update="�-2250 _neutral "/>
  <I nstall p�ackag jP�_1051A�12z�2Az22=3A=31=\66@= �z7�22 �7=277i1\78�2 p=99?�22�_�z10�_^�3�_O�z_�0�z��10�z_���4�L}�304;?\n�1�����30�5_P5��6_�_���30_��L���L08�_ ���㯂��309�󂯂}��1 ����1�_�����9o&���	�31_��=�����5��31�L����
_����c�__\�L���c�����L��?\����/E���T����/E���L���/E��������2=���=���=�������=���2����=���/E���=�������_����� 6� 4~~10.0. 1.6" upd ate="446 7702-232 7_neutra@l"/>
  < Install @packag jP_1122_f or_KB�~3 1bf3856a d364e35~Pamd6�82z3%Az91=31@=44%w=5�47=37�6�02��7r�= �7r�6�240�r�_�z410=O��64�L]�411_mP5__2_P6{__3__�k��461����6_���41Ͽ�__���41��Q�8==��Q9��z�4����41�L䂯����7�L����Z�4���_����_�_����42�5_�_�����42�5���T��5���T�����5���T�����5���L����5���/E������y�80/E/E/E��4��=�=w=8�_�_[3�z__�ϯ���/E���T����/E��/E����/E��/E���/E���/E���/E�?�/E���"E� tral"/>
  <Insta ll packa ge="P _1 188_for_ KB446770 2~31bf38 56ad364e 35~amd64 ~~10.0.1 .6" updat �b-2439P_neu.�9@z4R01z90A=12=1A=22=2�443E�3�444�4ѿ445�5�@�Ͳ6_�z47_O��97__8_P��m��4���9��_5���O20��_5���Q��_5��Q��_�5��Q��_5��Q�����z5�L䂯���<20�L������56�=����L�����L_�20�L�����L���L����L���L���6��L��1�L�����L���L����L����L����L����L����L����L����L���L�����L���L�����L���L�����L�������L���T���7��L���T����L�� � "Package _1219_fo r_KB4467 702~31bf 3856ad36 4e35~amd 64~~10.0 .1.6" up@date="b- 2472_neu tral"/>
  <InstaPll p�=�2�0Az32z1A=4w=�5w=6���7��68��9��80�_�z�k_�81__{����8������8￙_�q�8��__�R��__R��__�R��__R�L4{�z���2.������8�L����2�z_�4_��Y�9�L��3����
�9�L��5�����/E��7�����=���o&�o&k&9�5��3.����.��o&���50o&��o&����o&��o&�߯�o&��8..�..506_���T��T3.__\._�^4.���.�߯.���1.���.���.���.���.��o&�� d364e35~ amd64~~1 0.0.1.6"  update= "4467702 -2517_ne utral"/>
  <Inst all pack�ag jP_1 l�_for_KB� ~31bf385T6a)�92z3@z2�12=5A=32=7A=J5�9�52��6R1�52��6��2�53��6��@�L�z�6�L_�3�LQ�L_f3�LP70�L�3�8�k�_�53?\Ϳ2���40��?_Q�k����42������z��T�z󯂫�44_󂯂���7����/E�����?\���/E���7?\���=���?\���5�5��?\���.��8�o&���o&��o&����o&��o&����o&��o&���� ����=w=8o&����o&_^9o&����o&��o&����o&��o&����o&��o&����0ho&�o&g&9o&����o&_]30o&����� 702-2575 _neutral "/>
  <I nstall p ackage="P _1302_ for_KB4467 �~31bf 3856ad36 4e35~amd 64~~10.0 .1.6" up�dat �b�6wz*7wz8w=9v=80ɿ02��81��v2��3�_�z8�4__�=P�8������L��8��__R���__R��__R���__Q9��__R���__�259�L�䂯��������������9�L�_�_�_�_�[��9�L��������L�������L��������L��������L���3ϰϰ4ϰ="ǰ�9�L��4����y�60�L����
60�L��������L��5��L��6������L��7������L��8�����6���k�k��o&o&l&�L__�����L�
� all pack@age="P�_ 1308_for _KB44677 02~31bf3 856ad364 e35~amd6 4~~10.0. 1.6" updat �b-260 9_neutra@l"/>
  <�InstY�102zr9�z111= X?�1�22=1=613��2�614�3�(615�4�61�6�5�617_�O�`%?\]�18_P�6���1����4�m�2��Q7��2￙_����2��__�R��__R��Q�����2�L4{�z��p1.ӯ���="��2�L��^�p1�����L�߯Os�=�2�L��������L����z���3�L��������L��������L��������L��������L��������L��������L��������L��������L��������L�������4�L�������L���� 02~31bf3 856ad364 e35~amd6 4~~10.0. 1.6" upd ate="446 7702-264 2_neutra@l"/>
  < Install @packag jP_1325_f`or_KB�3�3�wz4w