<section class="form">
	<div class="container-fluid">
		<div class="row">
			<!-- Basic Form-->
			<div class="col-lg-12">
				<div class="card">
					<div class="container" style="padding: 25px;">
						<h2><?php echo $action_title;?></h2>
					</div>
				
					<div class="container" style="padding: 25px;">
						<div class='edit-container'>
							<form id="data-form" action="" method="post">
							
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> First name :</label> 
									<input class="form-control"  type="text" id="fname" name="fname" value="<?php echo @$user_id == null ? "" : "$fname"?>" placeholder="First Name" required="" />
								</div>
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> Last name :</label> 
									<input class="form-control"  type="text" id="lname" name="lname" value="<?php echo @$user_id == null ? "" : "$lname"?>" placeholder="Last Name" required="" />
								</div>
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> Mobile number :</label> 
									<input class="form-control"  type="text" id="user_mobile_no" name="user_mobile_no" value="<?php echo @$user_id == null ? "" : "$user_mobile_no"?>" placeholder="Mobile number" required="" />
								</div>
								
								<div class="form-group">
									<label class="form-label"> Email:</label> 
									<input class="form-control"  type="text" id="email" name="email" value="<?php echo @$user_id == null ? "" : "$email"?>" placeholder="Email"/>
								</div>
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> Aadhar no:</label> 
									<input class="form-control"  type="text" id="adhar_no" name="adhar_no" value="<?php echo @$user_id == null ? "" : "$adhar_no"?>" placeholder="Aadhar number" required="" />
								</div>
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> Address :</label> 
									<input class="form-control"  type="text" id="user_address" name="user_address" value="<?php echo @$user_id == null ? "" : "$user_address"?>" placeholder="Address" required="" />
								</div>
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> City :</label> 
									<input class="form-control"  type="text" id="user_city" name="user_city" value="<?php echo @$user_id == null ? "" : "$user_city"?>" placeholder="City" required="" />
								</div>
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> District :</label> 
									<input class="form-control"  type="text" id="user_district" name="user_district" value="<?php echo @$user_id == null ? "" : "$user_district"?>" placeholder="City" required="" />
								</div>
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> State :</label> 
									<input class="form-control"  type="text" id="user_state" name="user_state" value="<?php echo @$user_id == null ? "" : "$user_state"?>" placeholder="State" required="" />
								</div>								
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> pin :</label> 
									<input class="form-control"  type="text" id="pin" name="pin" value="<?php echo @$user_id == null ? "" : "$pin"?>" placeholder="Pincode" required="" />
								</div>								
								
								<button id="data-form-btn" data-url="<?php echo $action;?>" class="btn btn-outline-success">SAVE</button>
								<a href="<?php echo $cancle;?>" class="btn btn-outline-primary">Back</a>
								<div id="login-btn-loding"></div>
							</form>
							
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>