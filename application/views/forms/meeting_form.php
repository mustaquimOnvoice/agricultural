<section class="form">
	<div class="container-fluid">
		<div class="row">
			<!-- Basic Form-->
			<div class="col-lg-12">
				<div class="card">
					<div class="container" style="padding: 25px;">
						<h2><?php echo $action_title;?></h2>
					</div>
				
					<div class="container" style="padding: 25px;">
						<div class='edit-container'>
							<form id="data-form" action="" method="post">
							
								<div class="form-group">
									<label class="form-label">Title :</label> 
									<input class="form-control"  type="text" id="title" name="title" value="<?php echo @$id == null ? "" : "$title"?>" placeholder="Title" required="" />
								</div>
								<div class="form-group">
									<label class="form-label">Mom :</label> 
									<textarea class="form-control" rows="5" id="mom" name="mom" required=""><?php echo @$id == null ? "" : "$mom"?></textarea>
								</div>
								<div class="form-group">
									<label class="form-label">Next meeting :</label> 
									<input class="form-control"  type="text" id="next_meeting" name="next_meeting" value="<?php echo @$id == null ? "" : "$next_meeting"?>" placeholder="Next meeting" required="" />
								</div>
								
								<div class="form-group">
									<label class="form-label">Attendee :</label> 
									<input class="form-control"  type="text" id="attendee" name="attendee" value="<?php echo @$id == null ? "" : "$attendee"?>" placeholder="Attendee" required="" />
								</div>
								
								<div class="form-group">
									<label class="form-label">Location :</label> 
									<input class="form-control"  type="text" id="location" name="location" value="<?php echo @$id == null ? "" : "$location"?>" placeholder="Location" required="" />
								</div>
								
								<div class="form-group">
									<label class="form-label">User Mobile number :</label> 
									<input class="form-control"  type="number" id="user_mobile_no" name="user_mobile_no" value="<?php echo @$id == null ? "" : "$user_mobile_no"?>" placeholder="User Mobile Number" required="" />
								</div>
								
								
								<button id="data-form-btn" data-url="<?php echo $action;?>" class="btn btn-outline-success">SAVE</button>
								<a href="<?php echo $cancle;?>" class="btn btn-outline-primary">Back</a>
								<div id="login-btn-loding"></div>
							</form>
							
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>