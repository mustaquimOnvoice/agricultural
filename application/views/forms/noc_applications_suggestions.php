<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom"><?php echo isset($sub_title)?$sub_title:(isset($title)?$title:"Title");?></h2>
		</div>
	</header>
	<ul class="breadcrumb">
		<div class="container-fluid">
			<li class="breadcrumb-item"><a
				href="<?php echo base_url('/dashboard')?>">Home</a></li>
			<li class="breadcrumb-item active"><?php echo isset($title)?$title:"Title";?></li>
			<?php if (isset($sub_title)){?>
			<li class="breadcrumb-item active"><?php echo isset($title)?$sub_title:"Title";?></li>
			<?php }?>
		</div>
	</ul>
	<section class="form">
		<div class="container-fluid">
			<div class="row">
				<!-- Basic Form-->
				<div class="col-lg-12">
					<div class="card">
						<div class="container" style="padding: 25px;">
							<div class='edit-container'>
								<form class="POST_AJAX"
									action="<?php echo isset($action)?$action:""?>" method="post">
								
									<div class="form-group">
										<label class="form-label">Mandal Name</label> <input
											class="form-control"  type="text"
											id="mandal_name" name="mandal_name" 
											value="<?php echo (isset($mandal_name))?$mandal_name:""; ?>"  readonly />
									</div>
									<div class="form-group">
										<label class="form-label">Mandal Adheksh</label> <input
											class="form-control"  type="text"
											id="mandal_adheksh" name="mandal_adheksh" 
											value="<?php echo (isset($mandal_adheksh))?$mandal_adheksh:""; ?>"  readonly />
									</div>
									<div class="form-group">
										<label class="form-label">Registration No</label> <input
											class="form-control"  type="text"
											id="registration_no" name="registration_no"
											value="<?php echo (isset($registration_no))?$registration_no:""; ?>" readonly />
									</div>

                                    <div class="form-group">
										<label class="form-label">Area</label> <input
											class="form-control"  type="text"
											id="area" name="area"
											value="<?php echo (isset($area))?$area:""; ?>" readonly  />
									</div>

                                    <div class="form-group">
										<label class="form-label">Road</label> <input
											class="form-control"  type="text"
											id="road" name="road"
											value="<?php echo (isset($road))?$road:""; ?>" readonly />
									</div>

									 <div class="form-group">
										<label class="form-label">Depth</label> <input
											class="form-control"  type="text"
											id="depth" name="depth"
											value="<?php echo (isset($depth))?$depth:""; ?>" />
									</div>


 									<div class="form-group">
										<label class="form-label">Width</label> <input
											class="form-control"  type="text"
											id="width" name="width"
											value="<?php echo (isset($width))?$width:""; ?>" />
									</div>

 									<div class="form-group">
										<label class="form-label">Start Date</label> <input
											class="form-control"  type="text"
											id="start_date" name="start_date"
											value="<?php echo (isset($start_date))?$start_date:""; ?>" readonly />
									</div>
									<div class="form-group">
									<label class="form-label">End Date</label> <input
											class="form-control"  type="text"
											id="end_date" name="end_date"
											value="<?php echo (isset($end_date))?$end_date:""; ?>" readonly />
									</div>
									<div class="form-group">
									<label class="form-label">Police station</label> <input
											class="form-control"  type="text"
											
											value="<?php echo (isset($police_station))?$police_station:""; ?>" readonly />
									</div>
									<div class="form-group">
									<label class="form-label">Suggestions</label> <input
											class="form-control"  type="text"
											id="suggestions" name="suggestions" 
											value="<?php echo (isset($suggestions))?$suggestions:""; ?>" <?php echo (isset($_SESSION['department_id']))?"readonly":"" ?> />
									</div>
									<?php if(isset($_SESSION['department_id'])){ ?>
										<div class="form-group">
																		<label class="form-label">Suggestions</label> <input
																				class="form-control"  type="text"
																				id="suggestions" name="suggestions" 
																				value=""  />
																		</div>
									<?php } ?>

									
									<button value="save_html_data" ajax_events=true
										class="btn btn-outline-primary">SAVE</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
function validatation(){
	return true;
}
</script>