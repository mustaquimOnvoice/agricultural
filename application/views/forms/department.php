<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom"><?php echo isset($sub_title)?$sub_title:(isset($title)?$title:"Title");?></h2>
		</div>
	</header>
	<ul class="breadcrumb">
		<div class="container-fluid">
			<li class="breadcrumb-item"><a
				href="<?php echo base_url('/dashboard')?>">Home</a></li>
			<li class="breadcrumb-item active"><?php echo isset($title)?$title:"Title";?></li>
			<?php if (isset($sub_title)){?>
			<li class="breadcrumb-item active"><?php echo isset($title)?$sub_title:"Title";?></li>
			<?php }?>
		</div>
	</ul>
	<section class="form">
		<div class="container-fluid">
			<div class="row">
				<!-- Basic Form-->
				<div class="col-lg-12">
					<div class="card">
						<script
							src="https://cdnjs.cloudflare.com/ajax/libs/plupload/2.1.8/plupload.full.min.js"></script>
						<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
						<script type="text/javascript">
						//Some code from - http://www.bennadel.com/blog/2564-using-multiple-dropzones-and-file-inputs-with-a-single-plupload-instance.htm 
						</script>
						<div class="container" style="padding: 25px;">
							<div class='edit-container'>
								<form class="POST_AJAX"
									action="<?php echo isset($action)?$action:""?>" method="post">
								
									<div class="form-group">
										<label class="form-label"> Department Name </label> <input
											class="form-control" required="required" type="text"
											id="department_name" name="department_name"
											value="<?php echo (isset($department_name))?$department_name:""; ?>" />
									</div>
									<div class="form-group">
										<label class="form-label">Department Notice</label>
										<textarea name="department_notice" id="tinymceTextarea"
											class="use-tinymce"><?php echo (isset($department_notice))?$department_notice:""; ?></textarea>
									</div>
									
									
									<button value="save_html_data" ajax_events=true
										class="btn btn-outline-primary">SAVE</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
function validatation(){
	return true;
}
var PluploadHandler = function( $, plupload ) {
	var self = this;
	this.plupload = plupload;
	// Custom example logic
	this.uploader = new plupload.Uploader({
		runtimes : 'html5,flash,silverlight,html4',
		browse_button : document.getElementById('mybutton'),
		url : '<?php echo base_url("blogs/upload_image/blog_images") ?> ',	
		flash_swf_url : '/plupload-2.1.2/js/Moxie.swf',
		silverlight_xap_url : '/plupload-2.1.2/js/Moxie.xap',
		drop_element : "dropFilesHere", 
		filters : {
			max_file_size : '10mb',
			mime_types: [
				{title : "Image files", extensions : "jpg,jpeg,gif,png"}
			]
		},
		init: {
			PostInit: function() {
				$('.filelist').html('');
			},
			Error: function(up, err) {
				console.log("\nError #" + err.code + ": " + err.message);
			}
		}
	});
	this.uploader.init();
	this.uploader.bind("FilesAdded", handlePluploadFilesAdded);
	this.uploader.bind("FileUploaded", handlePluploadFileUploaded);
	function handlePluploadFilesAdded(up, files) {
		//console.log("+ handlePluploadFilesAdded");
		up.start();
	}
	function handlePluploadFileUploaded(up, file, res) {
		var img = "<img src='" + res.response + "'>";
		tinymce.activeEditor.execCommand('mceInsertContent', false, img);
	}
}
initTinymce();
</script>