<section class="form">
	<div class="container-fluid">
		<div class="row">
			<!-- Basic Form-->
			<div class="col-lg-12">
				<div class="card">
					<div class="container" style="padding: 25px;">
						<div class='edit-container'>
							<form action="<?php echo $action;?>" method="post" id="from_add_message" name="from_add_message">
							
								<div class="form-group">
									<label class="form-label">Message</label> 
									<input class="form-control"  type="text" id="message" name="message" value="<?php echo @$id == null ? "" : "$message"?>" placeholder="Message" required />
								</div>
								
								<div class="form-group">
									<label for="priority">Priority:</label>
									<select class="form-control" id="priority" name="priority" required>
										<option value="Major" <?php if ($priority=="Major") { ?>selected="selected"<?php } ?> >Major</option>
										<option value="Minor" <?php if ($priority=="Minor") { ?>selected="selected"<?php } ?> >Minor</option>
									</select>
								</div>
								
								<div class="form-group">
									<label for="status">Status:</label>
									<select class="form-control" id="status" name="status" required>
										<option value="Active" <?php if ($status=="Active") { ?>selected="selected"<?php } ?> >Active</option>
										<option value="Inactive" <?php if ($status=="Inactive") { ?>selected="selected"<?php } ?> >Inactive</option>
									</select>
								</div>
								
								<button value="save_html_data" type="submit" id="add_message" class="btn btn-outline-primary">SAVE</button>
								<a href="<?php echo $cancle;?>" class="btn btn-outline-primary">Cancel</a>
							</form>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>