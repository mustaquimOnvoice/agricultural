<section class="form">
	<div class="container-fluid">
		<div class="row">
			<!-- Basic Form-->
			<div class="col-lg-12">
				<div class="card">
					<div class="container" style="padding: 25px;">
						<div class='edit-container'>
							<form id="user-form" action="" method="post">
							
								<div class="form-group">
									<label class="form-label">User Mobile Number :</label> 
									<input class="form-control"  type="number" id="user_mobile_no" name="user_mobile_no" value="<?php echo @$user_id == null ? "" : "$user_mobile_no"?>" placeholder="User mobile number" required="" />
								</div>
								
								<button id="add-user-btn" data-url="<?php echo $action;?>" class="btn btn-outline-primary">SAVE</button>
								<a href="<?php echo $cancle;?>" class="btn btn-outline-primary">Cancel</a>
								<div id="login-btn-loding"></div>
							</form>
							
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>