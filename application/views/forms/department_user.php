<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom"><?php echo isset($sub_title)?$sub_title:(isset($title)?$title:"Title");?></h2>
		</div>
	</header>
	<ul class="breadcrumb">
		<div class="container-fluid">
			<li class="breadcrumb-item"><a
				href="<?php echo base_url('/dashboard')?>">Home</a></li>
			<li class="breadcrumb-item active"><?php echo isset($title)?$title:"Title";?></li>
			<?php if (isset($sub_title)){?>
			<li class="breadcrumb-item active"><?php echo isset($title)?$sub_title:"Title";?></li>
			<?php }?>
		</div>
	</ul>
	<section class="form">
		<div class="container-fluid">
			<div class="row">
				<!-- Basic Form-->
				<div class="col-lg-12">
					<div class="card">
						<div class="container" style="padding: 25px;">
							<div class='edit-container'>
								<form class="POST_AJAX"
									action="<?php echo isset($action)?$action:""?>" method="post">
								
									<div class="form-group">
										<label class="form-label"> User Name </label> <input
											class="form-control" required="required" type="text"
											id="user_name" name="user_name"
											value="<?php echo (isset($user_name))?$user_name:""; ?>" />
									</div>
									<div class="form-group">
										<label class="form-label">User Depatment</label> <select
											class="select2 form-control" name="department">
											<option value="0">Select Depatment</option>
											<?php if (isset ( $departments )) foreach ( $departments as $val ) {?>
												<option id="<?php echo $val['department_id']?>_select" value="<?php echo $val['department_id']?>"
												<?php echo isset($department_id)&&$department_id==$val['department_id']?"selected":"" ?>><?php echo $val['department_name']?></option>
                                                
                                            <?php }?>
                                            <input name="department_id" type="text" hidden=hidden
														value="<?=isset($department_id)?$department_id:"" ?>">
														<input name="department_name" type="hidden"
														value="<?=isset($department_name)?$department_name:"" ?>">
										</select>
									</div>
									<div class="form-group">
										<label class="form-label"> User Password </label> <input
											class="form-control" required="required" type="password"
											id="user_password" name="user_password"
											value="<?php echo (isset($user_password))?$user_password:""; ?>" />
									</div>

                                    <div class="form-group">
										<label class="form-label"> User Mobile no </label> <input
											class="form-control" required="required" type="text"
											id="user_mobile_no" name="user_mobile_no"
											value="<?php echo (isset($user_mobile_no))?$user_mobile_no:""; ?>" />
									</div>

									<div class="form-group">
										<label class="form-label"> Email Id </label> <input
											class="form-control" required="required" type="text"
											id="user_email" name="user_email"
											value="<?php echo (isset($user_email))?$user_email:""; ?>" />
									</div>

									<button value="save_html_data" ajax_events=true
										class="btn btn-outline-primary">SAVE</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
function validatation(){
    if($('input[name=user_name]:input').val()==""){
        $('input[name=user_name]:input').focus();
        return false;
    }else if($('input[name=user_password]:input').val()==""){
        $('input[name=user_password]:input').focus();
        return false;
    }else if($('input[name=user_mobile_no]:input').val()==""){
        $('input[name=user_mobile_no]:input').focus();
        return false;
    }else if($('input[name=user_email]:input').val()==""){
        $('input[name=user_email]:input').focus();
        return false;
    }else if($('input[name=department_id]:input').val()==0){
        return false;
    }else
	return true;
}

	$('select[name=department]:input').on("change", function (e) {
        
        $('input[name=department_id]:input').val($(this).val());
        $('input[name=department_id]:input').text($(this).val());
        
        $('input[name=department_name]:input').val($("#"+$(this).val()+"_select").html());
        $('input[name=department_name]:input').text($("#"+$(this).val()+"_select").html());
  

});

</script>