
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
    #exportButton {
        border-radius: 0;
    }
</style>
<div class="table_div_id" style="width: 100%;">
<?php $example2 = (isset($example2))?$example2:"data-table-".rand(); 
	switch($_SESSION['active_tag']){
	case 'Events':
		$active = 'Events';
		break;
	case 'Meetings':
		$active = 'Meetings';
		break;
	case 'users':
		$active = 'users';
		break;
	case 'farmer':
		$active = 'farmer';
		break;
	case 'contact_us':
		$active = 'contact_us';
		break;
	case 'message':
		$active = 'message';
		break;
	case 'survey':
		$active = 'survey';
		break;
	}
?>

<link
		href="<?php echo base_url('/components/')?>/css/dataTables.bootstrap4.min.css"
		rel="stylesheet" type="text/css" />

<style>
.paginate_button.disabled {
	pointer-events: none;
	color: #636c72;
	cursor: not-allowed;
}

img {
	height: 80px;
	width: 80px;
}

.paginate_button.current {
	pointer-events: none;
	cursor: not-allowed;
	color: #fff;
	background-color: #ff3f3f;
}

.paginate_button:HOVER {
	color: #fff;
	background-color: #ff3f3f;
}

.paginate_button.previous {
	border-radius: 6px 0px 0px 6px;
}

.paginate_button.next {
	border-radius: 0px 6px 6px 0px;
}

.paginate_button {
	line-height: 1.25;
	border: 1px solid transparent;
	padding: 0.5rem 1rem;
	cursor: pointer;
	transition: all 0.3s;
	border-color: #ddd;
	margin-left: -1px;
}
</style>
	<div class="content-inner">
		<!-- Page Header-->
		<?php if (isset($sub_title) || isset($title) || isset($add_url)){?>
		<header class="page-header">
			<div class="container-fluid">
			<?php if (isset($add_url)){?>
				<button class="btn btn-outline-primary"
					onclick="window.location='<?php echo isset($add_url)?$add_url:"";?>'"
					style="float: right; margin-right: 25px; margin-top: -5px;"
					data-toggle="tooltip" data-placement="bottom"
					title="Add <?php echo isset($title)?$title:"Record";?>">
					<i class="fa fa-plus"></i>
				</button>
				<?php }?>
				<?php if (isset($sub_title) || isset($title)){?>
				<h2 class="no-margin-bottom"><?php echo isset($sub_title)?$sub_title:(isset($title)?$title:"Title");?></h2>
				<?php }?>
			</div>
		</header>
				<?php } ?>
		<?php if (isset($sub_title) || isset($title)){?>
		<ul class="breadcrumb">
			<div class="container-fluid">
				<li class="breadcrumb-item"><a
					href="<?php echo base_url('/dashboard')?>">Home</a></li>
				<li class="breadcrumb-item active"><?php echo isset($title)?$title:"Table";?></li>
			<?php if (isset($sub_title)){?>
			<li class="breadcrumb-item active"><?php echo isset($title)?$sub_title:"Table";?></li>
			<?php }?>
		</div>
		</ul>
		<?php }?>
		<section class="tables">
			<div class="container-fluid">
				<div class="card">
					<div class="card-body">
						<span style="padding: 15px;">
							<?php
							if(@$module == "add_district_admin"){
								?>
							Add district admin
							<?php
						}
						else

							if(@$module == "add_district"){
								?>
							Add district
							<?php
						}
						else

							if(@$module == "edit_district"){
								?>
							Edit district
							<?php
						}
						else
							if(@$module == "edit_district_admin"){
								?>
							Edit district admin
							<?php
						}
						?>
						</span>
							<?php
							if(@$module == "add_district"){
								$this->load->view('common/add_district');
							}
							if(@$module == "add_district_admin"){
								?>
								<style>
									.error_color{
										color: red;
									}
								</style>
									<section class="form">
		<div class="container-fluid">
			<div class="row">
				<!-- Basic Form-->
				<div class="col-lg-12">
					<div class="card">
						<div class="container" style="padding: 25px;">
							<div class='edit-container'>
								<form class="POST_AJAX"
									action="" method="post" id="from_add_district_admin" name="from_add_district_admin">
								
									<div class="form-group">
										<label class="form-label">First Name</label> <input
											class="form-control"  type="text"
											id="district_admin_first_name" name="district_admin_first_name" maxlength="60"
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="add_district_firstn_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Last Name</label> <input
											class="form-control"  type="text"
											id="district_admin_last_name" name="district_admin_last_name" maxlength="60" 
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="add_district_lastn_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Mobile</label> <input
											class="form-control"  type="text"
											id="district_admin_mobile" name="district_admin_mobile" onkeypress="return isNumberKey(event)" maxlength="10" 
											  />
											  <span class="small"><em>(Min 10 digits)</em></span>
											  <span class="add_district_mob_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Email</label> <input
											class="form-control"  type="text"
											id="district_admin_email" name="district_admin_email" maxlength="60" 
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="add_district_email_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Address</label> <textarea
											class="form-control"  type="text"
											id="district_admin_address" name="district_admin_address" maxlength="1000" style="resize:none;"
											  /></textarea>
											  <span class="small"><em>(Max 1000 characters)</em></span>
											  <span class="add_district_address_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Date of birth</label> <input
											class="form-control"  type="text"
											id="district_admin_dob" name="district_admin_dob" readonly style="width:29%"  
											  />
											  
											  <span class="add_district_dob_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Designation</label> <input
											class="form-control"  type="text"
											id="district_admin_designation" name="district_admin_designation" maxlength="60" 
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="add_district_designation_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Role</label> <input
											class="form-control"  type="text"
											id="district_admin_role" name="district_admin_role" maxlength="60" 
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="district_admin_role_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Assembly</label> <input
											class="form-control"  type="text"
											id="district_admin_assembly" name="district_admin_assembly" maxlength="60" 
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="district_admin_assembly_error error_color"></span>
									</div>
									<div class="form-group">
										<input type="hidden" value="" id="selected_admin_district" name="selected_admin_district">
										<label class="form-label">Select district</label> 
										<?php 
										$getAllDistrict=getAllDistrictData();
										?>
										
											
											<?php
											foreach($getAllDistrict as $getAllDistrictData){
												?>
												<input type="checkbox" value="<?php echo $getAllDistrictData['districtName']?>" onchange="get_selected_district_admin();" name="get_district_admin[]"><?php echo $getAllDistrictData['districtName']?>&nbsp;
												<?php
											}
											?>
												
                                                
                                            <?php ?>
                                            <span class="add_district_error error_color"></span>
										
									</div>

                                   
									 

 									
									

									
									<button value="save_html_data" type="submit" id="add_district_admin_btn" 
										class="btn btn-outline-primary">SAVE</button>
										<a  href="<?php echo base_url();?>department/district_head" class="btn btn-outline-primary">Cancel</a>
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
				<div style="text-align: center;width: 50%;border: 1px solid green;display: none;" id="add_district_admin_success"></div>				<?php
							}
							else
								if(@$module == "edit_district"){
								$this->load->view('common/edit_district');
							}
								if(@$module == "edit_district_admin"){
								?>
								<style>
									.error_color{
										color: red;
									}
								</style>
									<section class="form">

		<div class="container-fluid">
			<div class="row">
				<!-- Basic Form-->
				<div class="col-lg-12">
					<div class="card">
						<div class="container" style="padding: 25px;">
							<div class='edit-container'>
								<?php 
							$this->load->view('common/edit_district_head');
							?>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
				<div style="text-align: center;width: 50%;border: 1px solid green;display: none;" id="edit_district_admin_success"></div>				<?php
							}
							?>
							
						<?php if(isset($display_contents) ){ if($_SESSION['active_tag'] == 'users' || $_SESSION['active_tag'] == 'Events' || $_SESSION['active_tag'] == 'Meetings'|| $_SESSION['active_tag'] == 'contact_us'|| $_SESSION['active_tag'] == 'survey' ){?>
							<div class="row">
								<!--<div class="col-md-3">
									<div class="form-group">
									  <!--<label for="sel1">district:</label>-->
									  <!--<select class="form-control select2" name="district" id="district" required>
										<option value="">-Select District-</option>
										<option value="AHMEDNAGAR">AHMEDNAGAR</option>
										<option value="AKOLA">AKOLA</option>
										<option value="AKOLA_RURAL">AKOLA RURAL</option>
										<option value="AMRAVATI">AMRAVATI</option>
										<option value="AURANGABAD">AURANGABAD</option>
										<option value="AURANGABAD_RURAL">AURANGABAD RURAL</option>
										<option value="BEED">BEED</option>
										<option value="BHANDARA">BHANDARA</option>
										<option value="BHIWANDI">BHIWANDI</option>
										<option value="BULDHANA">BULDHANA</option>
										<option value="CHANDRAPUR">CHANDRAPUR</option>
										<option value="DHULE">DHULE</option>
										<option value="GADHCHIROLI">GADHCHIROLI</option>
										<option value="GONDIYA">GONDIYA</option>
										<option value="HINGOLI">HINGOLI</option>
										<option value="JALGAON">JALGAON</option>
										<option value="JALNA">JALNA</option>
										<option value="KALYAN">KALYAN</option>
										<option value="KOLHAPUR_CITY">KOLHAPUR CITY</option>
										<option value="KOLHAPUR_RURAL">KOLHAPUR RURAL</option>
										<option value="LATUR">LATUR</option>
										<option value="NAGPUR">NAGPUR</option>
										<option value="NAGPUR_RURAL">NAGPUR RURAL</option>
										<option value="NANDED">NANDED</option>
										<option value="NANDED_RURAL">NANDED RURAL</option>
										<option value="NANDURBAR">NANDURBAR</option>
										<option value="NASHIK_CITY">NASHIK CITY</option>
										<option value="NASHIK_RURAL">NASHIK RURAL</option>
										<option value="NAVI_MUMBAI">NAVI MUMBAI</option>
										<option value="OSMANABAD">OSMANABAD</option>
										<option value="PALGHAR">PALGHAR</option>
										<option value="PARBHANI">PARBHANI</option>
										<option value="PIMPRI_CHINCHWAD">PIMPRI CHINCHWAD</option>
										<option value="PUNE">PUNE</option>
										<option value="PUNE_RURAL">PUNE RURAL</option>
										<option value="RAIGAD">RAIGAD</option>
										<option value="RATNAGIRI">RATNAGIRI</option>
										<option value="SANGLI">SANGLI</option>
										<option value="SATARA">SATARA</option>
										<option value="SINDHUDURG">SINDHUDURG</option>
										<option value="SOLAPUR">SOLAPUR</option>
										<option value="SOLAPUR_RURAL">SOLAPUR RURAL</option>
										<option value="THANE_CITY">THANE CITY</option>
										<option value="THANE_RURAL">THANE RURAL</option>
										<option value="VASAI_CITY">VASAI CITY</option>
										<option value="WARDHA">WARDHA</option>
										<option value="WASHIM">WASHIM</option>
										<option value="YAVATMAL">YAVATMAL</option>
									  </select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">-->
									 <!-- <label for="sel1">Select list:</label>-->
									  <!--<select class="form-control select2" id="assembly" name="assembly" required>
										<option value="">-Select Assembly-</option>
									  </select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group float-left">			
										<button id="exportButton" class="btn btn-sm btn-danger clearfix"><span class="fa fa-file-excel-o"></span> Export to Excel</button>
									</div>
								</div>-->
								
								<form id="data-form" action="<?php echo $action;?>" method="post">
									<div class="container-fluid">
										<div class="row">
											<div class="col-sm-2">			
												<button onclick="tableToExcel('<?php echo $example2; ?>', 'Export table')" id="exportButton" class="btn btn-sm btn-danger clearfix"><span class="fa fa-file-excel-o"></span> Export to Excel</button>
												<?php if($active == 'Events') {?>
													<a target="_blank" class="btn btn-sm btn-danger clearfix" href="<?php echo site_url()?>events/createxls"><i class="fa fa-file-excel-o"></i> Export All</a>
												<?php }?>
											</div>
											<div class="col-sm-3">
												<input type="text" data-date-format="dd/mm/yyyy" data-provide="datepicker" class="form-control datepicker" id="fdate" placeholder="From date" name="fdate" value="<?php echo @$fdate == null ? "" : "$fdate"?>" required>
											</div>
											
											<div class="col-sm-3">
												<input type="text" data-date-format="dd/mm/yyyy" data-provide="datepicker" class="form-control datepicker" id="tdate" placeholder="To date" name="tdate" value="<?php echo @$tdate == null ? "" : "$tdate"?>" required>
											</div>
											
											<div class="col-sm-2">
												<button id="data-filter-form-btn" type="submit" class="btn btn-outline-danger">Search</button>
											</div>
											
											<?php if($_SESSION['active_tag'] == 'Meetings') {?>
												<div class="col-sm-2">			
													<label class="switch">
													  <input value= "<?php echo $changeStatus; ?>" data-url="<?php echo $_SESSION['active_tag']; ?>" id="changeStatus" type="checkbox" <?php echo ($changeStatus == 'true') ? 'checked' : ''; ?> >
													  <span class="slider round"></span>
													</label>
												</div>
											<?php }?>
											
										</div>
									</div>
								</form>
							</div>
						<?php }?>
						<?php
								if(@$module == "district_admin"){
								?>
								<input type="hidden" id="delete_district_admin_id" name="delete_district_admin_id">
								<a  href="addDistrictAdmin" class="btn btn-sm btn-danger clearfix" style="float: right;">Add</a>
								<?php
							}
							?>
							<?php if($_SESSION['active_tag'] ==  "news"){ ?>
								<a href="<?php echo site_url('news/add');?>" class="btn btn-sm btn-success clearfix" style="float: right;">Add</a>
							<?php }?>
							
							<?php
								if(@$module == "district"){
								?>
								<input type="hidden" id="delete_district_id" name="delete_district_id">
								<a  href="addDistrict" class="btn btn-sm btn-danger clearfix" style="float: right;">Add</a>
								<?php
							}
							$this->load->view('common/delete_district_form');
							?>


<div id="delete_district_admin_modal" class="modal fade" role="dialog" style="position: absolute;">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form role="form" name="frm_district_admin_delete" id="frm_district_admin_delete" method="post">
            <div class="modal-content">
                <div class="modal-header" style="display: none">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure want to delete this record?</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="del_district_admin_submit" id="del_district_admin_submit" class="btn btn-primary">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </form>
    </div>
</div>					
					  
						<div class="row">
							<div class="col-md-12">
								<div style="text-align: center;width: 50%;border: 1px solid green;display: none;" id="delete_district_admin_success"></div>
								<div class="table-responsive">
									<table id="<?php echo $example2; ?>"
												class="table table-striped table-bordered table-hover exportTable"
												cellspacing="0" width="100%">
										<thead>
											<tr>
												<?php foreach ($display_contents as $row) {?>
													<th><?php echo $row ?></th>
												<?php } ?>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($table_data as $row){ ?>
												<tr <?php if (isset($row['tr_class']))echo "class=".$row['tr_class']; ?>>
													
													<?php foreach ($display_contents as $key=>$drow) {
														echo '<td>';
														if($drow == 'Image') { 
															switch($_SESSION['active_tag']){
																case 'Events':
																	$imgs = getEventimages($row['pid']);
																	break;
																case 'Meetings':
																	$imgs = getMeetingimages($row['pid']);
																	break;
																case 'contact_us':
																	$imgs = getContactUsimages($row['pid']);
																	break;
																case 'farmer':
																case 'users':
																	$imgs = getUserimages($row['user_id']);
																	break;
																case 'survey':
																	$imgs = getSurveyimages($row['pid']);
																	break;
															}
																	
															$cnt = count($imgs);
															if($cnt>0){
																	$y = 1; 
																	for($x=0;$x<$cnt;$x++){
																		echo '<a href='.$imgs[$x]['image_url'].' target="_blank"> Img'.$y++.'</a>, ';
																	}
																}else{ 
																	echo 'No images';
																}
														}
														else if($drow == "Action" && @$module == "district_admin"){

									
									
															?>
															<a  href="edit_district_admin/<?php echo $row['user_id']?>" class="btn btn-sm btn-danger clearfix">Edit</a>
															<a  class="btn btn-sm btn-danger clearfix" data-dismiss="modal" data-toggle="modal" data-target="#delete_district_admin_modal" onclick="getDistrictAdminId('<?php echo $row['user_id']?>')" style="cursor:pointer">Delete</a>
															<?php
														}
														else if($drow == "Action" && @$module == "district"){

									
									
															?>
															<a  href="edit_district/<?php echo $row['id']?>" class="btn btn-sm btn-danger clearfix">Edit</a>
															<a  class="btn btn-sm btn-danger clearfix" data-dismiss="modal" data-toggle="modal" data-target="#delete_district_data_modal" onclick="getDistrictId('<?php echo $row['id']?>')" style="cursor:pointer">Delete</a>
															
															<?php
														}
														if(@$drow == "District" && @$module == "district_admin"){
															$getAllUserDistricts=getAllUserDistrictData($row['user_id']);
															$getdistrictname="";
															foreach($getAllUserDistricts as $getAllUserDistrictsData){
																$getdistrictname.=$getAllUserDistrictsData['districtName'].",";
															}
															echo $userdistrict=rtrim($getdistrictname,",");
														}
														else{
															echo $row[$key];
														}
														echo '</td>';
													}
													?>
												</tr>
											<?php } ?>
										
										</tbody>
										<tfoot>
											<tr>
												<?php  foreach ($display_contents as $row) {?>
												<th><?php  echo $row ?></th>
												<?php } ?>
											</tr>
										</tfoot>
									</table>
									<?php } ?>
								</div>
						<?php if (isset($export_to_excel)) echo $export_to_excel?>
							</div>
						</div>
					</div>
				</div>
		
		</section>
	
	</div>
	<!-- start to add jquery calender plugin-->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/calender/jquery-ui.css">


  <script src="<?php echo base_url();?>assets/calender/jquery-ui.js"></script>
  <script>
  
  $( function() {
    $( "#district_admin_dob" ).datepicker({
          defaultDate: "+1w",
          readOnly: true,
          changeMonth: 1,
          changeYear: true,
          numberOfMonths: 1,
           maxDate:0,
          minDate:null,
          yearRange: '1951:2018',
        }).on( "change", function() {
          var edit_birth_date=$("#district_admin_dob").val();
          var fromsplit=edit_birth_date.split("/");
          $("#district_admin_dob").val(fromsplit[1]+'-'+fromsplit[0]+'-'+fromsplit[2]);
          
            
        });
          $( "#edit_district_admin_dob" ).datepicker({
          defaultDate: "+1w",
          readOnly: true,
          changeMonth: 1,
          changeYear: true,
          numberOfMonths: 1,
           maxDate:0,
          minDate:null,
          yearRange: '1951:2018',
        }).on( "change", function() {
          var edit_birth_date=$("#edit_district_admin_dob").val();
          var fromsplit=edit_birth_date.split("/");
          $("#edit_district_admin_dob").val(fromsplit[1]+'-'+fromsplit[0]+'-'+fromsplit[2]);
          
            
        });

  } );
  </script>
	<!-- end to add jquery calender plugin-->
	<script src="<?php echo base_url();?>assets/js/admin.js"></script>
	<script src="<?php echo base_url();?>assets/js/add_district.js"></script>
	<script
		src="<?php echo base_url("components") ?>/js/datatables/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script type="text/javascript">
/*!
SearchHighlight for DataTables v1.0.1
2014 SpryMedia Ltd - datatables.net/license
*/
(function(h,g,b){function e(d,c){d.unhighlight();c.rows({filter:"applied"}).data().length&&d.highlight(b.trim(c.search()).split(/\s+/))}b(g).on("init.dt.dth",function(d,c){if("dt"===d.namespace){var a=new b.fn.dataTable.Api(c),f=b(a.table().body());if(b(a.table().node()).hasClass("searchHighlight")||c.oInit.searchHighlight||b.fn.dataTable.defaults.searchHighlight)a.on("draw.dt.dth column-visibility.dt.dth column-reorder.dt.dth",function(){e(f,a)}).on("destroy",function(){a.off("draw.dt.dth column-visibility.dt.dth column-reorder.dt.dth")}),
a.search()&&e(f,a)}})})(window,document,jQuery);

/*
 * jQuery Highlight plugin
 *
 * Based on highlight v3 by Johann Burkard
 * http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html
 *
 * Code a little bit refactored and cleaned (in my humble opinion).
 * Most important changes:
 *  - has an option to highlight only entire words (wordsOnly - false by default),
 *  - has an option to be case sensitive (caseSensitive - false by default)
 *  - highlight element tag and class names can be specified in options
 *
 * Usage:
 *   // wrap every occurrance of text 'lorem' in content
 *   // with <span class='highlight'> (default options)
 *   $('#content').highlight('lorem');
 *
 *   // search for and highlight more terms at once
 *   // so you can save some time on traversing DOM
 *   $('#content').highlight(['lorem', 'ipsum']);
 *   $('#content').highlight('lorem ipsum');
 *
 *   // search only for entire word 'lorem'
 *   $('#content').highlight('lorem', { wordsOnly: true });
 *
 *   // don't ignore case during search of term 'lorem'
 *   $('#content').highlight('lorem', { caseSensitive: true });
 *
 *   // wrap every occurrance of term 'ipsum' in content
 *   // with <em class='important'>
 *   $('#content').highlight('ipsum', { element: 'em', className: 'important' });
 *
 *   // remove default highlight
 *   $('#content').unhighlight();
 *
 *   // remove custom highlight
 *   $('#content').unhighlight({ element: 'em', className: 'important' });
 *
 *
 * Copyright (c) 2009 Bartek Szopka
 *
 * Licensed under MIT license.
 *
 */

jQuery.extend({
    highlight: function (node, re, nodeName, className) {
        if (node.nodeType === 3) {
            var match = node.data.match(re);
            if (match) {
                var highlight = document.createElement(nodeName || 'span');
                highlight.className = className || 'highlight';
                var wordNode = node.splitText(match.index);
                wordNode.splitText(match[0].length);
                var wordClone = wordNode.cloneNode(true);
                highlight.appendChild(wordClone);
                wordNode.parentNode.replaceChild(highlight, wordNode);
                return 1; //skip added node in parent
            }
        } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
                !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
                !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted
            for (var i = 0; i < node.childNodes.length; i++) {
                i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
            }
        }
        return 0;
    }
});

jQuery.fn.unhighlight = function (options) {
    var settings = { className: 'highlight', element: 'span' };
    jQuery.extend(settings, options);

    return this.find(settings.element + "." + settings.className).each(function () {
        var parent = this.parentNode;
        parent.replaceChild(this.firstChild, this);
        parent.normalize();
    }).end();
};

jQuery.fn.highlight = function (words, options) {
    var settings = { className: 'highlight', element: 'span', caseSensitive: false, wordsOnly: false };
    jQuery.extend(settings, options);
    
    if (words.constructor === String) {
        words = [words];
    }
    words = jQuery.grep(words, function(word, i){
      return word != '';
    });
    words = jQuery.map(words, function(word, i) {
      return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    });
    if (words.length == 0) { return this; };

    var flag = settings.caseSensitive ? "" : "i";
    var pattern = "(" + words.join("|") + ")";
    if (settings.wordsOnly) {
        pattern = "\\b" + pattern + "\\b";
    }
    var re = new RegExp(pattern, flag);
    
    return this.each(function () {
        jQuery.highlight(this, re, settings.element, settings.className);
    });
};


window.onload=function(){
	$('.datepicker').datepicker({
		format: 'mm/dd/yyyy',
		startDate: '-3d'
	});
var table = $('#<?php echo $example2; ?>').DataTable({
		"paging" : true,
		"searching" : true,
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"ordering" : true,
                "order": [[ 0, "desc" ]],
		"fixedHeader": true,
		 "searchHighlight": true,
		"info" : true,
		"autoWidth" : false
	});
	  
	    table.on( 'draw', function () {
	        var body = $( table.table().body() );	 
	        body.unhighlight();
	        body.highlight( table.search() );  
	    } );
	    
	    if(!$('select').hasClass('form-control')){
	    	$('select').addClass('form-control');
		}
	
	//Select Assembly
	$( "#district" ).change(function() {
      var id = $('#district').val();
         $.ajax({
                type:'POST',
                url:'<?php echo base_url("events/getAssembly"); ?>',
                data:{'id':id},
                success:function(res){
					data = JSON.parse(res);
					var result = '';
					$.each(data, function(i, item) {
						result += '<option value="'+data[i]+'">'+data[i]+'</option>';
					})
					$('#assembly').html(result);
                }
            });
    });
	
	$( "#changeStatus" ).change(function() {
		var val = $('#changeStatus').val();
		// var url = $(this).attr('data-url');
		var url = 'meetings';
		var status = '';
		if (val == "true") {
			status = 'false';
		}else {
		  status = 'true';
		}
		var data = {
					'status' : status,
					'col' : 'status',
					'name' : url
					};
		
		$.ajax({
				type: "POST",
				url: '<?php echo base_url(); ?>'+url+'/change_status',
				data: data,
				success:function(data){
					if(data=="success"){
						show_message('success','Status changed successfully');
						$('#changeStatus').val(status);//change status
					}else{
						show_message('danger','Error Not updated');						
					}
					console.log(data);
				}
		});
	   
    });
	
function changeStatus(id){
	
	if(confirm('Are you sure to change the status..!') == true){
		var val = $('#changeStatus').val();
		var data = {
					'id': id,
					'status' : val,
					'col' : 'status'
					};
		console.log(data);
			// $.ajax({
					// type: "POST",
					// url: '<?php echo base_url(); ?>index.php/admin/adv/change_status',
					// data: data,
					// success:function(data)
					// {
						//// if(data=="success")
						//// {
							//// window.location.href='<?php echo base_url(); ?>index.php/admin/manage_awards';
						//// }else{
							//// window.location.href='<?php echo base_url(); ?>index.php/admin/manage_awards';
						//// }
						// location.reload();
					// }
			// });
	}
}
	
}
</script>
<link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light/all.min.css" />
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
<script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/jszip.min.js"></script>

<script type="text/javascript">
	var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()
	
</script>
</div>