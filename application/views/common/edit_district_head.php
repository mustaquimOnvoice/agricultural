	<form class="POST_AJAX"
									action="" method="post" id="frm_edit_district_admin" name="frm_edit_district_admin">
								<input type="hidden" id="update_district_admin_id" name="update_district_admin_id" value="<?php echo $table_data->user_id?>">
								<div class="form-group">
										<label class="form-label">First Name</label> <input
											class="form-control"  type="text"
											id="edit_district_admin_first_name" name="edit_district_admin_first_name" value="<?php echo $table_data->fname?>"
											  />
											   <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="edit_district_firstn_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Last Name</label> <input
											class="form-control"  type="text"
											id="edit_district_admin_last_name" name="edit_district_admin_last_name" value="<?php echo $table_data->lname?>"
											  />
											   <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="edit_district_lastn_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Mobile</label> <input class="form-control" type="text" id="edit_district_admin_mobile" name="edit_district_admin_mobile" value="<?php echo $table_data->user_mobile_no?>" onkeypress="return isNumberKey(event)" maxlength="10">
											  <span class="small"><em>(Min 10 digits)</em></span>
											  <span class="edit_district_mob_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Email</label> <input class="form-control" type="text" id="edit_district_admin_email" name="edit_district_admin_email" value="<?php echo $table_data->email?>" maxlength="60">
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="edit_district_email_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Address</label> <textarea class="form-control" type="text" id="edit_district_admin_address" name="edit_district_admin_address"  maxlength="1000" style="resize:none;"><?php echo $table_data->user_address?></textarea>
											  <span class="small"><em>(Max 1000 characters)</em></span>
											  <span class="edit_district_address_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Date of birth</label> 
										<input
											class="form-control"  type="text"
											id="edit_district_admin_dob" name="edit_district_admin_dob" readonly="" value="<?php echo date("d-m-Y",strtotime($table_data->user_dob));?>" style="width:29%"  
											  />	  
											  <span class="edit_district_dob_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Designation</label> <input
											class="form-control"  type="text"
											id="edit_district_admin_designation" name="edit_district_admin_designation" maxlength="60" value="<?php echo $table_data->designation?>" 
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="edit_district_designation_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Role</label> <input
											class="form-control"  type="text"
											id="edit_district_admin_role" name="edit_district_admin_role" maxlength="60" value="<?php echo $table_data->user_role?>"
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="edit_district_admin_role_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Assembly</label> <input
											class="form-control"  type="text"
											id="edit_district_admin_assembly" name="edit_district_admin_assembly" maxlength="60" value="<?php echo $table_data->assembly?>" 
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="edit_district_admin_assembly_error error_color"></span>
									</div>
									<div class="form-group">
										<input type="hidden" value="" id="edit_selected_admin_district" name="edit_selected_admin_district">
										<label class="form-label">Select district</label> 
										<?php 
										$getAllDistrict=getAllDistrictData();
										?>
										
											
											<?php
											$exist_district_name="";
											foreach($getAllDistrict as $getAllDistrictData){

												$getExistDistricts=getAllUserDistrictData($table_data->user_id);
												$checked_value="";
												$count=0;
												
												if(count($getExistDistricts) > 0){
												foreach($getExistDistricts as $getExistDistrictsdata){
													
												 	if($getExistDistrictsdata['districtName'] == $getAllDistrictData['districtName']){
														$exist_district_name .=$getExistDistrictsdata['districtName'].",";
												 		$checked_value="checked";

												 	} 
												 	$count++;
												}
												//$exist_district_name=rtrim($exist_district_name,",");
											}
												?>

												<input type="checkbox" value="<?php echo $getAllDistrictData['districtName']?>" onchange="get_selected_edit_district_admin();" name="get_edit_district_admin[]"  <?php echo $checked_value;?> <?php echo $checked_value;?>><?php echo $getAllDistrictData['districtName']?>&nbsp;
												<?php
											}
											?>
												
                                                
                                            <?php ?>
                                            <input type="hidden" value="<?php echo $exist_district_name;?>" id="edit_exist_district" name="edit_exist_district">
                                            <span class="edit_district_error error_color"></span>
										
									</div>

                                    
									 

 									
									

									
									<button value="save_html_data" type="submit" id="update_district_admin_btn" 
										class="btn btn-outline-primary">Update</button>
										<a  href="<?php echo base_url();?>department/district_head" class="btn btn-outline-primary">Cancel</a>
								</form>