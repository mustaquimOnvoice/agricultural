<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom"><?php echo isset($sub_title)?$sub_title:(isset($title)?$title:"Title");?></h2>
		</div>
	</header>
	<ul class="breadcrumb">
		<div class="container-fluid">
			<li class="breadcrumb-item"><a
				href="<?php echo base_url('/dashboard')?>">Home</a></li>
			<li class="breadcrumb-item active"><?php echo isset($title)?$title:"Title";?></li>
			<?php if (isset($sub_title)){?>
			<li class="breadcrumb-item active"><?php echo isset($title)?$sub_title:"Title";?></li>
			<?php }?>
		</div>
	</ul>
	<section class="form">
		<div class="container-fluid">
			<div class="row">
				<!-- Basic Form-->
				<div class="col-lg-12">
					<div class="card">
						<div class="container" style="padding: 25px;">
							<div class='edit-container'>
								<form class="POST_AJAX"
									action="<?php echo isset($action)?$action:""?>" method="post">
									<button
										done_message="<?php echo (isset($done_message))?$done_message:"Success..."?>"
										class="btn btn-outline-primary">SAVE</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
function validatation(){
		return true;
}
</script>