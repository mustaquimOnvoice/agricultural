-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 29, 2019 at 09:17 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `biodhan`
--

-- --------------------------------------------------------

--
-- Table structure for table `crop`
--

CREATE TABLE `crop` (
  `id` int(11) NOT NULL,
  `cropcat_id` int(11) NOT NULL,
  `vender_id` int(11) NOT NULL DEFAULT '0' COMMENT '0 Self, others vendors',
  `farmer_id` int(11) NOT NULL,
  `harvest_date` date NOT NULL,
  `pickup` datetime NOT NULL,
  `estimated_weight` varchar(20) NOT NULL,
  `lati` varchar(30) NOT NULL,
  `longi` varchar(30) NOT NULL,
  `address` varchar(250) DEFAULT NULL,
  `contact_no` bigint(20) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 Deactive/ 1 Active',
  `user_mobile_no` bigint(20) NOT NULL,
  `device_token` text NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crop`
--

INSERT INTO `crop` (`id`, `cropcat_id`, `vender_id`, `farmer_id`, `harvest_date`, `pickup`, `estimated_weight`, `lati`, `longi`, `address`, `contact_no`, `status`, `user_mobile_no`, `device_token`, `default_date`) VALUES
(15, 1, 1, 6, '2019-03-30', '2019-04-01 09:56:00', '1000', '18.5632974', '73.9174946', 'UG-24, East Court, Opposite Phoenix Market City, Viman Nagar, Pune, Maharashtra 411014, India\n', 1234567890, '1', 9284638880, 'eoE9u03YGOU:APA91bHAqopjle5fzRFGzsJlKxm3-JjdzfA2BGyuMFWmwApaMclhkE5299XIKU70LBIFiRiFvcJZsrRjbrUkkQ-iDUngjOVQ29MYkRcfRXAcqftUEy0hrnabhly0SSHy3ryw4GH63_VM', '2019-03-30 04:26:53'),
(17, 4, 1, 6, '2019-03-30', '2019-04-26 10:00:00', '1000', '18.563299', '73.9175034', 'UG-24, East Court, Opposite Phoenix Market City, Viman Nagar, Pune, Maharashtra 411014, India\n', 1234567890, '1', 9284638880, 'eoE9u03YGOU:APA91bHAqopjle5fzRFGzsJlKxm3-JjdzfA2BGyuMFWmwApaMclhkE5299XIKU70LBIFiRiFvcJZsrRjbrUkkQ-iDUngjOVQ29MYkRcfRXAcqftUEy0hrnabhly0SSHy3ryw4GH63_VM', '2019-03-30 04:30:53'),
(19, 1, 1, 5, '2019-03-20', '2019-03-23 22:59:00', '50kg', '10.2.3030.323', '12.30326.326', 'pune', 8888889999, '1', 9175917762, '123456789', '2019-03-30 04:33:21'),
(20, 6, 1, 6, '2019-03-30', '2019-04-01 10:08:00', '1000', '18.5633036', '73.91751949999998', 'UG-24, East Court, Opposite Phoenix Market City, Viman Nagar, Pune, Maharashtra 411014, India\n', 1234567890, '1', 9284638880, 'eoE9u03YGOU:APA91bHAqopjle5fzRFGzsJlKxm3-JjdzfA2BGyuMFWmwApaMclhkE5299XIKU70LBIFiRiFvcJZsrRjbrUkkQ-iDUngjOVQ29MYkRcfRXAcqftUEy0hrnabhly0SSHy3ryw4GH63_VM', '2019-03-30 04:40:20'),
(21, 1, 1, 6, '2019-03-30', '2019-04-01 10:15:00', '1000', '18.5633035', '73.9175085', 'UG-24, East Court, Opposite Phoenix Market City, Viman Nagar, Pune, Maharashtra 411014, India\n', 1234567890, '1', 9284638880, 'eoE9u03YGOU:APA91bHAqopjle5fzRFGzsJlKxm3-JjdzfA2BGyuMFWmwApaMclhkE5299XIKU70LBIFiRiFvcJZsrRjbrUkkQ-iDUngjOVQ29MYkRcfRXAcqftUEy0hrnabhly0SSHy3ryw4GH63_VM', '2019-03-30 04:45:38'),
(22, 1, 1, 10, '2019-03-30', '2019-04-01 10:50:00', '1000', '18.5633073', '73.9175656', '604, Phoenix Boundary Road, East Court, Viman Nagar, Pune, Maharashtra 411014, India', 1234567890, '1', 1234567890, 'eoE9u03YGOU:APA91bHAqopjle5fzRFGzsJlKxm3-JjdzfA2BGyuMFWmwApaMclhkE5299XIKU70LBIFiRiFvcJZsrRjbrUkkQ-iDUngjOVQ29MYkRcfRXAcqftUEy0hrnabhly0SSHy3ryw4GH63_VM', '2019-03-30 05:52:02');

-- --------------------------------------------------------

--
-- Table structure for table `crop_cat`
--

CREATE TABLE `crop_cat` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0=Inactive,1=Active',
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crop_cat`
--

INSERT INTO `crop_cat` (`id`, `name`, `status`, `default_date`) VALUES
(1, 'Sugaracane', '1', '2019-03-22 05:18:15'),
(2, 'Cotton', '1', '2019-03-22 05:18:15'),
(3, 'Bajrai', '1', '2019-03-22 05:20:39'),
(4, 'Wheat', '1', '2019-03-22 05:20:39'),
(5, 'Onion', '1', '2019-03-22 05:21:06'),
(6, 'Mung', '1', '2019-03-22 05:21:06'),
(7, 'Pepper', '1', '2019-03-22 05:21:19'),
(8, 'Other', '1', '2019-03-22 05:21:19');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `desciption` text CHARACTER SET utf8 NOT NULL,
  `start_date` text CHARACTER SET utf8 NOT NULL,
  `end_date` text CHARACTER SET utf8 NOT NULL,
  `rating` double NOT NULL,
  `location` text NOT NULL,
  `device_token` text NOT NULL,
  `user_mobile_no` varchar(20) NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `farm`
--

CREATE TABLE `farm` (
  `id` int(11) NOT NULL,
  `farmer_id` int(11) NOT NULL,
  `farmer_name` varchar(250) NOT NULL,
  `rsp_name` varchar(250) NOT NULL,
  `contact_no` int(11) NOT NULL,
  `alternate_no` int(11) NOT NULL,
  `survey_no` varchar(250) NOT NULL,
  `gat_no` varchar(250) NOT NULL,
  `pin` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 Deactive/ 1 Active',
  `user_mobile_no` bigint(20) NOT NULL,
  `device_token` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `farm`
--

INSERT INTO `farm` (`id`, `farmer_id`, `farmer_name`, `rsp_name`, `contact_no`, `alternate_no`, `survey_no`, `gat_no`, `pin`, `status`, `user_mobile_no`, `device_token`, `date_added`) VALUES
(1, 1, 'Fameer1', 'Repersentative name', 2147483647, 2147483647, 'Jo-(0lod9', '1547/9-op', 414001, '1', 9175917762, '123456789', '2019-03-29 08:22:38'),
(2, 1, 'Fameer1', 'Repersentative name', 2147483647, 2147483647, 'Jo-(0lod9', '1547/9-op', 414001, '1', 9175917762, '123456789', '2019-03-29 09:52:31');

-- --------------------------------------------------------

--
-- Table structure for table `farmer`
--

CREATE TABLE `farmer` (
  `user_id` int(11) NOT NULL,
  `user_mobile_no` text CHARACTER SET utf8 NOT NULL,
  `facebook` text CHARACTER SET utf8 NOT NULL,
  `twitter` text CHARACTER SET utf8 NOT NULL,
  `user_name` text CHARACTER SET utf8 NOT NULL,
  `fname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `age` text CHARACTER SET utf8 NOT NULL,
  `user_dob` text CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `adhar_no` bigint(20) NOT NULL,
  `pan_no` varchar(25) NOT NULL,
  `user_address` text CHARACTER SET utf8 NOT NULL,
  `user_city` varchar(250) NOT NULL,
  `user_district` text CHARACTER SET utf8 NOT NULL,
  `user_state` varchar(250) NOT NULL,
  `pin` int(11) NOT NULL,
  `user_role` text CHARACTER SET utf8 NOT NULL,
  `assembly` text CHARACTER SET utf8 NOT NULL,
  `user_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `designation` text CHARACTER SET utf8 NOT NULL,
  `user_status` int(11) NOT NULL COMMENT '0 pending, 1 otp send , 2 active , 3 remove ',
  `user_is` varchar(10) CHARACTER SET utf8 NOT NULL,
  `user_otp` varchar(4) CHARACTER SET utf8 NOT NULL,
  `device_token` text CHARACTER SET utf8 NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `farmer`
--

INSERT INTO `farmer` (`user_id`, `user_mobile_no`, `facebook`, `twitter`, `user_name`, `fname`, `lname`, `age`, `user_dob`, `email`, `adhar_no`, `pan_no`, `user_address`, `user_city`, `user_district`, `user_state`, `pin`, `user_role`, `assembly`, `user_type`, `designation`, `user_status`, `user_is`, `user_otp`, `device_token`, `default_date`) VALUES
(5, '9175917762', '', '', '', 'Mustaquimm', 'Sayyed', '', '', 'mustaquim.sayyed@gmail.com', 123456789987654, '123321123', 'Nahoha, Industries, Next to Bus stand', 'Ahmednagar', 'Ahmednagar', 'Maharashtra', 414001, '', '', '', '', 2, 'existing', '', '123456789', '2019-03-21 12:45:35'),
(6, '9284638880', '', '', '', 'Hdhshhss', 'Sbbsbs', '', '', 't@t.dhshs', 646767, '9898979776', 'Shdhhx', 'Zbbsb', 'Shshhs', 'Zhshhs', 767997, '', '', '', '', 2, 'existing', '', 'eoE9u03YGOU:APA91bHAqopjle5fzRFGzsJlKxm3-JjdzfA2BGyuMFWmwApaMclhkE5299XIKU70LBIFiRiFvcJZsrRjbrUkkQ-iDUngjOVQ29MYkRcfRXAcqftUEy0hrnabhly0SSHy3ryw4GH63_VM', '2019-03-23 06:41:17'),
(7, '9822401232', '', '', '', 'Dnyaneshwarjjjj', 'Dalvi', '', '', 'dnyaneshwar@onevoicetransmedia.com', 123654789098999, '8995959', 'Dnndndndjdj', 'Pune', 'Punepune', 'Maharashtra', 646646, '', '', '', '', 2, 'existing', '', 'dLmwiFWt_zQ:APA91bE0C3D-757R7316_3DdaBsjLHCu3dd6b_TOgpB3bLmtdX48VL9UmIxAIhcTm0wySa8jAg-PInmAkflIkdQBlXTwnG1yka9RasVxIuDtOEYrmlCL4ldP2uk93HWjRhD-b3V0Wvhz', '2019-03-28 12:21:28'),
(8, '9821331356', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '', '', '', 2, 'new', '', 'eoE9u03YGOU:APA91bHAqopjle5fzRFGzsJlKxm3-JjdzfA2BGyuMFWmwApaMclhkE5299XIKU70LBIFiRiFvcJZsrRjbrUkkQ-iDUngjOVQ29MYkRcfRXAcqftUEy0hrnabhly0SSHy3ryw4GH63_VM', '2019-03-30 05:13:38'),
(9, '9321195958', '', '', '', 'Monish', 'Ahuja', '', '', 'ghfhhbgjjj@yahoo.com', 625854655, '33666996', 'Brubhb', 'Btghbg', 'Bhhfgh', 'Bhyfhjj', 666666, '', '', '', '', 2, 'existing', '', 'eDiUIr75bYw:APA91bFL8QUslP-kTK7QLkZxMFPUBhICc1fglUF67StvmGViJwki12e8BZ-u_z5T_l98CDvn8sUVJ4yKC3xnfRK3vtqjE-t5vQf2kHRx50We59z4omDxe33yyxZu-lj6B1gKTOzWOG4U', '2019-03-30 05:13:45'),
(10, '1234567890', '', '', '', 'Abc ', 'Abcd', '', '', 'abc@abc.com', 1234567890, '123456', 'Abc', 'Abc', 'ABC', 'Abcsfgf', 123456, '', '', '', '', 2, 'existing', '', 'eoE9u03YGOU:APA91bHAqopjle5fzRFGzsJlKxm3-JjdzfA2BGyuMFWmwApaMclhkE5299XIKU70LBIFiRiFvcJZsrRjbrUkkQ-iDUngjOVQ29MYkRcfRXAcqftUEy0hrnabhly0SSHy3ryw4GH63_VM', '2019-03-30 05:14:16'),
(11, '8986860686', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '', '', '', 1, 'new', '1234', '', '2019-03-30 09:45:44'),
(12, '9886858558', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '', '', '', 2, 'new', '', 'eoE9u03YGOU:APA91bHAqopjle5fzRFGzsJlKxm3-JjdzfA2BGyuMFWmwApaMclhkE5299XIKU70LBIFiRiFvcJZsrRjbrUkkQ-iDUngjOVQ29MYkRcfRXAcqftUEy0hrnabhly0SSHy3ryw4GH63_VM', '2019-03-30 09:46:09'),
(13, '7977785116', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', 0, '', '', '', '', 2, 'new', '', 'eDiUIr75bYw:APA91bFL8QUslP-kTK7QLkZxMFPUBhICc1fglUF67StvmGViJwki12e8BZ-u_z5T_l98CDvn8sUVJ4yKC3xnfRK3vtqjE-t5vQf2kHRx50We59z4omDxe33yyxZu-lj6B1gKTOzWOG4U', '2019-03-30 10:37:14');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) NOT NULL,
  `image_url` text NOT NULL,
  `type` int(1) NOT NULL COMMENT '0 farmer/ 1 vendor/ 2 user/ 3 crop cat/ 4 crop',
  `ref_id` bigint(20) NOT NULL COMMENT 'FarmerID/VendorID/UserID/CropcatID'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image_url`, `type`, `ref_id`) VALUES
(1, 'http://biodhan.ilovengr.com/uploads/vendor/2/images_unitglo_mobile-morixoHICt.jpg', 1, 2),
(2, 'http://biodhan.ilovengr.com/uploads/vendor/2/images_unitglo_mobile-I6PRBjfQwN.jpg', 1, 2),
(3, 'http://biodhan.ilovengr.com/uploads/6/images_unitglo_mobile-lSpBtlz4i1.jpg', 0, 6),
(4, 'http://biodhan.ilovengr.com/uploads/cropcat/sugarcane.jpg', 3, 1),
(5, 'http://biodhan.ilovengr.com/uploads/cropcat/cotton.jpg', 3, 2),
(6, 'http://biodhan.ilovengr.com/uploads/cropcat/bajrai.jpg', 3, 3),
(7, 'http://biodhan.ilovengr.com/uploads/cropcat/wheat.jpg', 3, 4),
(8, 'http://biodhan.ilovengr.com/uploads/cropcat/onion.jpg', 3, 5),
(9, 'http://biodhan.ilovengr.com/uploads/cropcat/mung.jpg', 3, 6),
(10, 'http://biodhan.ilovengr.com/uploads/cropcat/pepper.jpg', 3, 7),
(11, 'http://biodhan.ilovengr.com/uploads/cropcat/other.jpg', 3, 8),
(12, 'http://biodhan.ilovengr.com/uploads/crop/1/images_unitglo_mobile-OvMtXG522T.jpg', 4, 1),
(13, 'http://biodhan.ilovengr.com/uploads/crop/1/images_unitglo_mobile-ZSfMHCWjIt.jpg', 4, 1),
(14, 'http://biodhan.ilovengr.com/uploads/farm/1/images_unitglo_mobile-oTlgilDZuP.jpg', 5, 1),
(15, 'http://biodhan.ilovengr.com/uploads/farm/1/images_unitglo_mobile-fhAyOq3wEF.jpg', 5, 1),
(16, 'http://biodhan.ilovengr.com/uploads/farm/2/images_unitglo_mobile-bSWhHngws4.jpg', 5, 2),
(17, 'http://biodhan.ilovengr.com/uploads/farm/2/images_unitglo_mobile-y7y1UUXiHs.jpg', 5, 2),
(18, 'http://biodhan.ilovengr.com/uploads/crop/5/images_unitglo_mobile-z4Muhl1oqK.png', 4, 5),
(19, 'http://biodhan.ilovengr.com/uploads/crop/5/images_unitglo_mobile-6cBaCufOuQ.jpg', 4, 5),
(20, 'http://biodhan.ilovengr.com/uploads/crop/6/images_unitglo_mobile-lKhCXbiXOQ.png', 4, 6),
(21, 'http://biodhan.ilovengr.com/uploads/crop/6/images_unitglo_mobile-geJwT6Gs4n.jpg', 4, 6),
(22, 'http://biodhan.ilovengr.com/uploads/crop/7/images_unitglo_mobile-qsrxjcORxb.png', 4, 7),
(23, 'http://biodhan.ilovengr.com/uploads/crop/7/images_unitglo_mobile-o713QOJaQV.png', 4, 7),
(24, 'http://biodhan.ilovengr.com/uploads/crop/12/images_unitglo_mobile-JNk2AB39M2.jpeg', 4, 12),
(25, 'http://biodhan.ilovengr.com/uploads/crop/12/images_unitglo_mobile-9fSLFffjaC.jpeg', 4, 12),
(26, 'http://biodhan.ilovengr.com/uploads/crop/13/images_unitglo_mobile-jyLD1a4M4E.jpeg', 4, 13),
(27, 'http://biodhan.ilovengr.com/uploads/crop/13/images_unitglo_mobile-QJ93qMZC1t.jpeg', 4, 13),
(28, 'http://biodhan.ilovengr.com/uploads/crop/14/images_unitglo_mobile-YXP8Fmab5t.jpeg', 4, 14),
(29, 'http://biodhan.ilovengr.com/uploads/crop/14/images_unitglo_mobile-KwAVvJzLaf.xiaomi', 4, 14),
(30, 'http://biodhan.ilovengr.com/uploads/crop/15/images_unitglo_mobile-WgQiYAAF6Z.jpeg', 4, 15),
(31, 'http://biodhan.ilovengr.com/uploads/crop/15/images_unitglo_mobile-kxzTwo3PzD.jpg', 4, 15),
(32, 'http://biodhan.ilovengr.com/uploads/crop/16/images_unitglo_mobile-q6ZGBWoNBd.jpg', 4, 16),
(33, 'http://biodhan.ilovengr.com/uploads/crop/16/images_unitglo_mobile-fXl9A8KSXl.jpg', 4, 16),
(34, 'http://biodhan.ilovengr.com/uploads/crop/17/images_unitglo_mobile-S2A4NiBHHK.jpeg', 4, 17),
(35, 'http://biodhan.ilovengr.com/uploads/crop/17/images_unitglo_mobile-h4LPJjQxlO.jpeg', 4, 17),
(36, 'http://biodhan.ilovengr.com/uploads/crop/20/images_unitglo_mobile-ZiYedxqgTY.jpeg', 4, 20),
(37, 'http://biodhan.ilovengr.com/uploads/crop/20/images_unitglo_mobile-aV74HLjaKH.jpeg', 4, 20),
(38, 'http://biodhan.ilovengr.com/uploads/crop/21/images_unitglo_mobile-vYi5X7NVQt.jpeg', 4, 21),
(39, 'http://biodhan.ilovengr.com/uploads/crop/21/images_unitglo_mobile-JcR1CYnr8D.jpeg', 4, 21),
(40, 'http://biodhan.ilovengr.com/uploads/7/images_unitglo_mobile-fprCIk5rr6.jpg', 0, 7),
(41, 'http://biodhan.ilovengr.com/uploads/9/images_unitglo_mobile-cOsPdkScjr.jpg', 0, 9),
(42, 'http://biodhan.ilovengr.com/uploads/10/images_unitglo_mobile-vJyIvLorRP.jpeg', 0, 10),
(43, 'http://biodhan.ilovengr.com/uploads/crop/22/images_unitglo_mobile-PWy8WlXDQn.jpeg', 4, 22),
(44, 'http://biodhan.ilovengr.com/uploads/crop/22/images_unitglo_mobile-iXcTPVW6OJ.jpeg', 4, 22),
(45, 'http://biodhan.ilovengr.com/uploads/7/images_unitglo_mobile-eOaBi1BHES.jpg', 0, 7),
(46, 'http://biodhan.ilovengr.com/uploads/7/images_unitglo_mobile-5E3JYVOfhT.jpg', 0, 7);

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

CREATE TABLE `meetings` (
  `id` int(11) NOT NULL,
  `device_token` text NOT NULL,
  `title` text CHARACTER SET utf8 NOT NULL,
  `date` text CHARACTER SET utf8 NOT NULL,
  `location` text CHARACTER SET utf8 NOT NULL,
  `mom` text CHARACTER SET utf8 NOT NULL,
  `next_meeting` text CHARACTER SET utf8 NOT NULL,
  `attendee` text CHARACTER SET utf8 NOT NULL,
  `user_mobile_no` text CHARACTER SET utf8 NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpyc_contact`
--

CREATE TABLE `mpyc_contact` (
  `id` int(11) NOT NULL,
  `desciption` text CHARACTER SET utf8 NOT NULL,
  `device_token` text NOT NULL,
  `user_mobile_no` varchar(20) NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpyc_districts`
--

CREATE TABLE `mpyc_districts` (
  `id` int(100) NOT NULL,
  `districtAdminId` int(100) DEFAULT NULL,
  `districtName` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `languageType` enum('0','1') DEFAULT '0' COMMENT '0-english,1-marathi'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpyc_district_admin`
--

CREATE TABLE `mpyc_district_admin` (
  `user_id` int(11) NOT NULL,
  `user_mobile_no` text CHARACTER SET utf8 NOT NULL,
  `facebook` text CHARACTER SET utf8 NOT NULL,
  `twitter` text CHARACTER SET utf8 NOT NULL,
  `user_name` text CHARACTER SET utf8 NOT NULL,
  `fname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `age` text CHARACTER SET utf8 NOT NULL,
  `user_dob` text CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user_address` text CHARACTER SET utf8 NOT NULL,
  `user_district` text CHARACTER SET utf8 NOT NULL,
  `user_role` text CHARACTER SET utf8 NOT NULL,
  `assembly` text CHARACTER SET utf8 NOT NULL,
  `user_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `designation` text CHARACTER SET utf8 NOT NULL,
  `user_status` int(11) NOT NULL COMMENT '0 pending, 1 otp send , 2 active , 3 remove ',
  `user_is` varchar(10) CHARACTER SET utf8 NOT NULL,
  `user_otp` varchar(4) CHARACTER SET utf8 NOT NULL,
  `device_token` text CHARACTER SET utf8 NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedOn` bigint(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpyc_survey`
--

CREATE TABLE `mpyc_survey` (
  `id` int(11) NOT NULL,
  `district` varchar(250) NOT NULL,
  `assembly` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `date` text NOT NULL,
  `user_mobile_no` varchar(20) NOT NULL,
  `device_token` text NOT NULL,
  `update_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpyc_users`
--

CREATE TABLE `mpyc_users` (
  `user_id` int(11) NOT NULL,
  `user_mobile_no` text CHARACTER SET utf8 NOT NULL,
  `facebook` text CHARACTER SET utf8 NOT NULL,
  `twitter` text CHARACTER SET utf8 NOT NULL,
  `user_name` text CHARACTER SET utf8 NOT NULL,
  `fname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `age` text CHARACTER SET utf8 NOT NULL,
  `user_dob` text CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user_address` text CHARACTER SET utf8 NOT NULL,
  `user_district` text CHARACTER SET utf8 NOT NULL,
  `user_role` text CHARACTER SET utf8 NOT NULL,
  `assembly` text CHARACTER SET utf8 NOT NULL,
  `user_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `designation` text CHARACTER SET utf8 NOT NULL,
  `user_status` int(11) NOT NULL COMMENT '0 pending, 1 otp send , 2 active , 3 remove ',
  `user_is` varchar(10) CHARACTER SET utf8 NOT NULL,
  `user_otp` varchar(4) CHARACTER SET utf8 NOT NULL,
  `device_token` text CHARACTER SET utf8 NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpyc_users_temp`
--

CREATE TABLE `mpyc_users_temp` (
  `user_id` int(11) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_mobile_no` varchar(12) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_status` int(1) NOT NULL COMMENT '0 pending , 1 active , 2 remove ',
  `user_type` int(1) NOT NULL COMMENT '0 admin / 1 department'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mpyc_users_temp`
--

INSERT INTO `mpyc_users_temp` (`user_id`, `user_email`, `user_mobile_no`, `user_password`, `user_name`, `user_status`, `user_type`) VALUES
(1, '', '7878787878', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news_alerts`
--

CREATE TABLE `news_alerts` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `status` int(1) NOT NULL COMMENT '0 pending , 1 active , 2 remove ',
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_district`
--

CREATE TABLE `tbl_district` (
  `id` int(100) NOT NULL,
  `districtName` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `marathiDistrictName` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `createdOn` bigint(200) DEFAULT NULL,
  `updatedOn` bigint(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message`
--

CREATE TABLE `tbl_message` (
  `id` int(11) NOT NULL,
  `message` varchar(250) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `priority` enum('Major','Minor') NOT NULL,
  `updateon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `status` varchar(25) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_status`
--

INSERT INTO `tbl_status` (`id`, `name`, `status`, `updated_on`) VALUES
(1, 'Meetings', 'true', '2019-07-29 07:13:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `department_user_id` int(11) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_mobile_no` varchar(12) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_status` int(1) NOT NULL COMMENT '0 pending , 1 active , 2 remove ',
  `user_type` int(1) NOT NULL COMMENT '0 admin / 1 department'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`department_user_id`, `user_email`, `user_mobile_no`, `user_password`, `user_name`, `user_status`, `user_type`) VALUES
(1, 'mustaquim.sayyed@onevoice.co.in', '8087586743', 'admin', 'admin', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_notification`
--

CREATE TABLE `user_notification` (
  `notification_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf32 NOT NULL,
  `body` text CHARACTER SET utf32 NOT NULL,
  `event_id` bigint(20) NOT NULL,
  `district_admin_id` int(11) NOT NULL,
  `type` int(1) NOT NULL COMMENT '0 : normal / 1 : event / 2 : news / 3: meeting',
  `device_token` text NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `user_id` int(11) NOT NULL,
  `user_mobile_no` text CHARACTER SET utf8 NOT NULL,
  `facebook` text CHARACTER SET utf8 NOT NULL,
  `twitter` text CHARACTER SET utf8 NOT NULL,
  `user_name` text CHARACTER SET utf8 NOT NULL,
  `fname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `age` text CHARACTER SET utf8 NOT NULL,
  `user_dob` text CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `adhar_no` bigint(20) NOT NULL,
  `pan_no` varchar(25) NOT NULL,
  `user_address` text CHARACTER SET utf8 NOT NULL,
  `user_city` varchar(250) NOT NULL,
  `user_district` text CHARACTER SET utf8 NOT NULL,
  `user_state` varchar(250) NOT NULL,
  `pin` int(11) NOT NULL,
  `bank_name` varchar(250) NOT NULL,
  `ac_no` int(20) NOT NULL,
  `ac_holder_name` varchar(250) NOT NULL,
  `ifsc` varchar(250) NOT NULL,
  `branch` varchar(250) NOT NULL,
  `branch_code` varchar(25) NOT NULL,
  `user_role` text CHARACTER SET utf8 NOT NULL,
  `assembly` text CHARACTER SET utf8 NOT NULL,
  `user_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `designation` text CHARACTER SET utf8 NOT NULL,
  `user_status` int(11) NOT NULL COMMENT '0 pending, 1 otp send , 2 active , 3 remove ',
  `user_is` varchar(10) CHARACTER SET utf8 NOT NULL,
  `user_otp` varchar(4) CHARACTER SET utf8 NOT NULL,
  `device_token` text CHARACTER SET utf8 NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`user_id`, `user_mobile_no`, `facebook`, `twitter`, `user_name`, `fname`, `lname`, `age`, `user_dob`, `email`, `adhar_no`, `pan_no`, `user_address`, `user_city`, `user_district`, `user_state`, `pin`, `bank_name`, `ac_no`, `ac_holder_name`, `ifsc`, `branch`, `branch_code`, `user_role`, `assembly`, `user_type`, `designation`, `user_status`, `user_is`, `user_otp`, `device_token`, `default_date`) VALUES
(1, '8087586743', '', '', '', 'vle1', 'shaikhh', '', '', 'atif@gmail.com', 5445678998765423, '12365256', 'sfdsa fgfd', 'pune', 'Pune', 'Maharashtra', 414020, 'SBI', 2147483647, 'Atif Shaikh', 'SBIN0000303', 'Ahmednagar', '303', '', '', '', '', 2, 'existing', '', '123456789', '2019-03-21 12:45:35'),
(3, '7777777777', '', '', '', 'vle2', 'patel', '', '', 'vle2@gmail.com', 88998989695896, 'H90Lo90-4', 'kdkkd lslsls .sllsls', 'pune', 'pune', 'Maharashtra', 414001, 'Axis', 456969886, 'vle2', 'AX000030', 'pune', '525', '', '', '', '', 2, 'existing', '', '123456789', '2019-03-28 09:43:07'),
(4, '5555665566', '', '', '', 'vle3', 'patel', '', '', 'vle3@gmail.com', 889989568896, 'H90k90-p4', 'kdkkd lslsls .sllsls', 'pune', 'pune', 'Maharashtra', 414001, 'Axis', 456969886, 'vle3', 'AX000030', 'pune', '525', '', '', '', '', 2, 'existing', '', '123456789', '2019-03-28 09:43:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `crop`
--
ALTER TABLE `crop`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`cropcat_id`),
  ADD KEY `vendor` (`vender_id`),
  ADD KEY `farmer` (`farmer_id`);

--
-- Indexes for table `crop_cat`
--
ALTER TABLE `crop_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farm`
--
ALTER TABLE `farm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farmer`
--
ALTER TABLE `farmer`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mpyc_contact`
--
ALTER TABLE `mpyc_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mpyc_districts`
--
ALTER TABLE `mpyc_districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mpyc_district_admin`
--
ALTER TABLE `mpyc_district_admin`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `mpyc_survey`
--
ALTER TABLE `mpyc_survey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mpyc_users`
--
ALTER TABLE `mpyc_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `mpyc_users_temp`
--
ALTER TABLE `mpyc_users_temp`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `news_alerts`
--
ALTER TABLE `news_alerts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_district`
--
ALTER TABLE `tbl_district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_status`
--
ALTER TABLE `tbl_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`department_user_id`);

--
-- Indexes for table `user_notification`
--
ALTER TABLE `user_notification`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `crop`
--
ALTER TABLE `crop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `crop_cat`
--
ALTER TABLE `crop_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `farm`
--
ALTER TABLE `farm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `farmer`
--
ALTER TABLE `farmer`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mpyc_contact`
--
ALTER TABLE `mpyc_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mpyc_districts`
--
ALTER TABLE `mpyc_districts`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mpyc_district_admin`
--
ALTER TABLE `mpyc_district_admin`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mpyc_survey`
--
ALTER TABLE `mpyc_survey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mpyc_users`
--
ALTER TABLE `mpyc_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mpyc_users_temp`
--
ALTER TABLE `mpyc_users_temp`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `news_alerts`
--
ALTER TABLE `news_alerts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_district`
--
ALTER TABLE `tbl_district`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_status`
--
ALTER TABLE `tbl_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `department_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_notification`
--
ALTER TABLE `user_notification`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `crop`
--
ALTER TABLE `crop`
  ADD CONSTRAINT `category` FOREIGN KEY (`cropcat_id`) REFERENCES `crop_cat` (`id`),
  ADD CONSTRAINT `farmer` FOREIGN KEY (`farmer_id`) REFERENCES `farmer` (`user_id`),
  ADD CONSTRAINT `vendor` FOREIGN KEY (`vender_id`) REFERENCES `vendors` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
