-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 23, 2019 at 05:47 AM
-- Server version: 5.5.62
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `biodhan`
--

-- --------------------------------------------------------

--
-- Table structure for table `crop_cat`
--

CREATE TABLE `crop_cat` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0=Inactive,1=Active',
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crop_cat`
--

INSERT INTO `crop_cat` (`id`, `name`, `status`, `default_date`) VALUES
(1, 'Sugaracane', '1', '2019-03-22 05:18:15'),
(2, 'Cotton', '1', '2019-03-22 05:18:15'),
(3, 'Bajrai', '1', '2019-03-22 05:20:39'),
(4, 'Wheat', '1', '2019-03-22 05:20:39'),
(5, 'Onion', '1', '2019-03-22 05:21:06'),
(6, 'Mung', '1', '2019-03-22 05:21:06'),
(7, 'Pepper', '1', '2019-03-22 05:21:19'),
(8, 'Other', '1', '2019-03-22 05:21:19');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `desciption` text CHARACTER SET utf8 NOT NULL,
  `start_date` text CHARACTER SET utf8 NOT NULL,
  `end_date` text CHARACTER SET utf8 NOT NULL,
  `rating` double NOT NULL,
  `location` text NOT NULL,
  `device_token` text NOT NULL,
  `user_mobile_no` varchar(20) NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) NOT NULL,
  `image_url` text NOT NULL,
  `type` int(1) NOT NULL COMMENT '0 profile / 1 event / 2 meetings /3 district admin /4 contact /5 Survey',
  `ref_id` bigint(20) NOT NULL COMMENT 'it is user id or event id '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image_url`, `type`, `ref_id`) VALUES
(1, 'http://biodhan.ilovengr.com/uploads/5/images_unitglo_mobile-mEo7EOhCiW.jpg', 0, 5),
(2, 'http://biodhan.ilovengr.com/uploads/6/images_unitglo_mobile-avdOADxQZG.jpg', 0, 6),
(3, 'http://biodhan.ilovengr.com/uploads/2/images_unitglo_mobile-YMMjxr26wc.png', 0, 2),
(4, 'http://biodhan.ilovengr.com/uploads/2/images_unitglo_mobile-SJysKY82Jl.png', 0, 2),
(5, 'http://biodhan.ilovengr.com/uploads/2/images_unitglo_mobile-ypYLjCWxby.png', 0, 2),
(6, 'http://biodhan.ilovengr.com/uploads/2/images_unitglo_mobile-8OwHQnA7GM.png', 0, 2),
(7, 'http://biodhan.ilovengr.com/uploads/2/images_unitglo_mobile-kq3q89jGX5.png', 0, 2),
(8, 'http://biodhan.ilovengr.com/uploads/2/images_unitglo_mobile-7RtJ2yHsYt.png', 0, 2),
(9, 'http://biodhan.ilovengr.com/uploads/7/images_unitglo_mobile-dGQQLv2VPI.jpg', 0, 7),
(10, 'http://biodhan.ilovengr.com/uploads/8/images_unitglo_mobile-LmK4y6rjaZ.ilovengr', 0, 8),
(11, 'http://biodhan.ilovengr.com/uploads/10/images_unitglo_mobile-wav9MJBvrn.jpg', 0, 10),
(12, 'http://biodhan.ilovengr.com/uploads/11/images_unitglo_mobile-cVw8Pis9TD.ilovengr', 0, 11),
(13, 'http://biodhan.ilovengr.com/uploads/12/images_unitglo_mobile-IvFtM6eFxt.jpg', 0, 12),
(14, 'http://biodhan.ilovengr.com/uploads/13/images_unitglo_mobile-XmalsayqfA.jpg', 0, 13),
(15, 'http://biodhan.ilovengr.com/uploads/14/images_unitglo_mobile-N3nhcvscts.jpg', 0, 14),
(16, 'http://biodhan.ilovengr.com/uploads/15/images_unitglo_mobile-lkDHsTGFtb.jpg', 0, 15),
(17, 'http://biodhan.ilovengr.com/uploads/16/images_unitglo_mobile-9ietFRsA8k.jpg', 0, 16),
(18, 'http://biodhan.ilovengr.com/uploads/18/images_unitglo_mobile-Tlz9PWTVIs.ilovengr', 0, 18),
(19, 'http://biodhan.ilovengr.com/uploads/19/images_unitglo_mobile-4S9pycYjZw.ilovengr', 0, 19),
(20, 'http://biodhan.ilovengr.com/uploads/19/images_unitglo_mobile-JFrW4sfcKb.jpg', 0, 19),
(21, 'http://biodhan.ilovengr.com/uploads/22/images_unitglo_mobile-8Ca6zFs3bl.jpg', 0, 22),
(22, 'http://biodhan.ilovengr.com/uploads/23/images_unitglo_mobile-OsHjlLV13F.jpg', 0, 23),
(23, 'http://biodhan.ilovengr.com/uploads/26/images_unitglo_mobile-tDR19iZdbL.3', 0, 26),
(24, 'http://biodhan.ilovengr.com/uploads/24/images_unitglo_mobile-45ke6FIHCE.jpg', 0, 24),
(25, 'http://biodhan.ilovengr.com/uploads/27/images_unitglo_mobile-VOgQaowLnH.jpg', 0, 27),
(26, 'http://biodhan.ilovengr.com/uploads/cropcat/sugarcane.jpg', 3, 1),
(27, 'http://biodhan.ilovengr.com/uploads/cropcat/cotton.jpg', 3, 2),
(28, 'http://biodhan.ilovengr.com/uploads/cropcat/bajrai.jpg', 3, 3),
(29, 'http://biodhan.ilovengr.com/uploads/cropcat/wheat.jpg', 3, 4),
(30, 'http://biodhan.ilovengr.com/uploads/cropcat/onion.jpg', 3, 5),
(31, 'http://biodhan.ilovengr.com/uploads/cropcat/mung.jpg', 3, 6),
(32, 'http://biodhan.ilovengr.com/uploads/cropcat/pepper.jpg', 3, 7),
(33, 'http://biodhan.ilovengr.com/uploads/cropcat/other.jpg', 3, 8),
(34, 'http://biodhan.ilovengr.com/uploads/29/images_unitglo_mobile-pzB2pWPwpd.jpg', 0, 29),
(35, 'http://biodhan.ilovengr.com/uploads/31/images_unitglo_mobile-WArSbtr2NL.jpg', 0, 31),
(36, 'http://biodhan.ilovengr.com/uploads/31/images_unitglo_mobile-tbWJpevlOk.jpg', 0, 31),
(37, 'http://biodhan.ilovengr.com/uploads/2/images_unitglo_mobile-iKLKDRNPGu.png', 0, 2),
(38, 'http://biodhan.ilovengr.com/uploads/2/images_unitglo_mobile-vyW6GL31PL.png', 0, 2),
(39, 'http://biodhan.ilovengr.com/uploads/2/images_unitglo_mobile-XtAHD1FTM4.png', 0, 2),
(40, 'http://biodhan.ilovengr.com/uploads/2/images_unitglo_mobile-pArFrEAGcA.png', 0, 2),
(41, 'http://biodhan.ilovengr.com/uploads/2/images_unitglo_mobile-ylgRsp95rZ.png', 0, 2),
(42, 'http://biodhan.ilovengr.com/uploads/2/images_unitglo_mobile-yKnzlq3dN7.png', 0, 2),
(43, 'http://biodhan.ilovengr.com/uploads/32/images_unitglo_mobile-hQflFQcfVb.jpg', 0, 32);

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

CREATE TABLE `meetings` (
  `id` int(11) NOT NULL,
  `device_token` text NOT NULL,
  `title` text CHARACTER SET utf8 NOT NULL,
  `date` text CHARACTER SET utf8 NOT NULL,
  `location` text CHARACTER SET utf8 NOT NULL,
  `mom` text CHARACTER SET utf8 NOT NULL,
  `next_meeting` text CHARACTER SET utf8 NOT NULL,
  `attendee` text CHARACTER SET utf8 NOT NULL,
  `user_mobile_no` text CHARACTER SET utf8 NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpyc_contact`
--

CREATE TABLE `mpyc_contact` (
  `id` int(11) NOT NULL,
  `desciption` text CHARACTER SET utf8 NOT NULL,
  `device_token` text NOT NULL,
  `user_mobile_no` varchar(20) NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpyc_department_user`
--

CREATE TABLE `mpyc_department_user` (
  `department_user_id` int(11) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_mobile_no` varchar(12) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_status` int(1) NOT NULL COMMENT '0 pending , 1 active , 2 remove ',
  `user_type` int(1) NOT NULL COMMENT '0 admin / 1 department'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpyc_districts`
--

CREATE TABLE `mpyc_districts` (
  `id` int(100) NOT NULL,
  `districtAdminId` int(100) DEFAULT NULL,
  `districtName` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `languageType` enum('0','1') DEFAULT '0' COMMENT '0-english,1-marathi'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpyc_district_admin`
--

CREATE TABLE `mpyc_district_admin` (
  `user_id` int(11) NOT NULL,
  `user_mobile_no` text CHARACTER SET utf8 NOT NULL,
  `facebook` text CHARACTER SET utf8 NOT NULL,
  `twitter` text CHARACTER SET utf8 NOT NULL,
  `user_name` text CHARACTER SET utf8 NOT NULL,
  `fname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `age` text CHARACTER SET utf8 NOT NULL,
  `user_dob` text CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user_address` text CHARACTER SET utf8 NOT NULL,
  `user_district` text CHARACTER SET utf8 NOT NULL,
  `user_role` text CHARACTER SET utf8 NOT NULL,
  `assembly` text CHARACTER SET utf8 NOT NULL,
  `user_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `designation` text CHARACTER SET utf8 NOT NULL,
  `user_status` int(11) NOT NULL COMMENT '0 pending, 1 otp send , 2 active , 3 remove ',
  `user_is` varchar(10) CHARACTER SET utf8 NOT NULL,
  `user_otp` varchar(4) CHARACTER SET utf8 NOT NULL,
  `device_token` text CHARACTER SET utf8 NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedOn` bigint(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpyc_survey`
--

CREATE TABLE `mpyc_survey` (
  `id` int(11) NOT NULL,
  `district` varchar(250) NOT NULL,
  `assembly` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `date` text NOT NULL,
  `user_mobile_no` varchar(20) NOT NULL,
  `device_token` text NOT NULL,
  `update_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpyc_users`
--

CREATE TABLE `mpyc_users` (
  `user_id` int(11) NOT NULL,
  `user_mobile_no` text CHARACTER SET utf8 NOT NULL,
  `facebook` text CHARACTER SET utf8 NOT NULL,
  `twitter` text CHARACTER SET utf8 NOT NULL,
  `user_name` text CHARACTER SET utf8 NOT NULL,
  `fname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `age` text CHARACTER SET utf8 NOT NULL,
  `user_dob` text CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user_address` text CHARACTER SET utf8 NOT NULL,
  `user_district` text CHARACTER SET utf8 NOT NULL,
  `user_role` text CHARACTER SET utf8 NOT NULL,
  `assembly` text CHARACTER SET utf8 NOT NULL,
  `user_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `designation` text CHARACTER SET utf8 NOT NULL,
  `user_status` int(11) NOT NULL COMMENT '0 pending, 1 otp send , 2 active , 3 remove ',
  `user_is` varchar(10) CHARACTER SET utf8 NOT NULL,
  `user_otp` varchar(4) CHARACTER SET utf8 NOT NULL,
  `device_token` text CHARACTER SET utf8 NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mpyc_users_temp`
--

CREATE TABLE `mpyc_users_temp` (
  `user_id` int(11) NOT NULL,
  `user_mobile_no` text CHARACTER SET utf8 NOT NULL,
  `facebook` text CHARACTER SET utf8 NOT NULL,
  `twitter` text CHARACTER SET utf8 NOT NULL,
  `user_name` text CHARACTER SET utf8 NOT NULL,
  `fname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `age` text CHARACTER SET utf8 NOT NULL,
  `user_dob` text CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user_address` text CHARACTER SET utf8 NOT NULL,
  `user_district` text CHARACTER SET utf8 NOT NULL,
  `user_role` text CHARACTER SET utf8 NOT NULL,
  `assembly` text CHARACTER SET utf8 NOT NULL,
  `user_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `designation` text CHARACTER SET utf8 NOT NULL,
  `user_status` int(11) NOT NULL COMMENT '0 pending, 1 otp send , 2 active , 3 remove ',
  `user_is` varchar(10) CHARACTER SET utf8 NOT NULL,
  `user_otp` varchar(4) CHARACTER SET utf8 NOT NULL,
  `device_token` text CHARACTER SET utf8 NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `news_alerts`
--

CREATE TABLE `news_alerts` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `status` int(1) NOT NULL COMMENT '0 pending , 1 active , 2 remove ',
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_district`
--

CREATE TABLE `tbl_district` (
  `id` int(100) NOT NULL,
  `districtName` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `marathiDistrictName` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `createdOn` bigint(200) DEFAULT NULL,
  `updatedOn` bigint(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message`
--

CREATE TABLE `tbl_message` (
  `id` int(11) NOT NULL,
  `message` varchar(250) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `priority` enum('Major','Minor') NOT NULL,
  `updateon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `status` varchar(25) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_mobile_no` text CHARACTER SET utf8 NOT NULL,
  `facebook` text CHARACTER SET utf8 NOT NULL,
  `twitter` text CHARACTER SET utf8 NOT NULL,
  `user_name` text CHARACTER SET utf8 NOT NULL,
  `fname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `age` text CHARACTER SET utf8 NOT NULL,
  `user_dob` text CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `adhar_no` int(20) DEFAULT NULL,
  `pan_no` bigint(20) NOT NULL,
  `user_address` text CHARACTER SET utf8 NOT NULL,
  `user_city` varchar(250) DEFAULT NULL,
  `user_district` text CHARACTER SET utf8 NOT NULL,
  `user_state` varchar(250) DEFAULT NULL,
  `pin` int(11) DEFAULT NULL,
  `user_role` text CHARACTER SET utf8 NOT NULL,
  `assembly` text CHARACTER SET utf8 NOT NULL,
  `user_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `designation` text CHARACTER SET utf8 NOT NULL,
  `user_status` int(11) NOT NULL COMMENT '0 pending, 1 otp send , 2 active , 3 remove ',
  `user_is` varchar(10) CHARACTER SET utf8 NOT NULL,
  `user_otp` varchar(4) CHARACTER SET utf8 NOT NULL,
  `device_token` text CHARACTER SET utf8 NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_mobile_no`, `facebook`, `twitter`, `user_name`, `fname`, `lname`, `age`, `user_dob`, `email`, `adhar_no`, `pan_no`, `user_address`, `user_city`, `user_district`, `user_state`, `pin`, `user_role`, `assembly`, `user_type`, `designation`, `user_status`, `user_is`, `user_otp`, `device_token`, `default_date`) VALUES
(2, '9175917762', '', '', '', 'Mustaquimm', 'Sayyed', '', '', 'mustaquim.sayyed@gmail.com', 2147483647, 123321123, 'Nahoha, Industries, Next to Bus stand', 'Ahmednagar', 'Ahmednagar', 'Maharashtra', 414001, '', '', '', '', 2, 'existing', '', '123456789', '2019-03-20 10:42:17'),
(20, '9860609068', '', '', '', '', '', '', '', '', NULL, 0, '', NULL, '', NULL, NULL, '', '', '', '', 1, 'new', '1234', '', '2019-03-22 04:44:49'),
(24, '9890048567', '', '', '', 'Narendra', 'Firodia', '', '', 'narendrafirod@123', 123156, 0, '2rfyjj', 'Qefgun', 'Qdfgv', 'Mhfa', 414001, '', '', '', '', 2, 'existing', '', 'faEu0MQgzS4:APA91bH4jLc-RHkTygzr7TGTLdkaEVSyFZKg6qXvMNVpcUebCMY9-2ZH-3aE4mNzfA3NqCunBrca-QmDVdMv3LqjkuQ0gP8zWgskeiMHwQ1-LOvQg9kZ2pPft_WEfiYJ6L91dZejjYZn', '2019-03-22 05:39:02'),
(25, '9833235520', '', '', '', '', '', '', '', '', NULL, 0, '', NULL, '', NULL, NULL, '', '', '', '', 2, 'new', '', 'digI-ABNNz8:APA91bFvIZ3OjC4iOyCFgASHTKRZyLDiehOPNX-85oK0TAE0gpEYiu7IIe8HRdAqDmlgOroYB9k8TpaUUZXGGXl3k_3vSNgCZY65FzK2JUUEOBz0i5gWNMc-7HUyS2hLbxNvTGgJPbes', '2019-03-22 05:39:11'),
(26, '9821331356', '', '', '', 'R', 'K', '', '', 'ak@gmail.com', 768856, 0, 'N', 'Mumbai', 'M', 'M', 414001, '', '', '', '', 2, 'existing', '', 'esdzi0SQ4vI:APA91bGLlQjZQg5O-4AXTh9k7lsSDWBErGX7m2Xzaa0yBMeTbamDuLi8Hd6k3sIcBkUmY7aRN3qp1htnVUfgyercdMPCt16Am4Id6zkBVonG0BCBQYmupOKC4lfEdjoGI1rpGuzsVgPk', '2019-03-22 05:39:19'),
(27, '8898057100', '', '', '', 'Monhhhb', 'Bbccbb', '', '', 'bshsbah@gmail.con', 53948284, 0, 'Buxbsbs', 'Bubsbs', 'Hhbb', 'Gahabdie', 666666, '', '', '', '', 2, 'existing', '', 'eZsE7cTNMa8:APA91bEdHRBmRUjKKZQcWvvRkxDhbzXwbw4abUokqnDSGW4oRR5dLtZxeu3sRC2wtKCVwQkvaBHuL5uDhNzXzrWZJHZFHHOHvCGoqxpEBjX8cnPij6lIixmQ2b7w5bF0l4APf0adZ1iw', '2019-03-22 05:39:35'),
(28, '9822401235', '', '', '', '', '', '', '', '', NULL, 0, '', NULL, '', NULL, NULL, '', '', '', '', 2, 'new', '', 'dPmk8orbeuw:APA91bHAVODlvHOL-_ksMsU_jnvFtSTY6pIBM4T0sEQlQwX0rbHSw1412DNCxANnmWLlEG-tU9rZPONzqdDQHUWxz1VtcUCWucn4dCRaGY09zLmvYvbgCt0MBtNMoCvCmtyM5nls6EjS', '2019-03-22 06:10:32'),
(29, '9822401232', '', '', '', 'Dnyaneshwar', 'Dalvi', '', '', 't@t.com', 2147483647, 0, 'Sjjsjsjsj\n', 'Shdhhd', 'Dhdhhd', 'Shhshs', 2147483647, '', '', '', '', 2, 'existing', '', 'faEu0MQgzS4:APA91bH4jLc-RHkTygzr7TGTLdkaEVSyFZKg6qXvMNVpcUebCMY9-2ZH-3aE4mNzfA3NqCunBrca-QmDVdMv3LqjkuQ0gP8zWgskeiMHwQ1-LOvQg9kZ2pPft_WEfiYJ6L91dZejjYZn', '2019-03-22 06:15:19'),
(32, '9284638880', '', '', '', 'Gdxydd', 'Tddx', '', '', 'hcch@rtt.hhj', 86688, 868, 'Cycyy\nYydyd\n    ', 'Chchcjdjdjd', 'Yfyc', 'Chfyccy', 6868, '', '', '', '', 2, 'existing', '', 'faEu0MQgzS4:APA91bH4jLc-RHkTygzr7TGTLdkaEVSyFZKg6qXvMNVpcUebCMY9-2ZH-3aE4mNzfA3NqCunBrca-QmDVdMv3LqjkuQ0gP8zWgskeiMHwQ1-LOvQg9kZ2pPft_WEfiYJ6L91dZejjYZn', '2019-03-22 15:42:09');

-- --------------------------------------------------------

--
-- Table structure for table `user_notification`
--

CREATE TABLE `user_notification` (
  `notification_id` int(11) NOT NULL,
  `title` text CHARACTER SET utf32 NOT NULL,
  `body` text CHARACTER SET utf32 NOT NULL,
  `event_id` bigint(20) NOT NULL,
  `district_admin_id` int(11) NOT NULL,
  `type` int(1) NOT NULL COMMENT '0 : normal / 1 : event / 2 : news / 3: meeting',
  `device_token` text NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `user_id` int(11) NOT NULL,
  `user_mobile_no` text CHARACTER SET utf8 NOT NULL,
  `facebook` text CHARACTER SET utf8 NOT NULL,
  `twitter` text CHARACTER SET utf8 NOT NULL,
  `user_name` text CHARACTER SET utf8 NOT NULL,
  `fname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `age` text CHARACTER SET utf8 NOT NULL,
  `user_dob` text CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `user_address` text CHARACTER SET utf8 NOT NULL,
  `user_district` text CHARACTER SET utf8 NOT NULL,
  `user_role` text CHARACTER SET utf8 NOT NULL,
  `assembly` text CHARACTER SET utf8 NOT NULL,
  `user_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `designation` text CHARACTER SET utf8 NOT NULL,
  `user_status` int(11) NOT NULL COMMENT '0 pending, 1 otp send , 2 active , 3 remove ',
  `user_is` varchar(10) CHARACTER SET utf8 NOT NULL,
  `user_otp` varchar(4) CHARACTER SET utf8 NOT NULL,
  `device_token` text CHARACTER SET utf8 NOT NULL,
  `default_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedOn` bigint(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `crop_cat`
--
ALTER TABLE `crop_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mpyc_contact`
--
ALTER TABLE `mpyc_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mpyc_department_user`
--
ALTER TABLE `mpyc_department_user`
  ADD PRIMARY KEY (`department_user_id`);

--
-- Indexes for table `mpyc_districts`
--
ALTER TABLE `mpyc_districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mpyc_district_admin`
--
ALTER TABLE `mpyc_district_admin`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `mpyc_survey`
--
ALTER TABLE `mpyc_survey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mpyc_users`
--
ALTER TABLE `mpyc_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `mpyc_users_temp`
--
ALTER TABLE `mpyc_users_temp`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `news_alerts`
--
ALTER TABLE `news_alerts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_district`
--
ALTER TABLE `tbl_district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_status`
--
ALTER TABLE `tbl_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_notification`
--
ALTER TABLE `user_notification`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `crop_cat`
--
ALTER TABLE `crop_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1354;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=220;

--
-- AUTO_INCREMENT for table `mpyc_contact`
--
ALTER TABLE `mpyc_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `mpyc_department_user`
--
ALTER TABLE `mpyc_department_user`
  MODIFY `department_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mpyc_districts`
--
ALTER TABLE `mpyc_districts`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `mpyc_district_admin`
--
ALTER TABLE `mpyc_district_admin`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `mpyc_survey`
--
ALTER TABLE `mpyc_survey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mpyc_users`
--
ALTER TABLE `mpyc_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=892;

--
-- AUTO_INCREMENT for table `mpyc_users_temp`
--
ALTER TABLE `mpyc_users_temp`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=302;

--
-- AUTO_INCREMENT for table `news_alerts`
--
ALTER TABLE `news_alerts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_district`
--
ALTER TABLE `tbl_district`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `tbl_status`
--
ALTER TABLE `tbl_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `user_notification`
--
ALTER TABLE `user_notification`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
