function initTinymce() {
				// tinymce.remove('');
							tinymce.init({selector:'textarea', 
								plugins: "code link visualblocks table emoticons template paste textcolor colorpicker textpattern ", 
								height: 200,
								menubar: false,
								browser_spellcheck: true,
								theme:'modern',
								skin: "lightgray",
								relative_urls : false,
								remove_script_host : false,
	// document_base_url : ,
								resize: true,
								statusbar: true,
								extended_valid_elements : "span[!class]",
								imagetools_cors_hosts: ['www.tinymce.com', 'codepen.io'],
								  content_css: [
								    '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
								    '//www.tinymce.com/css/codepen.min.css'
								  ],
								toolbar: "undo redo | formatselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link code mybutton |forecolor | backcolor | emoticons | table ",
								visualblocks_default_state: true,
								setup: function(editor) {
									editor.addButton('mybutton', {
										type: 'button',
										title: 'Insert image',
										icon: 'image',
										id: 'mybutton'
									});
									editor.on('init', function(e) {
										var pluploadHandler = new PluploadHandler(jQuery, plupload, 'html', 800 );
									});
								}			
							});		
}
function image_add(url){
	var img = "<img src='" + url+ "'>";
	tinymce.activeEditor.execCommand('mceInsertContent', false, img);
}
$(function () {
	  $('[data-toggle="tooltip"]').tooltip();
});
window.onload = function() {
	reload_funaction();
}
	function reload_funaction(){
		$('#login-btn').click(function(e) {
			e.preventDefault();
			var data_to_send = {
					data : $("form#login-form").serializeArray()
					};
				$.ajax({
							url : $('button#login-btn').attr('data-url'),
							data : data_to_send,
							type : 'post',
								statusCode : {
									500 : function() {
										show_message('danger',"<b>Server Error Contact To Unitglo Solutions.</b>");
									}
								},
								beforeSend : function() {
									$('#login-btn').attr('disabled','disabled');
								},
								success : function(returnHtml) {
									var message = "";
									try {
										returnHtml = JSON.parse(returnHtml);
										if (returnHtml.message == "fail") {
											$('#login-btn').removeAttr('disabled');
											show_message('danger','Please, Check Username Or Password!');
											if($('.error_div').length==1){
												$('.error_div').html("Please, Check Username Or Password!");
											}
										} else if (returnHtml.message == "done") {
											show_message('success','Login Successful!',1000,function(){ show_message('success','Please, Wait...',1000,function(){ window.location=returnHtml.url; }); });
										}
									} catch (err) {
										show_message('danger',"Error at : "+err);
										$('#login-btn').removeAttr('disabled');
							}
					},complete : function(data) {}
			});
		});
		$('form.POST_AJAX > button').unbind('click').click(function(e) {
			e.preventDefault();
			var url='';
			var failMessage = "Something Went Wrong, Please,Retry!!!";
			var doneMessage = "Success...";
			var parent_this=this;
			var data_call=$(this).val();
			var data_to_send = null;
			url = $(this).attr('data-url');
			if(data_call=="verify-user"){
				failMessage = "Check your Mobile Number OR Email ID is Wrong!!!";
				doneMessage = "OTP has been sent to you.";
			}else if(data_call=="check-otp"){
				failMessage = "Wrong OTP!!!";
				doneMessage = "OTP Verified.";
			}else if(data_call=="changePassword"){
				doneMessage = "Password Changed.";
			}else if(data_call=="save_html_data"){
				url = $(this).parent().attr('action');
				doneMessage = "Saved Successfully.";
				if($("#tinymceTextarea").length!=0){
				var contents = tinyMCE.get('tinymceTextarea').getContent();
				$('#tinymceTextarea').html(contents);}
				var form = $(this).parent()[0];
		        var data = new FormData(form);
				data_to_send = data;
			}else{
				url = $(this).parent().attr('action');
				if($(this).attr('fail_message')!=''){
					failMessage = $(this).attr('fail_message');
				}
				if(!validate($(this).parent()[0])){
					return false;
				}
				if($(this).attr('done_message')!='')
					doneMessage = $(this).attr('done_message');
				var form = $(this).parent()[0];
		        var data = new FormData(form);
				data_to_send = data;
			}
			if(url!=''){
				var contentType = false;
				var processData = false;
				if(data_to_send==null){
					data_to_send = { data : $(parent_this).parent().serializeArray() };
					contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
					processData = true;
				}
			$.ajax({
						url : url,
						data : data_to_send,
						type : 'post',
						enctype: 'multipart/form-data',
						cache : false,
						contentType : contentType,
						processData : processData,
						statusCode : {
							500 : function() {
								show_message('danger',"<b>Server Error Contact To Unitglo Solutions.</b>");
							}
						},
						beforeSend : function() {
							if(!validatation(parent_this,data_call)){
								show_message('danger',"I Think, You Missing Someting!!!");
								return false;
							}
							if($(parent_this).attr('ajax_events')=="true"){
								$(parent_this).attr('disabled','disabled');
							}
						},
						success : function(returnHtml) {
							if(data_call=="data"){
								$(parent_this).parent().next().html(returnHtml);
							}else{
							var message = "";
							try {
								returnHtml = JSON.parse(returnHtml);
								if (returnHtml.message == "fail") {
									if($('.error_div').length==1){
										$('.error_div').html(returnHtml.error_message);
									}
									if(returnHtml.error_message!=undefined){
										// console.log(returnHtml.error_message);
										show_message('danger',returnHtml.error_message);
									}else
										show_message('danger',failMessage);
									if($(parent_this).attr('ajax_events')=="true"){
										$(parent_this).removeAttr('disabled');
									}
								} else if (returnHtml.message == "done") {
									if(returnHtml.done_message!=undefined){
										doneMessage = returnHtml.done_message;
									}
									if(data_call=="changePassword"){
										show_message('success',doneMessage,1000,function(){ show_message('success','Please, Wait...',1000,function(){  window.location=returnHtml.url;  }); });
									}else if(data_call=="verify-user" || data_call=="check-otp"){
										show_message('success',doneMessage,1000,function(){
											if(data_call=="verify-user"){
												$(parent_this).parent().parent().find('form').css('display','none');
												$($(parent_this).parent().parent().find('form')[1]).find('div.form-group button').click();
												$($(parent_this).parent().parent().find('form')[1]).css('display','block');
											}else if(data_call=="check-otp"){
												$(parent_this).parent().parent().find('form').css('display','none');
												$($(parent_this).parent().parent().find('form')[2]).css('display','block');
											}
										});
									}else{
										show_message('success',doneMessage,1500,function(){   
											if(returnHtml.url!=undefined) window.location=returnHtml.url; 
										});
									}
								}else{
									if($(parent_this).attr('ajax_events')=="true"){
										$(parent_this).removeAttr('disabled');
									}
									show_message('danger',"<b>ERROR!</b> "+failMessage);
								}
							} catch (err) { 								
								if($(parent_this).attr('ajax_events')=="true"){
									$(parent_this).removeAttr('disabled');
								}
								show_message('danger',"Error at : "+err); }
							}
						},
					complete : function(data) {}
				});
			}else{
				show_message('danger',"<b>WARNING!</b> "+failMessage);
			}
		});
		
		//Adduser Temp
		$('#add-user-btn').click(function(e) {
			e.preventDefault();
			var data_to_send = {
					data : $("form#user-form").serializeArray()
					};
					console.log(data_to_send);
				$.ajax({
						url : $('button#add-user-btn').attr('data-url'),
						data : data_to_send,
						type : 'post',
							statusCode : {
								500 : function() {
									show_message('danger',"<b>Server Error Contact.</b>");
								}
							},
							beforeSend : function() {
								$('button#add-user-btn').attr('disabled','disabled');
							},
							success : function(returnHtml) {
								var message = "";
								try {
									returnHtml = JSON.parse(returnHtml);
									if (returnHtml.message == "fail") {
										$('#add-user-btn').removeAttr('disabled');
										show_message('danger',returnHtml.reason);
										// if($('.error_div').length==1){
											// $('.error_div').html("Please, Check Username Or Password!");
										// }
									} else if (returnHtml.message == "done") {
										$('#add-user-btn').removeAttr('disabled');
										show_message('success',returnHtml.reason,1000,function(){ show_message('success','Please, Wait...',1000,function(){ window.location=returnHtml.url; }); });
										// $("form#user-form")[0].reset();
									}
								} catch (err) {
									show_message('danger',"Error at : "+err);
									$('#add-user-btn').removeAttr('disabled');
						}
					},complete : function(data) {}
			});
		});
		
		//Add & Edit data
		$('#data-form-btn').click(function(e) {
			e.preventDefault();
			var data_to_send = {
					data : $("form#data-form").serializeArray()
					};
					console.log(data_to_send);
				$.ajax({
						url : $('button#data-form-btn').attr('data-url'),
						data : data_to_send,
						type : 'post',
							statusCode : {
								500 : function() {
									show_message('danger',"<b>Server Error Contact.</b>");
								}
							},
							beforeSend : function() {
								$('button#data-form-btn').attr('disabled','disabled');
							},
							success : function(returnHtml) {
								var message = "";
								try {
									returnHtml = JSON.parse(returnHtml);
									if (returnHtml.message == "fail") {
										$('#data-form-btn').removeAttr('disabled');
										show_message('danger',returnHtml.reason);
										// if($('.error_div').length==1){
											// $('.error_div').html("Please, Check Username Or Password!");
										// }
									} else if (returnHtml.message == "done") {
										$('#data-form-btn').removeAttr('disabled');
										show_message('success',returnHtml.reason);
										// show_message('success',returnHtml.reason,1000,function(){ show_message('success','Please, Wait...',1000,function(){ window.location=returnHtml.url; }); });
										// $("form#user-form")[0].reset();
									}
								} catch (err) {
									show_message('danger',"Error at : "+err);
									$('#data-form-btn').removeAttr('disabled');
						}
					},complete : function(data) {}
			});
		});
		
		
		$('#user_district').change(function(e) {
			e.preventDefault();
			// var data_to_send = {
			var dist = $("#user_district").val();
			// };
			console.log(dist);
				$.ajax({
						url : $('#user_district').attr('data-url'),
						data : 'district=' +dist,
						type : 'post',
							statusCode : {
								500 : function() {
									show_message('danger',"<b>Server Error Contact.</b>");
								}
							},
							beforeSend : function() {
								// $('button#data-form-btn').attr('disabled','disabled');
							},
							success : function(returnHtml) {
								console.log(returnHtml);
								var message = "";
								try {
									returnHtml = JSON.parse(returnHtml);
									if (returnHtml.message == "fail") {
										show_message('danger',returnHtml.reason);
									} else if (returnHtml.message == "done") {
										// show_message('success',returnHtml.reason);
											$('#assembly').html("<option value='' ></option>");
										$.each(returnHtml.assembly, function(i, item) {
											$('#assembly').append("<option value='"+item+"' >"+item+"</option>");
										})
									}
								} catch (err) {
									show_message('danger',"Error at : "+err);
									// $('#data-form-btn').removeAttr('disabled');
						}
					},complete : function(data) {}
			});
			// alert('d');
		});
		
	}

//Delete any row
function delete_row(e){
	console.log($('button#deleteRowBtn').attr('data-url'));
	// var uniqueid = $('button#deleteRowBtn').attr('data-uniqueid');
	console.log('id =' +e);
	if (!confirm('Are you sure?')) return false;
	$.ajax({
			url : $('button#deleteRowBtn').attr('data-url'),
			data : 'id =' +e,
			type : 'post',
			statusCode : {
				500 : function() {
					show_message('danger',"<b>Server Error Contact.</b>");
				}
			},
				
			success : function(returnHtml) {
				console.log(returnHtml);
				var message = "";
				try {
					returnHtml = JSON.parse(returnHtml);
					if (returnHtml.message == "fail") {
						show_message('danger',returnHtml.reason);
					} else if (returnHtml.message == "done") {
						$('button#deleteRowBtn[data-uniqueid="'+e+'"]').parents("tr").remove(); // remove row after delete
						show_message('success',returnHtml.reason);
					}
				} catch (err) {
					show_message('danger',"Error at : "+err);
				}
			},complete : function(data) {}
	});
}	

function btn_model_save_change(e){
	var url = $(e).val();
	$(e).next().click();
	var doneMessage = "Operation Successful...";
	var failMessage = "Failed!!!";
	$.ajax({
		url : url,
		type : 'post',
		statusCode : {
			500 : function() {
				show_message('danger',"<b>Server Error Contact To Unitglo Solutions.</b>");
			}
		},
		beforeSend : function() { 
		},
		success : function(returnHtml) {
			var message = "";
			try {
				returnHtml = JSON.parse(returnHtml);
				if (returnHtml.message == "fail") {
					if(returnHtml.error_message!=undefined){
						console.log(returnHtml.error_message);
						show_message('danger',returnHtml.error_message);
					}else
						show_message('danger',failMessage);
				} else if (returnHtml.message == "done") {
						show_message('success',doneMessage,1500,function(){   
							if(returnHtml.url!=undefined) 
								window.location=returnHtml.url; 
							else
								load_ajax_call(window.location.href+"/_ajax",".table_div_id");
							});
				}
			} catch (err) { show_message('danger',"Error at : "+err); }
	},
	complete : function(data) {}
});
}
function load_ajax_call(url,data_load_div){
	$.ajax({
		url : url,
		type : 'post',
		statusCode : {
			500 : function() {
				show_message('danger',"<b>Server Error Contact To Unitglo Solutions.</b>");
			}
		},
		beforeSend : function() { },
		success : function(returnHtml) {
			$(data_load_div).html('');	
			$($(data_load_div).parent().children()[0]).after(returnHtml);
		},
	complete : function(data) {}
});
}
function ajaxLoader(obj,data_to_send=null){
	var data_to_send1 = new FormData();
	var file_data = null;
    $.each($(data_to_send).find('input[type="file"]'),function(key,input){
    	data_to_send1.append(input.name,input.files[0]);
    });
//console.log($(data_to_send).find(':input').serializeArray());
	 var other_data = $(data_to_send).find(':input').serializeArray();
	    $.each(other_data,function(key,input){
	    	data_to_send1.append(input.name,input.value);
	    });
	$.ajax({
		url : $(obj).attr('data-url'),
		type : 'POST',
		data : data_to_send1,
		enctype: 'multipart/form-data',
		// cache : false,
		processData: false,
		contentType: false,
		statusCode : {
			500 : function() {
				show_message('danger',"<b>Server Error Contact To Unitglo Solutions.</b>");
			}
		},
		beforeSend : function() { },
		success : function(returnHtml) {
			eval(returnHtml);
		},
	complete : function(data) {}
});
}
function validate(e){
	var flag = true;
	$($(e).find('.form-control[required]')).each(function(item){
		if($(this).val()=='' || $(this).val()==null || $(this).val()==0 || $(this).val()=='NaN'){
			$(this).focus();
			flag = false;
			return false;
		}
	});
	return flag;
}
function show_message(alert_type='info',alert_message='Welcome!',timeout=2500,timeoutFunction=function(){}) {
	$('#alert-pops-shadow').html('<div class="w-35 alert alert-'+alert_type+' alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><b>'+alert_message+'<b></div></div>');
	setTimeout(function(){
			$("#alert-pops-shadow>.alert").alert('close');
			},timeout);
	setTimeout(timeoutFunction,timeout);
}
function ValidateEmail()  
{  
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var id_name = arguments[0];
	var etxt=document.getElementById(id_name).value;
	return (etxt.match(mailformat))?true:false;  
}
// read image file
function readURL1hhh(input) {
    if (input.files && input.files[0]) {
    var reader = new FileReader();
	    reader.onload = function (e) {
        $('#'+$(input).attr("img-id"))
            .attr('src', e.target.result)
            .width(200)
            .height(200);
    };                    reader.readAsDataURL(input.files[0]);
}
}

function isNumber(evt) {
	evt = (evt) ? evt : window.event;
	var id_name = arguments[1];
	var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ( charCode < 48  || charCode > 58) {    	
        document.getElementById(""+id_name ).style.borderColor = 'red';
        return false;
    }
    else {
    	document.getElementById(""+id_name ).style.borderColor = '';
        return true;
	}
}