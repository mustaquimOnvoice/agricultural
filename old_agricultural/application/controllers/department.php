<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class department extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		if (! isset ( $_SESSION ['id'] )) {
			$this->logout ();
		}
	}
	function district($id=null){
		$data ['display_contents'] = array (
				"id" => "ID",
				"districtName" => "District Name",
				"editdistrict" => "Action"
		);
		$data['module']="district";
		$data ['table_data'] = $this->Base_Models->CustomeQuary("SELECT *, '' as editdistrict FROM (`tbl_district`) ");
		//echo $this->db->last_query();
		if ($id != null)
			$this->load->view ( "common/table-view", $data );
		else
            $this->view ( "common/table-view", $data );
	}
	function district_head($id=null){
		$data ['display_contents'] = array (
				"user_id" => "ID",
				"fname" => "First Name",
				"lname" => "Last Name",
				"user_district_value" => "District",
				"edit" => "Action"
		);
		$data['module']="district_admin";
		$data ['table_data'] = $this->Base_Models->CustomeQuary("SELECT *, '' as edit FROM (`mpyc_district_admin`) ");
		//echo $this->db->last_query();
		if ($id != null)
			$this->load->view ( "common/table-view", $data );
		else
            $this->view ( "common/table-view", $data );
	}

	function addDistrictAdmin(){
		
		
		$data['module']="add_district_admin";

		//echo $this->db->last_query();
	
            $this->view ( "common/table-view",$data);
		
	}
	function addDistrict(){
		
		
		$data['module']="add_district";

		//echo $this->db->last_query();
	
            $this->view ( "common/table-view",$data);
		
	}
	function edit_district_admin($id=""){
		
		
		$data['module']="edit_district_admin";
$data ['table_data'] = $this->Base_Models->GetSingleDetails ( "mpyc_district_admin" , array('user_id' => $id ));
if($data ['table_data']->user_id == ""){
	redirect('department/district_head');
}
		//echo $this->db->last_query();
	
            $this->view ( "common/table-view",$data);
		
	}
	function edit_district($id=""){
		
		
		$data['module']="edit_district";
$data ['table_data'] = $this->Base_Models->GetSingleDetails ( "tbl_district" , array('id' => $id ));
if($data ['table_data']->id == ""){
	redirect('department/district');
}
		//echo $this->db->last_query();
	
            $this->view ( "common/table-view",$data);
		
	}
	
	function updateDistrict(){
		$update_district_admin_id=$this->input->post('update_district_admin_id');
		$data ['fname']=trim($this->input->post('edit_district_admin_first_name'));
		$data ['lname']=trim($this->input->post('edit_district_admin_last_name'));
		$data ['user_mobile_no']=trim($this->input->post('edit_district_admin_mobile'));
		$data ['email']=trim($this->input->post('edit_district_admin_email'));
		$data ['user_address']=trim($this->input->post('edit_district_admin_address'));
		$data ['designation']=trim($this->input->post('edit_district_admin_designation'));
		$data ['user_role']=trim($this->input->post('edit_district_admin_role'));
		$data ['assembly']=trim($this->input->post('edit_district_admin_assembly'));
		$data ['user_dob']=date("Y-m-d",strtotime($this->input->post('edit_district_admin_dob')));
		$data ['updatedOn']=strtotime(date("Y-m-d H:i:s")) * 1000;
		//$data ['user_district']=rtrim($this->input->post('edit_exist_district'),",");
		$check_mobile_no = $this->Base_Models->GetAllValues ( "mpyc_district_admin" , array('user_mobile_no' => $data ['user_mobile_no'],'user_id <>' => $update_district_admin_id ));
		 $check_edited_email = $this->Base_Models->GetAllValues ( "mpyc_district_admin" , array('email' => $data ['email'],'user_id <>' => $update_district_admin_id));
		 if(count($check_mobile_no) > 0){
		 	echo json_encode(array("count" => 1, "status" => FALSE));
		 }
		 else
		 	 if(count($check_edited_email) > 0){
		 	echo json_encode(array("count" => 2, "status" => FALSE));
		 }
		else{
		$temp = $this->Base_Models->RemoveValues ( "mpyc_districts", array (
                    "districtAdminId" => $update_district_admin_id));
            $temp = $this->Base_Models->UpadateValue ( "mpyc_district_admin", $data, array (
                    "user_id" => $update_district_admin_id));
         //start to update mpyc district in english
            $selected_admin_district=rtrim($this->input->post('edit_exist_district'),",");
             $fetch_id=explode(",",$selected_admin_district);
        foreach($fetch_id as $fetch_id_data){
           $input_district_array[] = array(
                        "districtAdminId" => $update_district_admin_id,
                        "districtName  " => $fetch_id_data,
                        "languageType  " => '0',
                    );
        }
        $this->db->insert_batch('mpyc_districts', $input_district_array);
        //end to update mpyc district in english
         //start to update mpyc district in marathi
           
        foreach($fetch_id as $fetch_id_data){
        	$district_marathi_data= $this->Base_Models->GetSingleDetails ( "tbl_district" , array('districtName' => $fetch_id_data ));
           $input_marathi_district_array[] = array(
                        "districtAdminId" => $update_district_admin_id,
                        "districtName" => $district_marathi_data->marathiDistrictName,
                        "languageType  " => '1',
                    );
        }
        $this->db->insert_batch('mpyc_districts', $input_marathi_district_array);
        //end to update mpyc district in marathi
        echo json_encode(array("count" => 0, "status" => TRUE));
    	}
    
		
	}
	function updateDistrictData(){
		$update_district_id=$this->input->post('update_district_id');
		$data ['districtName']=trim($this->input->post('edit_district_name'));
		$data ['marathiDistrictName']=trim($this->input->post('edit_district_name_marathi'));
		$data ['updatedOn']=strtotime(date("Y-m-d H:i:s")) * 1000;
		$check_district = $this->Base_Models->GetAllValues ( "tbl_district" , array('districtName' => $data ['districtName'],'id <>' => $update_district_id ));
		$check_district_marathi = $this->Base_Models->GetAllValues ( "tbl_district" , array('marathiDistrictName' => $data ['marathiDistrictName'],'id <>' => $update_district_id ));
		if(count($check_district) > 0){
			echo json_encode(array("count" => 1, "status" => FALSE));
			
        }
         else
         	if(count($check_district_marathi) > 0){
			echo json_encode(array("count" => 2, "status" => FALSE));
			
        }
         else
         {

	//------------start to update district
            $temp = $this->Base_Models->UpadateValue ( "tbl_district", $data, array (
                    "id" => $update_district_id));
            //------------end to update district
            //------------start to update district head in english
            $new_data ['districtName']=trim($this->input->post('edit_district_name'));
            $exist_district_name=trim($this->input->post('exist_district_name'));
            $english_temp = $this->Base_Models->UpadateValue ( "mpyc_districts", $new_data, array (
                    "districtName" => $exist_district_name));
             //------------end to update district head in english

            //------------start to update district head in marathi
            $new_data ['districtName']=trim($this->input->post('edit_district_name_marathi'));
            $exist_district_marathi_name=trim($this->input->post('exist_district_marathi_name'));
            $marathi_temp = $this->Base_Models->UpadateValue ( "mpyc_districts", $new_data, array (
                    "districtName" => $exist_district_marathi_name));
             //------------end to update district head in marathi
            echo json_encode(array("count" => 0, "status" => TRUE));
}  
		
	}
	function deleteDistrict(){
		$delete_district_admin_id=$this->input->post('delete_district_admin_id');;
            $temp = $this->Base_Models->RemoveValues ( "mpyc_district_admin", array (
                    "user_id" => $delete_district_admin_id));
             $temp = $this->Base_Models->RemoveValues ( "mpyc_districts", array (
                    "districtAdminId" => $delete_district_admin_id));
	}
	function deleteSingleDistrict(){
		$delete_district_id=$this->input->post('delete_district_id');
		$table_data= $this->Base_Models->GetSingleDetails ( "tbl_district" , array('id' => $delete_district_id ));
          $temp = $this->Base_Models->RemoveValues ( "tbl_district", array (
                    "id" => $delete_district_id));
            $temp1 = $this->Base_Models->RemoveValues ( "mpyc_districts", array (
                    "districtName" => $table_data->districtName));
            $temp3 = $this->Base_Models->RemoveValues ( "mpyc_districts", array (
                    "districtName" => $table_data->marathiDistrictName));
	}
	
	
	function addNewDistrictAdmin(){
		
		
		$district_admin_first_name=trim($this->input->post('district_admin_first_name'));
		$district_admin_last_name=trim($this->input->post('district_admin_last_name'));
		$district_admin_mobile=trim($this->input->post('district_admin_mobile'));
		$district_admin_email=trim($this->input->post('district_admin_email'));
		$district_admin_address=trim($this->input->post('district_admin_address'));
		$district_admin_designation=trim($this->input->post('district_admin_designation'));
		$district_admin_role=trim($this->input->post('district_admin_role'));
		$district_admin_assembly=trim($this->input->post('district_admin_assembly'));
		$user_dob=date("Y-m-d",strtotime($this->input->post('district_admin_dob')));
		 $selected_admin_district = rtrim($this->input->post('selected_admin_district'),",");
		 $check_mobile_no = $this->Base_Models->GetAllValues ( "mpyc_district_admin" , array('user_mobile_no' => $district_admin_mobile ));
		 $check_email = $this->Base_Models->GetAllValues ( "mpyc_district_admin" , array('email' => $district_admin_email ));
		 if(count($check_mobile_no) > 0){
		 	echo json_encode(array("count" => 1, "status" => FALSE));
		 }
		 else
		 	 if(count($check_email) > 0){
		 	echo json_encode(array("count" => 2, "status" => FALSE));
		 }
		 else
		{
		            $input_array = array(
                        "fname" => $district_admin_first_name,
                        "lname  " => $district_admin_last_name,
                        "user_mobile_no  " => $district_admin_mobile,
                        "email  " => $district_admin_email,
                        "user_address  " => $district_admin_address,
                        "user_dob  " => $user_dob,
                        "designation  " => $district_admin_designation,
                        "user_role  " => $district_admin_role,
                        "assembly  " => $district_admin_assembly
                    );
        $this->db->insert('mpyc_district_admin', $input_array);
        $insert_id=$this->db->insert_id();
         $fetch_id=explode(",",$selected_admin_district);
         //start to insert mpyc district in english
        foreach($fetch_id as $fetch_id_data){
           $input_district_array[] = array(
                        "districtAdminId" => $insert_id,
                        "districtName  " => $fetch_id_data,
                         "languageType  " => '0'
                    );
        }
        $this->db->insert_batch('mpyc_districts', $input_district_array); 
        //end to insert mpyc district in english

         //start to insert mpyc district in marathi
           
        foreach($fetch_id as $fetch_id_data){
        	$district_marathi_data= $this->Base_Models->GetSingleDetails ( "tbl_district" , array('districtName' => $fetch_id_data ));
           $input_marathi_district_array[] = array(
                        "districtAdminId" => $insert_id,
                        "districtName" => $district_marathi_data->marathiDistrictName,
                        "languageType  " => '1',
                    );
        }
        $this->db->insert_batch('mpyc_districts', $input_marathi_district_array);
        //end to insert mpyc district in marathi
        echo json_encode(array("count" => 0, "status" => TRUE));
    	}
    	
	}
		function addNewDistrict(){
		$add_district_name=trim($this->input->post('add_district_name'));
		$add_district_name_marathi=trim($this->input->post('add_district_name_marathi'));
		$check_district = $this->Base_Models->GetAllValues ( "tbl_district" , array('districtName' => $add_district_name ));
		$check_district_marathi = $this->Base_Models->GetAllValues ( "tbl_district" , array('marathiDistrictName' => $add_district_name_marathi ));
if(count($check_district) > 0){
	echo json_encode(array("count" => 1, "status" => FALSE));
	
}
else
	if(count($check_district_marathi) > 0){
	echo json_encode(array("count" => 2, "status" => FALSE));
	
}
else
{
$input_array = array(
                        "districtName" => $add_district_name,
                        "marathiDistrictName" => $add_district_name_marathi,
                        "createdOn  " => strtotime(date("Y-m-d H:i:s")) * 1000
                    );
        $this->db->insert('tbl_district', $input_array);
	echo json_encode(array("count" => 0, "status" => TRUE));
	
}
		
		            

	}
	function city_head($id=null){
		$data ['display_contents'] = array (
				"department_user_id" => "ID",
				"user_name" => "User Name",
				"user_email" => "User Email",
				"user_mobile_no" => "Contact"
		);
		$data ['table_data'] = $this->Base_Models->GetAllValues ( "mpyc_department_user" , array('user_type' => 2 ));
		if ($id != null)
			$this->load->view ( "common/table-view", $data );
		else
            $this->view ( "common/table-view", $data );
	}
	public function index($id = null) {
        
        if ($_SESSION ['role'] != "Admin") {
			exit ( "<script>history.back();</script>" );
		}
		$_SESSION ['active_btn'] = "Departments";
		$_SESSION ['active_tag'] = "Departments";
		$data ['title'] = "Department";
		$data ['add_url'] = base_url ( 'department/add_department' );
		$data ['display_contents'] = array (
				"department_id" => "ID",
				"department_name" => "Department Name",
				"application_count" => "Total applications",
				"pending_application" => "Pending application Count",
				"department_status" => "Status",
				"action" => "Actions" 
		);
		$data ['table_data'] = $this->Base_Models->GetAllValues ( "corporation_department" );
		foreach ( $data ['table_data'] as $key => $val ) {
			$data ['table_data'] [$key] ['id'] = $key + 1;
			$edit = " <button  onclick='window.location=\"" . base_url("department/add_department/".$data ['table_data'] [$key] ['department_id']) . "\"' class='btn btn-outline-info'><i class='fa fa-edit' data-toggle='tooltip' data-placement='top' title='Edit Department'></i></button> ";
			$activate = " <button  onclick='$(\"#myModalActionBtn\").attr(\"value\",$(this).attr(\"data-url\")); $(\"#myModalBody\").html(\"Are you sure to activate this record?\"); $(\"#myModalLabel\").html(\"Activate Record\"); $(\"#myModalLabel\").parent().parent().css(\"background-color\",\"#fff5f5\");' class='btn btn-outline-success'  data-url='" . base_url () . "department/activate_department/" . $data ['table_data'] [$key] ['department_id'] . "' data-target='#myModal' data-toggle='modal'><i class='fa fa-unlock-alt' data-toggle='tooltip' data-placement='left' title='Activate Department'></i></button>";
			$deactivate = " <button  onclick='$(\"#myModalActionBtn\").attr(\"value\",$(this).attr(\"data-url\")); $(\"#myModalBody\").html(\"Are you sure to deactivate this record?\"); $(\"#myModalLabel\").html(\"Deactivate Record\"); $(\"#myModalLabel\").parent().parent().css(\"background-color\",\"#fff5f5\");' class='btn btn-outline-danger'  data-url='" . base_url () . "department/deactivate_department/" . $data ['table_data'] [$key] ['department_id'] . "' data-target='#myModal' data-toggle='modal'><i class='fa fa-lock' data-toggle='tooltip' data-placement='left' title='Deactivate Department'></i></button>";
			$data ['table_data'] [$key] ['action'] = $edit;
			if ($data ['table_data'] [$key] ['department_status'] == 0 && $data ['table_data'] [$key] ['department_id'] != '' && $data ['table_data'] [$key] ['department_id'] != '0') {
				$data ['table_data'] [$key] ['action'] .= $activate ;
			} elseif ($data ['table_data'] [$key] ['department_status'] == 1) {
				$data ['table_data'] [$key] ['action'] .= $deactivate ;
			} elseif ($data ['table_data'] [$key] ['department_status'] == 2) {
				$data ['table_data'] [$key] ['action'] .= $activate ;
			}
			$data ['table_data'] [$key] ['department_status'] = ($val ['department_status'] == 0) ? "Pending" : ($val ['department_status'] == 1 ? "Active" : ($val ['department_status'] == 2 ? "Deactivated" : ""));
		}
		if ($id != null)
			$this->load->view ( "common/table-view", $data );
		else
            $this->view ( "common/table-view", $data );
            
            
    }
    function add_department($id=null){
        $data= null;
        if(isset($id))
        {$data1=$this->Base_Models->GetAllValues ( "corporation_department" ,array("department_id"=>$id));
            $data=$data1[0];
        }
        $data ['title'] = "Departments";
		$data ['action'] = $id == null ? base_url ( 'department/accept_department' ) : base_url ( 'department/accept_department/' . $id );
		$data ['sub_title'] = ($id == null ? "Add" : "Update") . " Department";
		$data ['done_message'] = "Department " . ($id == null ? "Added..." : "Updated...");
        $this->view ( "forms/department", $data );
        
        
    }
    function accept_department($id=null){
    
        $response ['message'] = "fail";
        $post = $_POST;
        $data = array ();
        if ($id != null) {
           
            $data ['department_notice'] = $post ['department_notice'];
            $data ['department_name'] = $post ['department_name'];
            $temp = $this->Base_Models->UpadateValue ( "corporation_department", $data, array (
                    "department_id" => $id 
            ) );
            if ($temp != 0) {
                $response ['message'] = "done";
                $response ['url'] = "../";
            }
        } else {
         
            $data ['department_notice'] = $post ['department_notice'];
            $data ['department_name'] = $post ['department_name'];
            $temp = $this->Base_Models->AddValues ( "corporation_department", $data );
            if ($temp != 0) {
                $response ['message'] = "done";
                $response ['url'] = "./";
            }
        }
        echo json_encode ( $response );
    }
    function activate_department($id) {
        $response ['message'] = "fail";
		if (isset ( $id )) {
			$temp = $this->Base_Models->UpadateValue ( "corporation_department", array (
					"department_status" => 1
			), array (
					"department_id" => $id 
			) );
			if ($temp != 0) {
				$response ['message'] = "done";
				$response ['url'] = "./";
			}
		}
		echo json_encode ( $response );
    }
    function deactivate_department($id) {
		$response ['message'] = "fail";
		if (isset ( $id )) {
			$temp = $this->Base_Models->UpadateValue ( "corporation_department", array (
					"department_status" => 2 
			), array (
					"department_id" => $id 
			) );
			if ($temp != 0) {
				$response ['message'] = "done";
				$response ['url'] = "./";
			}
		}
		echo json_encode ( $response );
	}



// Departments user

	public function users($department_id = null, $id = null) {
        //user_type
        if ($_SESSION ['role'] != "Admin") {
			exit ( "<script>history.back();</script>" );
		}
		$_SESSION ['active_btn'] = "Departments Users";
		$_SESSION ['active_tag'] = "Department Users";
		$data ['title'] = "Department Users";
		$data ['add_url'] = base_url ( 'department/add_department_user' );
		$data ['display_contents'] = array (
				"user_id" => "ID",
				"department_name" => "Department Name",
				"user_name" => "User Name",
				"user_mobile_no" => "Contact No",
				"user_name" => "User Name",
				"application_count" => "Total applications",
				"pending_application" => "Pending application Count",
				"user_status" => "Status",
				"user_type" => "Type",
				"action" => "Actions" 
		);
		$data['user_type']=1;
		if(isset($department_id))
		$data['department_id']=$department_id;
		
		
		$data ['table_data'] = $this->Base_Models->GetAllValues ( "corporation_users inner join corporation_department on corporation_department.department_id=corporation_users.department_id " );
		foreach ( $data ['table_data'] as $key => $val ) {
			$data ['table_data'] [$key] ['id'] = $key + 1;
			$edit = " <button  onclick='window.location=\"./add_department_user/" . $data ['table_data'] [$key] ['department_id'] . "\"' class='btn btn-outline-info'><i class='fa fa-edit' data-toggle='tooltip' data-placement='top' title='Edit Department'></i></button> ";
			$activate = " <button  onclick='$(\"#myModalActionBtn\").attr(\"value\",$(this).attr(\"data-url\")); $(\"#myModalBody\").html(\"Are you sure to activate this record?\"); $(\"#myModalLabel\").html(\"Activate Record\"); $(\"#myModalLabel\").parent().parent().css(\"background-color\",\"#fff5f5\");' class='btn btn-outline-success'  data-url='" . base_url () . "department/activate_department_user/" . $data ['table_data'] [$key] ['user_id'] . "' data-target='#myModal' data-toggle='modal'><i class='fa fa-unlock-alt' data-toggle='tooltip' data-placement='left' title='Activate User'></i></button>";
			$deactivate = " <button  onclick='$(\"#myModalActionBtn\").attr(\"value\",$(this).attr(\"data-url\")); $(\"#myModalBody\").html(\"Are you sure to deactivate this record?\"); $(\"#myModalLabel\").html(\"Deactivate Record\"); $(\"#myModalLabel\").parent().parent().css(\"background-color\",\"#fff5f5\");' class='btn btn-outline-danger'  data-url='" . base_url () . "department/deactivate_department_user/" . $data ['table_data'] [$key] ['user_id'] . "' data-target='#myModal' data-toggle='modal'><i class='fa fa-lock' data-toggle='tooltip' data-placement='left' title='Deactivate User'></i></button>";
			
			if($data ['table_data'] [$key] ['user_type']=="1"){
				$data ['table_data'] [$key] ['action'] = $edit;
				$data ['table_data'] [$key] ['user_type']="Department";
			if ($data ['table_data'] [$key] ['user_status'] == 0 && $data ['table_data'] [$key] ['department_id'] != '' && $data ['table_data'] [$key] ['department_id'] != '0') {
				$data ['table_data'] [$key] ['action'] .= $activate ;
			} elseif ($data ['table_data'] [$key] ['user_status'] == 1) {
				$data ['table_data'] [$key] ['action'] .= $deactivate ;
			} elseif ($data ['table_data'] [$key] ['user_status'] == 2) {
				$data ['table_data'] [$key] ['action'] .= $activate ;
			}
		}else{
			$data ['table_data'] [$key] ['user_type']="Admin";
			$data ['table_data'] [$key] ['action'] = "";
		}
			$data ['table_data'] [$key] ['user_status'] = ($val ['user_status'] == 0) ? "Pending" : ($val ['user_status'] == 1 ? "Active" : ($val ['user_status'] == 2 ? "Deactivated" : ""));
		}
		if ($id != null)
			$this->load->view ( "common/table-view", $data );
		else
            $this->view ( "common/table-view", $data );
            
            
    }
    function add_department_user($id=null){
        $data= null;
        if(isset($id))
        {
			$data1=$this->Base_Models->GetAllValues ( "corporation_users" ,array("user_id"=>$id));
			$data=$data1[0];
			
		}
		$data['departments']=$this->Base_Models->GetAllValues ( "corporation_department",array("department_status"=>1) );
        $data ['title'] = "Departments";
		$data ['action'] = $id == null ? base_url ( 'department/accept_department_user' ) : base_url ( 'department/accept_department_user/' . $id );
		$data ['sub_title'] = ($id == null ? "Add" : "Update") . " Department";
		$data ['done_message'] = "Department " . ($id == null ? "Added..." : "Updated...");
        $this->view ( "forms/department_user", $data );
        
        
    }
    function accept_department_user($id=null){
    
        $response ['message'] = "fail";
        $post = $_POST;
        $data = array ();
        if ($id != null) {
           
			$data ['user_name'] = $post ['user_name'];
			$data ['user_password'] = $post ['user_password'];
			$data ['user_email'] = $post ['user_email'];
			$data ['user_mobile_no'] = $post ['user_mobile_no'];

            $data ['department_id'] = $post ['department_id'];
            $data ['department_name'] = $post ['department_name'];
			$temp = $this->Base_Models->UpadateValue ( "corporation_users", $data, array (
                    "user_id" => $id 
            ) );
            if ($temp != 0) {
                $response ['message'] = "done";
                $response ['url'] = "../users";
            }
        } else {
         
			$data ['user_name'] = $post ['user_name'];
			$data ['user_password'] = $post ['user_password'];
			$data ['user_email'] = $post ['user_email'];
			$data ['user_mobile_no'] = $post ['user_mobile_no'];

            $data ['department_id'] = $post ['department_id'];
			$data ['department_name'] = $post ['department_name'];
			$data ['user_type'] = "1";
            $temp = $this->Base_Models->AddValues ( "corporation_users", $data );
            if ($temp != 0) {
                $response ['message'] = "done";
                $response ['url'] = "./users";
            }
        }
        echo json_encode ( $response );
    }
    function activate_department_user($id) {
        $response ['message'] = "fail";
		if (isset ( $id )) {
			$temp = $this->Base_Models->UpadateValue ( "corporation_users", array (
					"user_status" => 1
			), array (
					"user_id" => $id 
			) );
			if ($temp != 0) {
				$response ['message'] = "done";
				$response ['url'] = "./users";
			}
		}
		echo json_encode ( $response );
    }
    function deactivate_department_user($id) {
		$response ['message'] = "fail";
		if (isset ( $id )) {
			$temp = $this->Base_Models->UpadateValue ( "corporation_users", array (
					"user_status" => 2 
			), array (
					"user_id" => $id 
			) );
			if ($temp != 0) {
				$response ['message'] = "done";
				$response ['url'] = "./users";
			}
		}
		echo json_encode ( $response );
	}
}
?>