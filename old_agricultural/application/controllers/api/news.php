<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class news extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		//check_token
		if(!empty($_POST['user_type']) && $_POST['user_type'] == 'district_admin' ){
			$this->api_model->check_token('mpyc_district_admin',$_POST['user_mobile_no'],$_POST['device_token']);
		}else if(!empty($_POST['user_type']) && $_POST['user_type'] == 'user' ){
			$this->api_model->check_token('mpyc_users',$_POST['user_mobile_no'],$_POST['device_token']);
		}else{
			$response ['message'] = "fail";
			$response ['result'] =  "User type not found";
			echo json_encode($response);
			die();
		}
    }
    function news_list(){
        $response ['message'] = "fail";
        $response ['result'] =  "Unable to access";

        $news_alerts = null;
        if(isset($_POST['user_mobile_no']) && isset($_POST['device_token'])){

          if(isset($_POST['news_id'])){
                $news_alerts= $this->Base_Models->GetAllValues ( "news_alerts" , array('id' =>$_POST['news_id'] ));
          }else{
                $news_alerts= $this->Base_Models->GetAllValues ( "news_alerts" ,array("status"=>"1") );
          }
        $response ['news_list'] = $news_alerts;

        $response ['message'] = "done";
        $response ['result'] =  "News List";
        
        }
        echo json_encode($response);
    }

}
?>