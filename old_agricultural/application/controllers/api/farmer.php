<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class farmer extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		//check_token
		if(!empty($_POST['type']) && ($_POST['type'] == 'vendor' ||$_POST['type'] == 'Vendor' ||$_POST['type'] == 'VENDOR') ){
			$this->api_model->check_token('vendors',$_POST['user_mobile_no'],$_POST['device_token']);
		}else if(!empty($_POST['type']) && ($_POST['type'] == 'farmer' || $_POST['type'] == 'Farmer' || $_POST['type'] == 'FARMER') ){
			$this->api_model->check_token('users',$_POST['user_mobile_no'],$_POST['device_token']);
		}else{
			$response ['message'] = "fail";
			$response ['result'] =  "User type not found";
			echo json_encode($response);
			die();
		}
	}
	
	function profile(){
			
		$response ['message'] = "fail";
		$response ['result']="";
		if(isset($_POST['device_token']) && 
			isset($_POST['fname'])  && 
			isset($_POST['lname'])  && 
			isset($_POST['user_mobile_no'])&&			
			isset($_POST['email']) && 
			isset($_POST['adhar_no']) &&
			isset($_POST['pan_no']) &&
			isset($_POST['user_address']) &&
			isset($_POST['user_city']) &&			
			isset($_POST['user_district']) &&			
			isset($_POST['user_state']) &&			
			isset($_POST['pin'])		
		){
			
			$details = $this->Base_Models->GetAllValues ( "users", array ("user_mobile_no" => $_POST['user_mobile_no']),"user_id");

			if(count($details)==1){
			
				$tbdata['fname']=$_POST['fname'];
				$tbdata['lname']=$_POST['lname'];
				$tbdata['email']=$_POST['email'];
				$tbdata['adhar_no']=$_POST['adhar_no'];
				$tbdata['pan_no']=$_POST['pan_no'];
				$tbdata['user_address']=$_POST['user_address'];
				$tbdata['user_city']=$_POST['user_city'];
				$tbdata['user_district']=$_POST['user_district'];
				$tbdata['user_state']=$_POST['user_state'];
				$tbdata['pin']=$_POST['pin'];
				$tbdata['user_is']="existing";
				$_POST['image_url']="";
				$response ['message'] = "done";
				foreach ($_FILES as $key => $value) {
					$imgresponse = $this->uploadImageFile($value,$details[0] ['user_id'] );
					$_POST['image_url']=$imgresponse['image_url'];
				}

					$temp = $this->Base_Models->UpadateValue ( "users",$tbdata, array (
								"user_id" => $details[0] ['user_id'] 
						) );
				$details = $this->Base_Models->GetAllValues ( "users", array ("user_mobile_no" => $_POST['user_mobile_no']),"*, (SELECT image_url FROM  `images` where ref_id=users.user_id AND type = 0 ORDER BY id DESC LIMIT 1) as image_url");
				$response ['result'] = "Profile updated successfully";
				$response ['farmer_id'] = $details[0]['user_id'];
				$response ['fname'] = $details[0]['fname'];
				$response ['fname'] = $details[0]['fname'];
				$response ['lname'] = $details[0]['lname'];
				$response ['email'] = $details[0]['email'];
				$response ['adhar_no'] = $details[0]['adhar_no'];
				$response ['pan_no'] = $details[0]['pan_no'];
				$response ['user_address'] = $details[0]['user_address'];
				$response ['user_city'] = $details[0]['user_city'];
				$response ['user_district'] = $details[0]['user_district'];
				$response ['user_state'] = $details[0]['user_state'];
				$response ['pin'] = $details[0]['pin'];
				$response['user_is']="existing";
				$response['image_url']=$details[0]['image_url'];			
				// $response['data']=$details;			
				// $response['query']=$this->db->last_query();			
				
			}else{
				$response ['result'] = "User Not exist";

			}

				log_message('error', 'update : '.print_r($temp,true));
		}else{
				$response ['result'] = "Pram Not match";
		}
			log_message('error', 'Post OBJ : '.print_r($_POST,true)." File ".print_r($_FILES,true));
			log_message('error', 'response : '.print_r($response,true));
			echo json_encode ( $response );
	}
	
	function profile_details(){
			
		$response ['message'] = "fail";
		$response ['result']="";
		if(isset($_POST['device_token']) && 
			
			isset($_POST['user_mobile_no']) 		
		){
			// 		$tbdata['user_district']=$_POST['user_district'];
			// $tbdata['user_role']=$_POST['user_role'];
			// $tbdata['assembly']=$_POST['assembly'];

		$details = $this->Base_Models->GetAllValues ( "users", array (
						"user_mobile_no" => $_POST['user_mobile_no']
				),"*, (SELECT image_url FROM  `images` where ref_id=users.user_id ORDER BY id DESC LIMIT 1) as image_url" );

			$response ['result'] = json_encode($details);

		
			
		}
		echo json_encode ( $response );
	}
	
	function uploadImageFile($file,$user_id,$type=0) {
		$response ['message'] = "fail";
		if (isset ( $user_id) ) {
			if (isset ( $file ) && $file ['error'] == 0) {
				if (! file_exists ( APPPATH . "../uploads/" . $user_id )) {
					mkdir ( APPPATH . "../uploads/" . $user_id, 0777, true );
				}
					log_message('error', 'img  file: '.print_r($file,true));
				$temp = "uploads/" . $user_id . "/images_unitglo_mobile-" . $this->generate_random_string ( 10 );
				if ($temp != "") {
					$image_folder = APPPATH . "../" . $temp;
					list ( $a, $b ) = explode ( '.', $file ['name'] );
					$result = $this->imageCompress ( $file ['tmp_name'], $image_folder . "." . $b, 80 );
					if ($result != '') {
						$response ['message'] = "done";
						$response ['image_url'] = base_url ( $temp . "." . $b );

						$TableValues ['ref_id'] = $user_id;
						$TableValues ['type'] = $type;
						$TableValues ['image_url'] = $response ['image_url'];

						$response ['upload_id'] = $this->Base_Models->AddValues ( "images", $TableValues );
					}
				}
			}
		}
		//log_message('error', 'img : '.print_r($response,true));
		return  $response ;
	}

	function update_profile(){
		$response ['message'] = "fail";
		if(isset($_POST['device_token']) && isset($_POST['age'])  && isset($_POST['fname'])  && isset($_POST['lname'])  && isset($_POST['emial']) ){

		}	

		echo json_encode ( $response );
	}
	
	function notification(){

	   $response ['message'] = "fail";
		$response ['result'] =  "Unable to access";
		if(isset($_POST['user_mobile_no']) && isset($_POST['device_token'])){
	  
			$user_notification = $this->Base_Models->GetAllValues ( "user_notification" ,array("device_token"=>$_POST['device_token']) );

				
			$response ['message'] = "done";
			$response ['result'] =  "Notification List";
			$response ['notifications'] =  $user_notification;
		}

		echo json_encode($response);
	}
	
	function contact_us(){
		$response ['message'] = "fail";
		$response ['result']="";
		if( isset($_POST['device_token']) &&
			isset($_POST['desciption'])  && 
			isset($_POST['user_mobile_no'])         
		){
			$TableValues['desciption']=$_POST['desciption'];
			$TableValues['user_mobile_no']=$_POST['user_mobile_no'];
			$TableValues['device_token']=$_POST['device_token'];

			$id= $this->Base_Models->AddValues ( "mpyc_contact", $TableValues );
			foreach ($_FILES as $key => $value) {
				$imgresponse = $this->uploadImageFile($value,$id,4);            
			}
			$response ['message'] = "done";
			$response ['result']="Contact successfully";
		}

		// log_message('error', 'img  file: '.print_r($_POST,true));
		   echo json_encode($response);
    }
	
}
?>