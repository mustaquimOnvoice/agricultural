<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class login extends Base_Controller {
	public function __construct() {
		parent::__construct ();
	}

	/**
	 * Registration of New User
	 *
	 * @param
	 *        	Parameters as
     *          user_mobile_no
     *          device_token
     * 
	 * @return message string and result
	 Login type : user, vendor
	 */

    public function status() {
        $response ['message'] = "done";
        $response ['version'] =  "1.0";
        $response ['maintenance_mode'] =  false;
        echo json_encode($response);
    }
	
	public function registration() {
		$response ['message'] = "fail";
		$response ['result'] =  "OTP not send";
        // echo json_encode($_POST);
        // exit();
        if( isset($_POST['user_mobile_no']) && isset($_POST['type']) ){
           $TableValues['user_mobile_no']= $_POST['user_mobile_no'];
			$type = $_POST['type'];
				if($type == 'farmer' || $type == 'Farmer' || $type == 'FARMER'){
					// $temp1 = $this->Base_Models->GetAllValues("mpyc_users_temp",$TableValues);
					// if(count($temp1)==0){
						// $response ['message'] = "fail";
						// $response ['result'] =  "User not found";
					// }else{
						
						$temp = $this->Base_Models->GetAllValues("users",$TableValues);
						// $TableValues['device_token']= $_POST['device_token'];
						$otp = "";
						$send_message = "Hi, your OTP for Biodhan is ";

						if(count($temp)==0){
							// $str = $this->generate_random_string ( 4, 'num' );
							// if($TableValues['user_mobile_no'] == '9175917762'){
								$str="1234";
							// }
							$TableValues['user_otp']=$str;
							$TableValues['user_status']="1";
							$TableValues['user_is']="new";
							$otp=$str;
							
				
							$temp = $this->Base_Models->AddValues ( "users", $TableValues);
							$response ['type'] = "new";
							$response ['message'] = "done";
							$response ['result'] =  "OTP send";
							

						}else {
							
							if($temp[0]['user_otp']==""){  
								// $TableValues['device_token']= $_POST['device_token'];
								// $str = $this->generate_random_string ( 4, 'num' );
								// if($TableValues['user_mobile_no'] == '9503346902'){
									$str="1234";
								// }
								$otp=$str;
								
								 $this->Base_Models->UpadateValue ( "users", array (
									"user_otp" =>$str,
									// "device_token"=>$TableValues["device_token"],
									"user_status"=>1
								), array (
									"user_id" => $temp[0]['user_id']
								) );
							}else{
								$this->Base_Models->UpadateValue ( "users", array (
										// "device_token"=>$TableValues["device_token"],
										"user_status"=>1

								), array (
										"user_id" => $temp[0]['user_id']
								) );
								$otp=$temp[0]['user_otp'];
							}
							$response ['type'] = $temp[0]['user_is'];
							$response ['message'] = "done";
							$response ['result'] =  "OTP send";
			   
						}
							$send_message=$send_message.$otp." ";
							//$this->message_send ( $send_message,  $TableValues['user_mobile_no']);
					// }
				}else if($type == 'vendor' || $type == 'Vendor' || $type == 'VENDOR'){
					$temp = $this->Base_Models->GetAllValues("vendors",$TableValues);
					// $TableValues['device_token']= $_POST['device_token'];
					$otp = "";
					$send_message = "Hi, your OTP for Biodhan is ";

					if(count($temp)==0){
						$response ['message'] = "fail";
						$response ['result'] =  "District Admin not found";        

					}else {
						
						if($temp[0]['user_otp']==""){  
							// $TableValues['device_token']= $_POST['device_token'];
							// $str = $this->generate_random_string ( 4, 'num' );
							$str="1234";
							$otp=$str;
							
							 $this->Base_Models->UpadateValue ( "vendors", array (
								"user_otp" =>$str,
								// "device_token"=>$TableValues["device_token"],
								"user_status"=>1
							), array (
								"user_id" => $temp[0]['user_id']
							) );
						}else{
							$this->Base_Models->UpadateValue ( "vendors", array (
									// "device_token"=>$TableValues["device_token"],
									"user_status"=>1

							), array (
									"user_id" => $temp[0]['user_id']
							) );
							$otp=$temp[0]['user_otp'];
						}
						$response ['message'] = "done";
						$response ['result'] =  "OTP send";
						
						$send_message=$send_message.$otp." ";
						//$this->message_send ( $send_message,  $TableValues['user_mobile_no']);
		   
					}
				}
            
        }else {
            $response ['message'] = "fail";
        }

		echo json_encode ( $response );
    }


    public function otp_varification(){
		$response ['message'] = "fail";
		$response ['result'] =  "OTP not varify";
        if( isset($_POST['user_mobile_no'])  && isset($_POST['device_token']) && isset($_POST['user_otp'])){
           $TableValues['user_mobile_no']= $_POST['user_mobile_no'];
           $TableValues['user_otp']= $_POST['user_otp']; 
		
			$type = $_POST['type'];
				if($type == 'farmer' || $type == 'Farmer' || $type == 'FARMER'){					
					$cnt = $this->Base_Models->GetAllValues("users",$TableValues,"user_id");
					$TableValues['device_token']= $_POST['device_token'];
					$TableValues['user_otp']="";
					$TableValues['user_status']=2;
					
					if(count($cnt)==1){
						$this->Base_Models->UpadateValue ( "users",$TableValues, array (
							"user_id" => $cnt[0]['user_id']
						) );
						
						$temp = $this->Base_Models->GetAllValues("users",$TableValues,"device_token,fname,lname,user_id,user_is,email,user_address,user_mobile_no,user_city,user_district,user_state,adhar_no,pan_no,pin,(SELECT image_url FROM `images` where ref_id=users.user_id and type=0 ORDER BY id DESC LIMIT 1) as image_url");
						$response ['type'] = $temp[0]['user_is'];
						$response ['message'] = "done";
						// $response ['result'] = json_encode($temp[0]);
						$response ['result'] = $temp[0];
					}else{
						$response ['message'] = "fail";
					}
				}else if($type == 'vendor' || $type == 'Vendor' || $type == 'VENDOR'){
					$cnt = $this->Base_Models->GetAllValues("vendors",$TableValues,"user_id");
					$TableValues['device_token']= $_POST['device_token'];
					$TableValues['user_otp']="";
					$TableValues['user_status']=2;
					
					if(count($cnt)==1){
						$this->Base_Models->UpadateValue ( "vendors",$TableValues, array (
							"user_id" => $cnt[0]['user_id']
						) );
						
						$temp = $this->Base_Models->GetAllValues("vendors",$TableValues,"device_token,fname,lname,user_id,user_is,email,user_address,user_mobile_no,(SELECT image_url FROM  `images` where ref_id=vendors.user_id and type=3 ORDER BY id DESC LIMIT 1) as image_url, (SELECT GROUP_CONCAT(districtName) FROM mpyc_districts where districtAdminId=vendors.user_id ORDER BY id ) as user_district");
						$response ['type'] = $temp[0]['user_is'];
						$response ['message'] = "done";
						// $response ['result'] = json_encode($temp[0]);
						$response ['result'] = $temp[0];
					}else{
						$response ['message'] = "fail";
					}
				}
		}else{
			$response ['message'] = "fail";
		}
        
		echo json_encode ( $response );
    }




    
}
?>