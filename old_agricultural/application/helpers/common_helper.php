<?php 

function getEventimages($id) {
   $CI = get_instance();
   $CI->load->model('base_models');    
   $event_img = $CI->base_models->GetAllValues('images', array('type'=> 1, 'ref_id'=>$id), 'image_url');
   return $event_img;
}

function getMeetingimages($id) {
   $CI = get_instance();
   $CI->load->model('base_models');    
   $event_img = $CI->base_models->GetAllValues('images', array('type'=> 2, 'ref_id'=>$id), 'image_url');
   return $event_img;
}

function getContactUsimages($id) {
   $CI = get_instance();
   $CI->load->model('base_models');    
   $event_img = $CI->base_models->GetAllValues('images', array('type'=> 4, 'ref_id'=>$id), 'image_url');
   return $event_img;
}

function getUserimages($id) {
   $CI = get_instance();
   $CI->load->model('base_models');    
   $where = array('type'=> 0, 'ref_id'=>$id);
   // $event_img = $CI->base_models->GetAllValues('images', array('type'=> 0, 'ref_id'=>$id), 'image_url');
   $img = $CI->base_models->CustomeQuary('Select id,image_url from images where type = 0 AND ref_id = '.$id.' order by id DESC limit 1');
   return $img;
}

function getSurveyimages($id) {
   $CI = get_instance();
   $CI->load->model('base_models');    
   $where = array('type'=> 5,'ref_id'=>$id);
   $imgs = $CI->base_models->GetAllValues('images', $where, 'image_url');
   return $imgs;
}
function getAllDistrictData() {
   $CI = get_instance();
   $CI->load->model('base_models');    
   $event_img = $CI->base_models->GetAllValues('tbl_district', array('id <>'=> 0), 'id,districtName');
   return $event_img;
}
function getAllUserDistrictData($user_id) {
   $CI = get_instance();
   $CI->load->model('base_models');    
   $event_img = $CI->base_models->GetAllValues('mpyc_districts', array('districtAdminId'=> $user_id,'languageType'=> '0'), 'districtName');
   return $event_img;
}


?>