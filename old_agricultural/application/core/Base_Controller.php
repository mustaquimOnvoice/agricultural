<?php
require_once APPPATH . 'core/class.phpmailer.php';
require_once APPPATH . 'core/class.smtp.php';

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Base_Controller extends CI_Controller {
	public $Base_Models = NULL;
	public $session_value = "";
	public $user_id = 1;
	//
	public $trending_blog_limit_in_days = 20;
	public $api_emailId = "****@***.**";
	public $api_emailPassword = "****";
	public $admission_report_form_action = "accept_form";
	public $fcm_project_url = "https://console.firebase.google.com/project/mandalapp-44e0e/settings/general/android:mandal.onevoice.com.mandalapplication";
	public $fcm_project_key = "AIzaSyA2bEgy32pLw1CV0YdVRG3qdj1AAs0FLiA";
	public $_403_access_forbidden = '<div class="col-md-12" style="font-size: 20px; padding : 10px;">403 - ACCESS FORBIDDEN!! <br>Please Contact System Administrator.</div>';
	public $data_update_mobile_log_values = array ();
	public function __construct() {
		parent::__construct ();
		// $this->load->library ( 'session' );
		$this->load->model ( "base_models" );
		$this->Base_Models = new Base_Models ();
		$this->load->helper ( 'url' );
		$this->load->helper('cookie');
		
		//
		session_start ();
		// unset ( $_SESSION ['active_tag'] );
		if (isset ( $_SESSION ['id'] )) {
			$_SESSION ['active_tag'] = '';
			$_SESSION ['active_btn'] = '';
		}
		
		/*
		 * session_start (); if (function_exists ( "date_default_timezone_set" )) { date_default_timezone_set ( 'Asia/Kolkata' ); } if (isset ( $_SESSION ['id'] )) { $this->check_tag_active (); if ($_SESSION ['role'] != "Admin" && $_SESSION ['role'] != "CEO" && $_SESSION ['role'] != "Director") { if (! isset ( $_SESSION ['daily_report_status'] )) { $this->check_if_user_filled_daily_report ( $_SESSION ['id'] ); } elseif ($_SESSION ['daily_report_status'] == "PENDING") { $this->check_if_user_filled_daily_report ( $_SESSION ['id'] ); } } }
		 */
	}
	/**
	 * compress image size
	 *
	 * @param string $source        	
	 * @param string $destination        	
	 * @param integer $quality        	
	 * @return string
	 */
	function imageCompress($source, $destination, $quality) {
		$info = getimagesize ( $source );
		$image=null;
		//strtolower($info ['mime']);
		if ($info ['mime'] == 'image/jpeg' || $info ['mime'] == 'image/jpg')
			$image = imagecreatefromjpeg ( $source );
		elseif ($info ['mime'] == 'image/gif')
			$image = imagecreatefromgif ( $source );
		elseif ($info ['mime'] == 'image/png')
			$image = imagecreatefrompng ( $source );
		if($image)
		imagejpeg ( $image, $destination, $quality );
		else
			$destination="";
		
		return $destination;
	}
	
	/**
	 *
	 * @param string/array $registration_ids
	 *        	For mutiple notification to devices pass array OR use device string for single
	 * @param string $message
	 *        	message for notification
	 * @param string $type
	 *        	default '' , options user definded
	 * @return boolean true / false
	 */
	public function pushNotification($registration_ids, $message, $admin_id, $type = "", $notification_type = "", $notification_id = null, $action_id = null) {
		/*
		 * $registrationIds = array ( $registration_ids );
		 */
		$data ['title'] = "MPYC";
		$data ['device_token'] =$registration_ids;
		$data ['body'] =$message;
		$data ['event_id'] =$notification_id;
		$data ['district_admin_id'] =$admin_id;
		$data ['type'] =$type;
		
		$temp = $this->Base_Models->AddValues ( "user_notification", $data );     
		$msg = array (
				'body' => $message,
				'title' => 'MPYC',
				'vibrate' => 1,
				'sound' => 1 
		);
		$data = array (
				'intent' => $notification_type 
		);
		
		if (isset ( $type )) {
			$data ['type'] = $notification_type;
		}
		if (isset ( $notification_id )) {
			$data ['id'] = $notification_id;
		}
		
		if (isset ( $action_id )) {
			$data ['action_id'] = $action_id;
		}
		
		$fields = array ();
		if (is_array ( $registration_ids )) { // for mutiple device ids
			$fields = array (
					'registration_ids' => $registration_ids,
					'notification' => $msg,
					'data' => $data 
			);
		} else {
			$fields = array ( // for single device id
					'to' => $registration_ids,
					'notification' => $msg,
					'data' => $data 
			);
		}
		$headers = array (
				'Authorization:key=AAAAl2bNm0o:APA91bHkHX3xBVmPBNOA2npRfC0iD19KJ7RhUbTNFHnvIfxDvmDcdk1sqWZurDH1ax4aTOHdIYUpXE49oE-FPIamcve2FiJLH3lh-w0iFLK9aSw1VveVbNIVVW4WsFeHStWdsMIFxn8F',
				'Content-Type:application/json' 
		);
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
		$result = curl_exec ( $ch );
		curl_close ( $ch );
		$data = json_decode ( $result );
		// echo $result;
		// die();
		$return = false;
		if ($data->success == 1) {
			$return = true;
		} else if ($data->failure == 1) {
			$return = false;
		}
		return $return;
	}
	
	/**
	 * Logout SESSION
	 *
	 * @return redirect to login
	 * @copyright 28-08-17
	 */
	public function logout() {
		session_destroy ();
		redirect ( base_url ( '/login' ) );
	}
	public function logout_user() {
		session_destroy ();
		redirect ( base_url ( '/home' ) );
	}
// 	public function send($email = "ajay.s.dahiwal@gmail.com", $name = "Ajay Dahiwal", $subject = "Quotation From SUBODHAN ENGINEERS (PUNE) PVT. LTD.", $format = "1", $attachment = false, $id = null, $cc = null, $bcc = null) {
// 		$email = urldecode ( $email );
// 		$subject = urldecode ( $subject );
// 		$html = "";
// 		$html .= '<body style="margin: 0; padding: 0; font-family: Arial, sans-serif; width: 820px;">
// 	<table align="center" border="0" cellpadding="0" cellspacing="0" >
// 		<tr colspan="4">
// 			<td colspan="4" style=" border:none;">
// 				<a href="#"><img style=" height:100%; width:100%;" src="http://subodhancapacitor.com/images/header.jpg"></a>
// 			</td>
// 		</tr>
// 		<tr colspan="2">
// 			<td colspan="4"  style="padding: 30px 30px; border:none; background-color:rgba(255, 224, 1, 0.7);">';
// 		if ($format != 1) {
// 			$html .= $format;
// 		}
// 		$html .= '</td>
	
// 		</tr>
	
		
// 	</table>
// </body>		';
// 		$mail = new PHPMailer ();
// 		$mail->IsSMTP (); // set mailer to use SMTP
// 		$mail->Host = "smtpout.secureserver.net"; // specify main and backup server
// 		$mail->Port = 80; // set the port to use
// 		$mail->SMTPAuth = true; // turn on SMTP authentication
// 		$mail->Username = $this->api_emailId; // your SMTP username or your gmail username
// 		$mail->Password = $this->api_emailPassword; // your SMTP password or your gmail password
// 		if (isset ( $_SESSION ['email'] )) {
// 			$from = $_SESSION ['email']; // Reply to this email
// 		} else {
// 			$from = "no-replay@healthbuddies.com"; // Reply to this email
// 		}
// 		// $from = "no-replay@subodhancapacitor.com"; // Reply to this email
// 		$to = "" . $email; // Recipients email ID
// 		$name = "" . $name; // Recipient's name
// 		$mail->From = $from;
// 		$mail->FromName = "HealthBuddies"; // Name to indicate where the email came from when the recepient received
// 		$email_arr = explode ( " /,/ ", $email );
// 		$name_arr = explode ( " /,/ ", $name );
// 		for($i = 0; $i < count ( $email_arr ); $i ++) {
// 			$mail->AddAddress ( $email_arr [$i], $name_arr [$i] );
// 		}
// 		if ($cc != null) {
// 			foreach ( $cc as $key => $val ) {
// 				if (strpos ( $val, "<,>" ) === false) {
// 					$mail->AddCC ( $val );
// 				} else {
// 					list ( $a, $b ) = explode ( "<,>", $val );
// 					$mail->AddCC ( $b, $a );
// 				}
// 			}
// 		}
// 		if ($bcc != null) {
// 			foreach ( $bcc as $key => $val ) {
// 				if (strpos ( $val, "<,>" ) === false) {
// 					$mail->AddBCC ( $val );
// 				} else {
// 					list ( $a, $b ) = explode ( "<,>", $val );
// 					$mail->AddBCC ( $b, $a );
// 				}
// 			}
// 		}
// 		$mail->AddReplyTo ( $from, "HealthBuddies" );
// 		if ($attachment == true)
// 			$mail->AddAttachment ( __DIR__ . $id );
			
// 			// $mail->WordWrap = 50; // set word wrap
// 		$mail->IsHTML ( true ); // send as HTML
// 		$mail->Subject = "" . $subject;
// 		$mail->Body = "" . $html; // HTML Body
// 		$mail->AltBody = strip_tags ( $html ); // Text Body
// 		$mail->Sender = 'healthbuddies@gmail.com';
		
// 		// $mail->AddAttachment($amol);
// 		if (! $mail->Send ()) {
// 			return "Mailer Error: " . $mail->ErrorInfo;
// 		} else {
// 			return 1;
// 			// $this->base_view ( "login/load_popup" );
// 		}
// 		// redirect(base_url('index.php/amol/quotation_view/'.$id));
// 	}
	
	public function array_column(array $input, $columnKey, $indexKey = null) {
		$array = array ();
		foreach ( $input as $value ) {
			if (! array_key_exists ( $columnKey, $value )) {
				trigger_error ( "Key \"$columnKey\" does not exist in array" );
				return false;
			}
			if (is_null ( $indexKey )) {
				$array [] = $value [$columnKey];
			} else {
				if (! array_key_exists ( $indexKey, $value )) {
					trigger_error ( "Key \"$indexKey\" does not exist in array" );
					return false;
				}
				if (! is_scalar ( $value [$indexKey] )) {
					trigger_error ( "Key \"$indexKey\" does not contain scalar value" );
					return false;
				}
				$array [$value [$indexKey]] = $value [$columnKey];
			}
		}
		return $array;
	}
	function clean_string($string) {
		$string = str_replace ( '-', ' ', $string ); // Replaces all spaces with hyphens.
		$string = preg_replace ( '/[^A-Za-z0-9\-]/', '', $string ); // Removes special chars.
		return preg_replace ( '/-+/', ' ', $string ); // Replaces multiple hyphens with single one.
	}
	public function pars_firebase_string($text) {
		return str_replace ( '.', "%", $text );
	}
	public function formatFormValues($dataArray = array()) {
		if (! empty ( $dataArray )) {
			$len = count ( $dataArray );
			$arrResponse = array ();
			for($i = 0; $i < $len; $i ++) {
				if (! empty ( $dataArray [$i] ['name'] )) {
					$arrResponse [$dataArray [$i] ['name']] = $dataArray [$i] ['value'];
				}
			}
			return $arrResponse;
		} else {
			return false;
		}
	}
	public function load_header() {
		$this->load->view ( 'common/header' );
	}
	public function load_footer() {
		$this->load->view ( 'common/footer' );
	}
	public function view($url, $data = null) {
		if (isset ( $url )) {
			$this->load_header ();
			$this->load->view ( 'common/nav' );
			$this->load->view ( 'common/top' );
			if (isset ( $data ))
				$this->load->view ( $url, $data );
			else
				$this->load->view ( $url );
			$this->load_footer ();
		}
	}
	public function base_view($url, $data = null) {
		if (isset ( $url )) {
			$this->load->view ( 'common/header' );
			if (isset ( $data ))
				$this->load->view ( $url, $data );
			else
				$this->load->view ( $url );
			$this->load->view ( 'common/footer' );
		}
	}
	public function export_script() {
		$this->load->view ( 'common/export_script' );
	}
	public function message_send($message1, $number) {
		// Your authentication key
		$authKey = "228445Am2S94aqL0L5b5aa9b7";
		
		// Multiple mobiles numbers separated by comma
		$mobileNumber = $number;
		
		// Sender ID,While using route4 sender id should be 6 characters long.
		//$senderId = "HETHBD";
		//MH-mpyc
		$senderId = "MHMPYC";
		
		// Your message to send, Add URL encoding here.
		$message = urlencode ( $message1 );
		
		// Define route
		//$route = "1";// transactional  
		$route = "4";//promotional
		// Prepare you post parameters
		$postData = array (
				'authkey' => $authKey,
				'mobiles' => $mobileNumber,
				'message' => $message,
				'sender' => $senderId,
				'route' => $route 
		);
		
		// API URL
		$url = "https://control.msg91.com/api/sendhttp.php";
		
		// init the resource
		$ch = curl_init ();
		curl_setopt_array ( $ch, array (
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $postData 
		// ,CURLOPT_FOLLOWLOCATION => true
				) );
		
		// Ignore SSL certificate verification
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		
		// get response
		$output = curl_exec ( $ch );
		
		// Print error if any
		if (curl_errno ( $ch )) {
			echo 'error:' . curl_error ( $ch );
		}
		
		curl_close ( $ch );
		
		// redirect ( 'https://control.msg91.com/api/sendhttp.php?authkey=103359At0Uz40o4Gt56b34fe9&mobiles=' . $number . '&message=this%20is%20api%20test%20sms&sender=CARING&route=4' );
	}
	function generate_random_string($length = 10, $flag = null) {
		$characters = '';
		if ($flag == "num") {
			$characters = '0123456789';
		} else {
			$characters = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		}
		// $characters = '0123456789';
		$charactersLength = strlen ( $characters );
		$randomString = '';
		for($i = 0; $i < $length; $i ++) {
			$randomString .= $characters [rand ( 0, $charactersLength - 1 )];
		}
		return $randomString;
	}
	public function getMainCategories() {
		$data = $this->Base_Models->GetAllValues ( "main_categories_health_buddies", array (
				"status" => 0 
		) );
		return $data;
	}
	// calibri (body)
	public function encryt_decrypt_code($data, $flag = false) {
		$temp = "";
		if (! $flag) {
			$temp = "get_" . base64_encode ( $data ) . "_code_data_" . random_string ();
			$data = " " . base64_encode ( $temp ) . " ";
		} else {
			$data = trim ( $data );
			$data = base64_decode ( $data );
			$data = explode ( "_", $data );
			if (isset ( $data [1] )) {
				$data = $data [1];
			} else
				$data [0];
		}
		return $data;
	}
}
