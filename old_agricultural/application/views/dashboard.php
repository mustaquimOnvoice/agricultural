<style>
section {
	padding: 30px 0;
}
</style>
<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom"><?php echo isset($title)?$title:"Home";?></h2>
		</div>
	</header>
	<!-- Dashboard Counts Section-->
	<section class="dashboard-counts no-padding-bottom">
		<div class="container-fluid">
			<div class="row bg-white has-shadow">
				<!-- Item -->
				<div class="col-xl-6 col-sm-6">
					<div class="item d-flex align-items-center">
						<div class="icon bg-violet">
							<i class="icon-user"></i>
						</div>
						<div class="title">
							<span>Mobile<br>Users
							</span>
							<div class="progress">
								<div role="progressbar" style="width: 25%; height: 4px;"
									aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"
									class="progress-bar bg-violet"></div>
							</div>
						</div>
						<div class="number">
							<?php
							echo isset ( $mobile_user_count ) ? $mobile_user_count : "";
							?>
						</div>
					</div>
				</div>
				<!-- Item -->
				<div class="col-xl-6 col-sm-6">
					<div class="item d-flex align-items-center">
						<div class="icon bg-red">
							<i class="icon-padnote"></i>
						</div>
						<div class="title">
							<span>Department<br>Count
							</span>
							<div class="progress">
								<div role="progressbar" style="width: 25%; height: 4px;"
									aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"
									class="progress-bar bg-red"></div>
							</div>
						</div>
						<div class="number">
							<?php
							//echo count($departments);
							?>
						
						</div>
					</div>
				</div>
				<!-- Item -->
				
			</div>
		</div>
	</section>
	<!-- Dashboard Header Section    -->
	<section class="dashboard-header">
		<div class="container-fluid">
			<div class="row">
				<!-- Statistics -->
				<?php if(isset($departments))foreach($departments as $row) { ?>
				<div class="statistics col-lg-4 col-12">
					<div
						class="statistic d-flex align-items-center bg-white has-shadow">
						<div class="icon bg-green">
							<i class="icon-bill"></i>
						</div>
						<div class="text">
							<strong> <?php echo isset($row['acounts'])?$row['acounts']:"0";?> Pending</strong> <br>
							<!-- <strong> <?php //echo isset($row['counts'])?($row['counts']-$row['approved']):"0";?> Pending</strong><br> -->

							<small><?= $row['department_name']?><br> (<?=$row['counts']?>)Total application </small>
							
						</div>
					</div>
				</div>
				<?php } ?>

			</div>
		</div>
	</section>
	<section class="dashboard-header">
		<div class="container-fluid">
			<div class="row">
				<!-- Line Chart            -->
				<div class="chart col-lg-12 col-12">
					<div
						class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
						<canvas id="lineCahrt"></canvas>
					</div>
				</div>
				
			</div>
		</div>
	</section>
	<!-- <section class="dashboard-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<h2>Inquiries From Customers</h2>
					<a href="./dashboard/get_inquires/data">See All Inquiries</a>
					<form class="POST_AJAX" action="./dashboard/get_inquires">
						<button style="display: none;" value="data" id="get_data1">getData</button>
					</form>
					<div
						class="bg-white d-flex align-items-center justify-content-center has-shadow"></div>
				</div>
			</div>
		</div>
	</section> -->
</div>
<script src="<?php echo base_url('components/')?>/js/Chart.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/charts-home.js"></script>
<script type="text/javascript">
document.ready=function(){
	//setTimeout(function(){ $('form.POST_AJAX button#get_data1').click();},2000);
};
function validatation(){
	return true;
}
// chart 1
var legendState = true;
if ($(window).outerWidth() < 576) {
    legendState = false;
}
var LINECHART = $('#lineCahrt');
var myLineChart = new Chart(LINECHART, {
    type: 'line',
    options: {
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                display: true,
                gridLines: {
                    display: false
                }
            }]
        },
        legend: {
            display: legendState
        }
    },
    data: {
        labels: [<?php if (isset($applications)){ foreach ($applications as $key=>$val){ echo ($key==0)?"'".$val['date']."'":",'".$val['date']."'"; } } ?>],
        datasets: [
            {
                label: "Pending Login",
                fill: true,
                lineTension: 0,
                backgroundColor: "transparent",
                borderColor: '#f15765',
                pointBorderColor: '#da4c59',
                pointHoverBackgroundColor: '#da4c59',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                borderWidth: 1,
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBorderColor: "#fff",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 0,
            	data: [<?php if (isset($pending_applications)){ foreach ($pending_applications as $key=>$val){ echo ($key==0)?"'".$val['cnt']."'":",'".$val['cnt']."'"; } } ?>],
                spanGaps: false
            },
            {
                label: "Active Login",
                fill: true,
                lineTension: 0,
                backgroundColor: "transparent",
                borderColor: "#54e69d",
                pointHoverBackgroundColor: "#44c384",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                borderWidth: 1,
                pointBorderColor: "#44c384",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBorderColor: "#fff",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
				data: [<?php if (isset($applications)){ foreach ($applications as $key=>$val){ echo ($key==0)?"'".$val['cnt']."'":",'".$val['cnt']."'"; } } ?>],
                spanGaps: false
            }
        ]
    }
});
		
		
</script>
