<div class="page login-page" style="text-align: justify;">
	<div class="container d-flex align-items-center">
		<div class="form-holder has-shadow">
			<div class="row">
				<!-- Logo & Information Panel-->
				<!-- 				<div class="col-lg-6">
					<div class="info d-flex align-items-center">
						<div class="content">
							<div class="logo">
								<h1>HEALTH BUDDIES</h1>
							</div>
							<p>Let the stories open here.</p>
						</div>
					</div>
				</div>
 -->
				<!-- Form Panel    -->
				<div class="col-lg-12 bg-white">
					<div class="form d-flex align-items-center">
						<div class="content">
							<p style="text-align: center;">
								<strong>PRIVACY POLICY</strong>
							</p>
							<p>CuroMe, the creator of this Privacy Policy ensures its
								commitment to Your privacy with regard to the protection of your
								information. This privacy policy contains information about
								CuroMe. In order to provide You with uninterrupted use of
								our services, We may collect and, in some circumstances,
								disclose information about you. Such information may be
								classified as personal information under the purview
								of&nbsp;Regulation 4 of the&nbsp;Information Technology
								(Reasonable security practices and procedures and sensitive
								personal data or information) Rules, 2011.</p>
							<p>
								If You have any queries or concerns regarding this privacy
								policy, You should <a href="www.curome.in/contact">www.curome.in/contact</a>.
							</p>
							<p>ANY CAPITALISED WORDS USED HENCEFORTH SHALL HAVE THE MEANING
								ACCORDED TO THEM UNDER THIS AGREEMENT. FURTHER, ALL HEADING USED
								HEREIN ARE ONLY FOR THE PURPOSE OF ARRANGING THE VARIOUS
								PROVISIONS OF THE AGREEMENT IN ANY MANNER. NEITHER THE USER NOR
								THE CREATERS OF THIS PRIVACY POLICY MAY USE THE HEADING TO
								INTERPRET THE PROVISIONS CONTAINED WITHIN IT IN ANY MANNER. THIS
								PRIVACY POLICY SHALL BE COTERMINOUS TO THE PRIVACY POLICY
								GOVERNING THE USE OF&nbsp;CuroMe WEBSITE OR APP.</p>
							<ol start="1">
								<li><strong>Definitions</strong></li>
							</ol>
							<ol type="a" start="1">
								<li>&ldquo;We&rdquo;, &ldquo;Our&rdquo;, and &ldquo;Us&rdquo;
									shall mean and refer to the creators of this privacy policy.</li>
								<li>&ldquo;You&rdquo;, &ldquo;Your&rdquo;,
									&ldquo;Yourself&rdquo; and &ldquo;User&rdquo; shall mean and
									refer to natural and legal individuals who use the Application,
									including End-Users, Patients, general Users and Medical
									expert.</li>
								<li>&ldquo;Application&rdquo; shall mean and refer to
									CuroMe.</li>
								<li>&ldquo;Personal Information&rdquo; shall mean and refer to
									any personally identifiable information that We may collect
									from You.</li>
								<li>&ldquo;Third Parties&rdquo; refer to any application,
									website, company or individual apart from the User and the
									creator of the Application.</li>
							</ol>
							<ol start="2">
								<li><strong>Overview</strong></li>
							</ol>
							<p>In order to use the services of this Application, You are
								required to register Yourself by verifying the authorised
								device.&nbsp;This Privacy Policy applies to your information
								that we collect and receive on and through CuroMe; it
								does not apply to practices of businesses that we do not own or
								control or people we do not employ.</p>
							<p>By using this Application, you agree to the terms of this
								Privacy Policy. Please read the following Privacy Policy to
								understand how your personal information will be treated as you
								use this Site and its services. The following discloses our
								information gathering and dissemination practices.</p>
							<ol start="3">
								<li><strong>Information Collected</strong></li>
							</ol>
							<p>We may collect the following information:</p>
							<ol type="a" start="1">
								<li>Personal data of the User such as, but not limited to, Your
									name, Your age, date of birth, gender, health issue.</li>
								<li>The User&rsquo;s e-mail and contact information, GPS based
									location;</li>
								<li>The User&rsquo;s tracking Information such as, but not
									limited to the device ID, Google Advertising ID and Android ID;</li>
								<li>The User&rsquo;s data sent across through the Application.</li>
							</ol>
							<p>As a User of CuroMe, you may provide information about
								yourself, your spouse or family, your friends, their health
								issues, gender of the patient, age of the patient, previous
								medication taken, previous medical conditions, allergies, etc.</p>
							<p>The Information specified above and collected by us may be
								classified as &lsquo;Personal Information&rsquo; or
								&lsquo;Sensitive Information&rsquo; under the&nbsp;Information
								Technology (Reasonable security practices and procedures and
								sensitive personal data or information) Rules, 2011. Collection
								of information which has been designated as &lsquo;sensitive
								personal data or information&rsquo; under the&nbsp;Information
								Technology (Reasonable security practices and procedures and
								sensitive personal data or information)&nbsp;Rules requires your
								express consent. By affirming your assent to this Privacy
								Policy, you provide your consent to such collection as required
								under applicable law. Our Services may be unavailable to you in
								the event such consent is not given before sign up.</p>
							<p>For Medical expert using CuroMe, we would collect your
								Name, Education, Specialization, address of practicing
								hospitals/clinics, Contact information, achievements, Device ID
								(IMEI), Location, etc.</p>
							<ol start="4">
								<li><strong>External Links on The Application</strong></li>
							</ol>
							<p>The Application may include offers from our vendors,
								advertisements, hyperlinks to other websites, applications,
								content or resources. We have no control over such external
								links present in the Application, which are provided by persons
								or companies other than Us.</p>
							<p>You acknowledge and agree that We are not responsible for any
								collection or disclosure of your Personal Information by any
								external sites, applications, companies or persons, nor are do
								We endorse any advertising, products or other material on or
								available from such external application, websites or resources.</p>
							<p>You further acknowledge and agree that We are not liable for
								any loss or damage which may be incurred by You as a result of
								the collection and/or disclosure of Your personal information by
								external applications, sites or resources, or as a result of any
								reliance placed by You on the completeness, accuracy or
								existence of any advertising, products or other materials on, or
								available from such applications, websites or resources. These
								external applications, websites and resource providers may have
								their own privacy policies governing the collection, storage,
								retention and disclosure of Your Personal Information that You
								may be subject to. We recommend that You enter the external
								application or website and review their privacy policy.</p>
							<p>We may allow third parties/individuals to display
								advertisements when you use the Application.</p>
							<ol start="5">
								<li><strong>Our Use of Your Information:</strong></li>
							</ol>
							<p>The information provided by You shall be used to contact You
								when necessary. For more details about the nature of such
								communications, please refer to out Terms of Service. We use
								Your tracking information to help identify you and to gather
								broad demographic information. The information is also used to
								customise your experience of using CuroMe.</p>
							<p>We may release your Personal Information to a third-party in
								order comply with a Court Order or other similar legal
								procedure, or when we believe in good faith that such disclosure
								is necessary to comply with the law; prevent imminent physical
								harm or financial loss; or investigate or take action regarding
								illegal activities, suspected fraud, or violations of Our Terms
								of Use. The information made available by user is displayed to
								Medical expert and patients. The information collected from
								Medical expert is displayed on the Application to enable a User
								to connect with a Medical expert of their choice.</p>
							<ol start="6">
								<li><strong>Disclosure of Your Information to Third Parties:</strong></li>
							</ol>
							<p>When you use our Application, we must provide some of your
								personal information to third parties to give you better
								services and for enhancement and visibility of our Application.
								However, we do not sell or rent individual customer names or
								other Personal Information to third parties except sharing of
								such information with our alliance partners or vendors who are
								engaged by us for providing various promotional and other
								benefits to our customers from time to time.</p>
							<p>Due to the existing regulatory environment, we cannot ensure
								that all of your information shall never be disclosed in ways
								other than those described in this Privacy Policy. For example,
								but without limiting and foregoing, we may be forced to disclose
								Your Personal Information to the government, law enforcement
								agencies or other Third Parties. Under certain circumstances,
								Third Parties may unlawfully intercept or access transmission or
								private communications, or abuse or misuse Your Personal
								Information that they may collect from our Application.
								Therefore, we do not promise, and you should not expect, that
								your personally identifiable information or private
								communications would always remain private. We may share your
								personal information to those service providers and Medical
								expert with which you make arrangements through our app. We
								provide them with the Personal Information needed to make and
								answer the query you pose.</p>
							<ol>
								<ol start="1">
									<li>We cooperate with law enforcement inquiries, as well as
										other third parties to enforce laws, such as: intellectual
										property rights, fraud and other rights. We can, and You so
										authorise Us, disclose Your Personal Information to law
										enforcement and other government officials as We, in Our sole
										discretion, believe necessary or appropriate, in connection
										with an investigation of fraud, intellectual property
										infringements, or other activity that is illegal or may expose
										Us/ Us or You to any legal liability.</li>
									<li>We gather up data such as personally identifiable
										information and disclose such information in a non-personally
										identifiable manner to advertisers and other third parties for
										other marketing and promotional purposes. However, in these
										situations, we do not disclose to these entities any
										information that could be used to identify you personally. We
										may use third-party advertising companies to serve
										advertisement on our behalf. These companies may employ
										cookies and action tags (also known as single pixel gifs or
										web beacons) to measure advertising effectiveness. Any
										information that these third parties collect via cookies and
										action tags is completely anonymous</li>
								</ol>
							</ol>
							<ol start="7">
								<li><strong>Application Terms</strong></li>
							</ol>
							<p>
								By downloading and / or using this Application, you hereby agree
								to be bound by the Terms of Use / Service, Privacy Policy, and
								other Policies as set forth on the website <a
									href="http://www.curome.in">www.curome.in</a>
								. It is hereby further specified that all the Terms and Policies
								on the Website <a href="http://www.curome.in">www.curome.in</a>
								shall be coterminous to these terms.
							</p>
							<p>&nbsp;</p>
							<p>&nbsp;</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyrights text-center">
		<p>
			Neloc Solutions Pvt. Ltd. &copy; 2017 | Design &amp; Developed By <a
							href="http://www.unitglo.com/web" target="_blank"><b> Universal IT Global Solutions Pvt. Ltd. </b></a>
		</p>
	</div>
</div>
<script type="text/javascript">
$(function(){
	show_message();
});
</script>
<script src="<?php echo base_url('components/')?>/js/tether.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/jquery.cookie.js"> </script>
<script
	src="<?php echo base_url('components/')?>/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/front.js"></script>
