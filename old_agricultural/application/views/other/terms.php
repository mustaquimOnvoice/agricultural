<style>
@media only screen and (max-width: 500px) {
	.ol {
		margin-left: -90px;
	}
}
</style>
<div class="page login-page" style="text-align: justify;">
	<div class="container d-flex align-items-center">
		<div class="form-holder has-shadow">
			<div class="row">
				<!-- Logo & Information Panel-->
				<!-- 				<div class="col-lg-6">
					<div class="info d-flex align-items-center">
						<div class="content">
							<div class="logo">
								<h1>HEALTH BUDDIES</h1>
							</div>
							<p>Let the stories open here.</p>
						</div>
					</div>
				</div>
 -->
				<!-- Form Panel    -->
				<div class="col-lg-12 bg-white">
					<div class="form d-flex align-items-center">
						<div class="content">
							<p style="text-align: center;">
								<strong>Terms of Service and Privacy Policy - CuroMe</strong>
							</p>
							<p style="text-align: center;">
								<strong>Terms of Service</strong>
							</p>
							<ol start="1">
								<li><strong>General</strong></li>
							</ol>
							<p>The mobile application CuroMe (hereinafter referred to
								as &ldquo;Application&rdquo; or &ldquo;App&rdquo;),&nbsp;is
								owned by &nbsp;Neloc. </p>
							<ol type="a" start="1">
								<li>For the purpose of these Terms of Use, along with any
									amendments to the same, and wherever the context so requires <strong>&nbsp;&ldquo;You&rdquo;</strong>&nbsp;
									or &nbsp;<strong>&ldquo;User&rdquo;</strong> &nbsp;shall mean
									any natural or legal person who has agreed to become a user of
									the Application by installing the Application. The term&nbsp; <strong>&ldquo;We&rdquo;</strong>,
									<strong>&nbsp; &ldquo;Us&rdquo;</strong>, <strong>&nbsp;&ldquo;Our&rdquo;</strong>&nbsp;
									shall mean&nbsp;CuroMe. <strong>&nbsp;&ldquo;Agreement&rdquo;</strong>
									&nbsp;shall mean and refer to this Terms of Service, including
									any amendments that may be incorporated into it and the Terms
									of Service, and other Policies available on our
									app/website.&nbsp;<strong>&ldquo;Third Party&rdquo;&nbsp;</strong>shall
									mean and refer to any individual(s), company or entity apart
									from the User and CuroMe.
								</li>
							</ol>
							<ol type="a" start="2">
								<li>By using the App, You accept and agree to be bound by this
									Agreement, the&nbsp;Privacy Policy, as well as&nbsp;rules,
									guidelines, policies, terms, and conditions applicable to any
									service that is provided by this Application.&nbsp;Your use of
									Our Application is evidence that You have read and agreed to be
									contractually bound by these Terms of Service and our Privacy
									Policy. Please read the terms carefully. The use of this
									Application by You is governed by this policy and any policy so
									mentioned by terms of reference.</li>
							</ol>
							<p>If you do not agree with any of these terms, please
								discontinue using the Application.</p>
							<ol type="a" start="3">
								<li>We hold the sole right to modify the Terms of Service
									without prior permission from You or providing notice to You.
									The relationship creates on You a duty to periodically check
									the Terms of Service and stay updates on its requirements. If
									You continue to use the Application or avail of its services
									following such change, it is deemed as consent by You to the so
									amended policies.&nbsp;Your continued use of the Application is
									conditioned upon your compliance /with the Terms of Service,
									including but not limited to compliance with the Terms of
									Service even after alterations, if any.</li>
							</ol>
							<ol start="2">
								<li><strong>Restrictions on Use</strong></li>
							</ol>
							<p>To fully avail the services of the Application and use it, you
								must download the app from the &lsquo;Google Play store
								APP&rsquo;, and verify your phone number.&nbsp;Without
								limitation to the foregoing, in the event you are barred from
								undertaking legally binding obligations under the Indian
								Contract Act, 1872, or are for any reason, unable to provide
								&lsquo;Consent&rsquo; as per the&nbsp;Information Technology
								(Reasonable security practices and procedures and sensitive
								personal data or information) Rules, 2011, you are not eligible
								to&nbsp;register for, use or avail the services available on the
								Application.</p>
							<p>Without limiting any other provisions of these Terms, you may
								not use this Application for any purpose that is unlawful or
								prohibited by these Terms and/or any applicable additional
								terms. Your access of this Application may be terminated
								immediately, in our sole discretion, with or without notice, if
								you fail to comply with any provision of these Terms and/or
								additional terms, or for any other reason, or no reason.</p>
							<ol start="3">
								<li><strong>Intermediary Application</strong></li>
							</ol>
							<p>The CuroMe is a platform that Patients and Medical
								expert utilize to meet and interact with one another. We act as
								aggregators who display to the End User, Patients or Medical
								expert best suited to your needs, based on the Content provided
								to us / updated on the Mobile Application.</p>
							<ol start="4">
								<li><strong>Medical expert Display</strong></li>
							</ol>
							<p>CuroMe collects, directly or indirectly, and displays
								on the Application, relevant information regarding the profile
								and practice of the Medical expert listed on the Application,
								such as their specialization, qualification area of practice,
								location, and similar details. Although we take reasonable
								efforts to ensure that such information is accurate and updated
								at frequent intervals, we cannot be held liable for any
								inaccuracies or incompleteness represented on the Application
								pertaining to Medical expert Content.</p>
							<p>It is hereby expressly clarified that, the Information that
								you obtain or receive from CuroMe, is for informational
								and User&rsquo;s personal purposes only. We make no guarantees,
								representations or warranties, whether expressed or implied,
								with respect to professional qualifications, quality of work,
								expertise or other information pertaining to the Medical expert
								provided on the app.</p>
							<p>In no event shall we be liable to you or anyone else for any
								decision made or action taken by you in reliance on such
								information. CuroMe is not meant to be a substitute for
								Emergency Medical Care.</p>
							<ol start="5">
								<li><strong>Disclaimers</strong></li>
							</ol>
							<ol type="i" start="1">
								<li>CuroMe does not support any Medical expert displayed
									/ made available through the Application.</li>
							</ol>
							<ol type="i" start="2">
								<li>CuroMe shall have the right to edit or remove the
									Content (stories/queries/info) in such manner as it may deem at
									any time.</li>
							</ol>
							<ol type="i" start="3">
								<li>CuroMe assumes no liability for the repercussions of
									using the application for the purpose of transmitting
									User&rsquo;s medical / personal data to any other User,
									including without limitation, loss of data, failure to boot, or
									other errors in the working of the device on to which the
									application has been downloaded.</li>
								<li>CuroMe is an application used by Medical expert to
									promote their Services. By using CuroMe, the User
									understands and accepts that CuroMe does not endorse any
									Medical expert&rsquo;s services.</li>
								<li>Any disputes arising out of the use of CuroMe shall
									be governed by the law, rules and regulations of India, as
									applicable in the State of Maharashtra. The exclusive
									jurisdiction and venue for actions and disputes shall be the
									courts located in Pune.</li>
							</ol>
							<ol start="6">
								<li><strong>User&rsquo;s Obligations</strong></li>
							</ol>
							<p>The User undertakes to fulfil the following obligations.
								Failure to satisfy any of these obligations gives Us the right
								to permanently suspend Your account and/or claim damages for any
								losses that accrue to Us or additional costs that may be imposed
								on us.</p>
							<ol>
								<ol>
									<li>You hereby certify that you are at least 18 years of age.</li>
									<li>You agree to comply with all local laws and regulations
										governing the downloading, installation and/or use of the
										Application, including, without limitation, any usage rules
										set forth in this Agreement.</li>
									<li>You undertake not to:</li>
								</ol>
							</ol>
							<ol type="a" start="1">
								<li>Cut, copy, distribute, modify, recreate, reverse engineer,
									distribute, post, publish or create derivative works from,
									transfer, or sell any information or software obtained from the
									Application. For the removal of doubt, it is clarified that
									unlimited or wholesale reproduction, copying of the content for
									commercial or non-commercial purposes and unwarranted
									modification of data and information within the content of the
									Application is not permitted. Should You want to engage in one
									or more such actions, prior permission from Us must be
									obtained.</li>
								<li>Access (or attempt to access) the Application and/or the
									materials or Services by any means other than through the
									interface that is provided by the Application. The use of
									deep-link, robot, spider or other automatic device, program,
									algorithm or methodology, or any similar or equivalent manual
									process, to access, acquire, copy or monitor any portion of the
									Application or Content, or in any way reproduce or circumvent
									the navigational structure or presentation of the Application,
									materials or any Content, to obtain or attempt to obtain any
									materials, documents or information through any means not
									specifically made available through the Application is
									prohibited. You acknowledge and agree that by accessing or
									using the Application or Services, You may be exposed to
									content from other Users or Third Parties that You may consider
									offensive, indecent or otherwise objectionable. We disclaim all
									liabilities arising in relation to such offensive content on
									the Application. Further, You may report such offensive
									content.</li>
								<li>Use the Application in any manner that may impair,
									overburden, damage, disable or otherwise compromise
									CuroMe services.</li>
								<li>Use the User information made available through
									CuroMe to further any unlawful purposes or to conduct
									any unlawful activity, including, but not limited to, fraud,
									embezzlement, and money laundering or identity theft.</li>
								<li>use the User information made available through
									CuroMe to&nbsp;abuse, harass, threaten, defame,
									disillusion, erode, abrogate, demean or otherwise violate the
									legal rights of others;</li>
								<li>engage in any activity that interferes with or disrupts
									access to the Application or the Services (or the servers and
									networks which are connected to the Application);</li>
								<li>probe, scan or test the vulnerability of the Application or
									any network connected to the Application, nor breach the
									security or authentication measures on the Application or any
									network connected to the Application. You may not reverse
									look-up, trace or seek to trace any information on any other
									user, of or visitor to, the Application, or exploit the
									Application or Service or information made available or offered
									by or through the Application, in any way whether or not the
									purpose is to reveal any information, including but not limited
									to personal identification information, other than Your own
									information, as provided for by the Application;</li>
								<li>disrupt or interfere with the security of, or otherwise
									cause harm to, the Application, systems resources, servers or
									networks connected to or accessible through the Applications or
									any affiliated or linked applications;</li>
								<li>violate any applicable laws or regulations for the time
									being in force within or outside India;</li>
								<li>violate any code of conduct or other guidelines, which may
									be applicable for or to any particular Service;</li>
							</ol>
							<ol start="7">
								<li><strong>Updates</strong></li>
							</ol>
							<p>From time to time, the Application may automatically check the
								version of the Application installed on the Device and, if
								applicable, provide updates for the Application (hereinafter
								referred to as &ldquo;Updates&rdquo;). Updates may contain,
								without limitation, bug fixes, patches, enhanced functionality,
								plug-ins and new versions of the Application. By installing the
								Application, You authorize the automatic download and
								installation of Updates and agree to download and install
								Updates manually if necessary. Your use of the Application and
								Updates shall be governed by this Agreement (as amended by any
								terms and conditions that may be provided with Updates).</p>
							<ol start="8">
								<li><strong>Actions Undertaken on Your Device:</strong></li>
							</ol>
							<p>Upon download and installation of the Application, You grant
								the following permissions to the Applications to perform the
								following actions on the device You have installed the
								Application in.</p>
							<ol type="a" start="1">
								<li>To read from, write on, modify and delete data pertaining to
									the Application on the device&rsquo;s hard disk and/or external
									storage;</li>
								<li>To access information about networks, access networks
									including wifi networks, receive and send data through the
									network;</li>
								<li>To determine Your approximate location from sources like,
									but not limited to mobile towers and connected Wi-Fi networks;</li>
								<li>To determine Your exact location from sources such as, but
									not limited to GPS.</li>
								<li>To access the model number, IMEI number and details about
									the operating system of the device the Application has been
									installed on, as well as the phone number of the device.</li>
								<li>To retrieve information about other applications running on
									the device the Application has been installed on and open them.</li>
								<li>To detect when the phone had been switched off and switched
									on for the purpose of sending notification/ push notifications.</li>
								<li>To access and change the display and sound settings of the
									device the Application has been installed in.</li>
							</ol>
							<ol start="9">
								<li><strong>Copyright</strong></li>
							</ol>
							<ol>
								<ol type="a" start="1">
									<li>All information, content, services and software displayed
										on, transmitted through, or used in connection with the
										Application, including for example text, photographs, images,
										illustrations, video, html, source and object code,
										trademarks, logos, and the like (collectively and hereinafter
										referred to as the &ldquo;App Content&rdquo;), as well as its
										selection and arrangement, is owned by Us other Third Parties.
										You may use the App Content only through the Application, and
										solely for your personal, non-commercial use.</li>
									<li>You may not, republish any portion of the App Content on
										any Internet, Intranet or extranet site or incorporate the App
										Content in any database, compilation, archive or cache. You
										may not distribute any App Content to others, whether or not
										for payment or other consideration, and you may
										not&nbsp;modify,&nbsp;copy, frame, cache, reproduce, sell,
										publish, transmit, display or otherwise use any portion of the
										App Content. You may not scrape or otherwise copy the App
										Content without permission. You agree not to decompile,
										reverse engineer or disassemble any software or other products
										or processes accessible through the Application; not to insert
										any code or product or manipulate the content of&nbsp;the
										Application&nbsp;in any way that affects the user's
										experience, and not to use any data mining, data gathering or
										extraction method.</li>
								</ol>
							</ol>
							<ol start="10">
								<li>CONTACT INFORMATION</li>
							</ol>
							<p>
								If a User has any questions concerning CuroMe, this
								Agreement, the Services, or anything related to any of the
								foregoing, CuroMe customer support can be reached via the
								contact information available on the website <a
									href="http://www.curome.in/contact">www.curome.in/contact</a>.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	show_message();
});
</script>
<script src="<?php echo base_url('components/')?>/js/tether.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/jquery.cookie.js"> </script>
<script
	src="<?php echo base_url('components/')?>/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/front.js"></script>
