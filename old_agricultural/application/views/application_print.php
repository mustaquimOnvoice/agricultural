<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<center>* अहमदनगर महानगरपालिका *<br> गणेशोत्सव मंडप परवाना

<br><br><br>
<table >
    <tr><td>परवाना क्रमांक </td><td>जा. क. <?=$application_id?></td></tr>
    <tr><td>परवाना कालावधी </td><td><?=$start_date?> ते <?=$end_date?></td></tr>
    <tr><td>मंडपाचे मोजमाप </td><td><?=$depth?> X <?=$width?>  फूट </td></tr>
    <tr><td>मंडपाचे ठिकाण </td><td><?=$area?></td></tr>
    <tr><td>गणेश मंडळाचे नाव  </td><td><?=$mandal_name?></td></tr>
    <tr><td>मंडळाचे अध्यक्ष  </td><td><?=$mandal_adheksh?></td></tr>
    <tr><td>मोबाईल क्रमांक </td><td><?=$user_mobile_no?></td></tr>
</table>
</center>