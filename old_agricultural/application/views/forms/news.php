<div class="content-inner">
	<!-- Page Header-->
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom"><?php echo isset($sub_title)?$sub_title:(isset($title)?$title:"Title");?></h2>
		</div>
	</header>
	<ul class="breadcrumb">
		<div class="container-fluid">
			<li class="breadcrumb-item"><a
				href="<?php echo base_url('/dashboard')?>">Home</a></li>
				<?php if(isset($title)){ ?>
			<li class="breadcrumb-item active"><?php echo isset($title)?$title:"Title";?></li>
		<?php } ?>
			<?php if (isset($sub_title)){?>
			<li class="breadcrumb-item active"><?php echo isset($sub_title)?$sub_title:"Title";?></li>
			<?php }?>
		</div>
	</ul>
	<section class="form">
		<div class="container-fluid">
			<div class="row">
				<!-- Basic Form-->
				<div class="col-lg-12">
					<div class="card">
						<script type="text/javascript">
						//Some code from - http://www.bennadel.com/blog/2564-using-multiple-dropzones-and-file-inputs-with-a-single-plupload-instance.htm 
						</script>
						<div class="container" style="padding: 25px;">
							<div class='edit-container'>
								<form class="POST_AJAX"
									action="<?php echo isset($action)?$action:""?>" method="post">
									
									
									<div class="form-group">
										<label class="form-label"> Title Of News </label> <input
											class="form-control" required="required" type="text"
											id="title" name="title"
											value="<?php echo (isset($title))?$title:""; ?>" />
									</div>
									<div class="form-group">
										<label class="form-label"> News Description </label> <input
											class="form-control" required="required" type="text"
											id="title" name="description"
											value="<?php echo (isset($description))?$description:""; ?>" />
									</div>
	

								
									<div class="form-group">
										<label for="blog_img">News Image</label> <input type="file"
											id="test" img-id="blog_img" name="image"
											onchange="readURL1hhh(this)" accept="image/*"> 
											<input
											name="image_path" type="hidden"
											value="<?php echo (isset($image))?$image:""; ?>"
											> <img alt="your image"
											src="<?php echo (isset($image))?base_url($image) :base_url("components/img/unitglo-mpyc-logo.png"); ?>"
											style="height: 200px; width: 200px;" id="blog_img">
									</div>									
									
									<button value="save_html_data" ajax_events=true
										class="btn btn-outline-primary">SAVE</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">

// $('select[name=categories]:input').on("change", function (e) {
// 	$('input[name=categories_id]:input').val(""+$('select[name=categories]').val().join(','));
// });
// $('select[name=categories]:input').val($('input[name=categories_id]:input').val().split(','));
function validatation(){
	return true;
}
</script>