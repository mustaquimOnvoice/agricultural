<section class="form">
	<div class="container-fluid">
		<div class="row">
			<!-- Basic Form-->
			<div class="col-lg-12">
				<div class="card">
					<div class="container" style="padding: 25px;">
						<h2><?php echo $action_title;?></h2>
					</div>
				
					<div class="container" style="padding: 25px;">
						<div class='edit-container'>
							<form id="data-form" action="" method="post">
							
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> First name :</label> 
									<input class="form-control"  type="text" id="fname" name="fname" value="<?php echo @$user_id == null ? "" : "$fname"?>" placeholder="First Name" required="" />
								</div>
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> Last name :</label> 
									<input class="form-control"  type="text" id="lname" name="lname" value="<?php echo @$user_id == null ? "" : "$lname"?>" placeholder="Last Name" required="" />
								</div>
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> Address :</label> 
									<input class="form-control"  type="text" id="user_address" name="user_address" value="<?php echo @$user_id == null ? "" : "$user_address"?>" placeholder="Address" required="" />
								</div>
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> District :</label> 
									<select id="user_district" data-url="../get_assembly_byDist" name="user_district" class="form-control select2" required="" >
										<?php foreach($districts as $district){ ?>
											<option value="<?php echo $district['districtName'];?>" <?php if ($user_district==$district['districtName']) { ?>selected="selected"<?php } ?>><?php echo $district['districtName'];?></option>
										<?php }?>										
									</select>
								</div>
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> Assembly :</label> 
									<select id="assembly" name="assembly" class="form-control select2" required="" >
										<option value="<?php echo $assembly;?>" ><?php echo $assembly;?></option>
									</select>
								</div>
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> Mobile number :</label> 
									<input class="form-control"  type="text" id="user_mobile_no" name="user_mobile_no" value="<?php echo @$user_id == null ? "" : "$user_mobile_no"?>" placeholder="Mobile number" required="" />
								</div>
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> Designation :</label> 
									<select id="designation" name="designation" class="form-control" required="" >
										<option value="State" <?php if ($designation=='State') { ?>selected="selected"<?php } ?>>State</option>
										<option value="District" <?php if ($designation=='District') { ?>selected="selected"<?php } ?>>District</option>
										<option value="Legislative Assembly" <?php if ($designation=='Legislative Assembly') { ?>selected="selected"<?php } ?>>Legislative Assembly</option>
									</select>
								</div>
								
								<div class="form-group">
									<label class="form-label"><span class="text-danger">*</span> Role :</label> 
									<select id="user_role" name="user_role" class="form-control" required="" >
										<option value="President" <?php if ($user_role=='President') { ?>selected="selected"<?php } ?>>President</option>
										<option value="Vice President" <?php if ($user_role=='Vice President') { ?>selected="selected"<?php } ?>>Vice President</option>
										<option value="Secretary" <?php if ($user_role=='Secretary') { ?>selected="selected"<?php } ?>>Secretary</option>
										<option value="General Secretary" <?php if ($user_role=='General Secretary') { ?>selected="selected"<?php } ?>>General Secretary</option>
									</select>
								</div>
								
								<div class="form-group">
									<label class="form-label"> Email:</label> 
									<input class="form-control"  type="text" id="email" name="email" value="<?php echo @$user_id == null ? "" : "$email"?>" placeholder="Email"/>
								</div>
								
								
								<div class="form-group">
									<label class="form-label"> Facebook Link :</label> 
									<input class="form-control"  type="text" id="facebook" name="facebook" value="<?php echo @$user_id == null ? "" : "$facebook"?>" placeholder="Facebook Link"/>
								</div>
								
								<div class="form-group">
									<label class="form-label"> Twitter Link :</label> 
									<input class="form-control"  type="text" id="twitter" name="twitter" value="<?php echo @$user_id == null ? "" : "$twitter"?>" placeholder="Twitter Link"/>
								</div>
								<!--<div class="form-group">
									<label class="form-label"> Age :</label> 
									<input class="form-control"  type="date" id="age" name="age" value="<?php// echo @$user_id == null ? "" : date('mm/dd/yy',strtotime("$age"))?>" placeholder="Age"/>
								</div>-->
								
								
								<button id="data-form-btn" data-url="<?php echo $action;?>" class="btn btn-outline-success">SAVE</button>
								<a href="<?php echo $cancle;?>" class="btn btn-outline-primary">Back</a>
								<div id="login-btn-loding"></div>
							</form>
							
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>