
<?php if(isset($_SESSION ['role'])){ ?>
<div class="page-content d-flex align-items-stretch">

	<!-- Side Navbar -->
	<nav class="side-navbar">
		<!-- Sidebar Header-->
		<div class="sidebar-header d-flex align-items-center">
			<div class="avatar">
				<img
					src="<?php echo isset($_SESSION['pic'])&&$_SESSION['pic']!='0'?base_url(''.$_SESSION['pic']):'';?>"
					onerror="this.src='<?php echo base_url('/components/img/unitglo-mpyc-logo.png')?>'"
					alt="..." class="img-fluid rounded-circle">
			</div>
			<div class="title">
				<h1 class="h4"><?php echo isset($_SESSION['name'])?$_SESSION['name']:"ADMIN";?></h1>
				<p><?php echo $_SESSION ['role'];?></p>
			</div>
		</div>
		<!-- Sidebar Navidation Menus-->
		<span class="heading">Main</span>
		<ul class="list-unstyled navbar-left-side">

			<li
				class="<?php echo (isset($_SESSION['active_tag']) && $_SESSION['active_tag']=="Home")?"active":""?> "><a
				href="<?php echo base_url('dashboard');?>"><i class="fa fa-home"></i>Home</a></li>
				<li
				class="<?php echo (isset($_SESSION['active_tag']) && $_SESSION['active_tag']=="Events")?"active":""?> "><a
				href="<?php echo base_url('events');?>"><i class="fa fa-calendar"></i>Events</a></li>
				<li
				class="<?php echo (isset($_SESSION['active_tag']) && $_SESSION['active_tag']=="Meetings")?"active":""?> "><a
				href="<?php echo base_url('meetings');?>"><i class="fa fa-comments-o"></i>Meetings</a></li>
				<li
				class="<?php echo (isset($_SESSION['active_tag']) && $_SESSION['active_tag']=="news")?"active":""?> "><a
				href="<?php echo base_url().'news/';?>"><i class="fa fa-newspaper-o"></i>News</a></li>
				<li
				class="<?php echo (isset($_SESSION['active_tag']) && $_SESSION['active_tag']=="contact_us")?"active":""?> "><a
				href="<?php echo base_url('contact_us');?>"><i class="fa fa-envelope-o"></i>Contact us</a></li>
				<li
				class="<?php echo (isset($_SESSION['active_tag']) && $_SESSION['active_tag']=="temp_user")?"active":""?> "><a
				href="<?php echo base_url('users/temp_user');?>"><i class="fa fa-user-plus"></i>Add Users</a></li>
				<li
				class="<?php echo (isset($_SESSION['active_tag']) && $_SESSION['active_tag']=="users")?"active":""?> "><a
				href="<?php echo base_url('users');?>"><i class="fa fa-user"></i>users</a></li>
				<li
				class="<?php echo (isset($_SESSION['active_tag']) && $_SESSION['active_tag']=="message")?"active":""?> "><a
				href="<?php echo base_url('message');?>"><i class="fa fa-envelope-o"></i>message</a></li>
				<li
				class="<?php echo (isset($_SESSION['active_tag']) && $_SESSION['active_tag']=="survey")?"active":""?> "><a
				href="<?php echo base_url('survey');?>"><i class="fa fa-square"></i>Survey</a></li>
				
		</ul>
		<span class="heading">General</span>
		<ul class="list-unstyled navbar-left-side">			
		<?php if ($_SESSION['role']=="Admin"){?>
		<li style="display: none;">
				class="<?php echo (isset($_SESSION['active_tag']) && $_SESSION['active_tag']=="Departments")?"active":""?> Profile"><a
				href="<?php echo base_url('/department/district');?>"><i class="fa fa-user-o"></i>
				District</a></li>
			<li
				class="<?php echo (isset($_SESSION['active_tag']) && $_SESSION['active_tag']=="Departments")?"active":""?> Profile"><a
				href="<?php echo base_url('/department/district_head');?>"><i class="fa fa-user-o"></i>
				District head</a></li>

			<li style="display: none;"
				class="<?php echo (isset($_SESSION['active_tag']) && $_SESSION['active_tag']=="Departments")?"active":""?> Profile"><a
				href="<?php echo base_url('/department/city_head');?>"><i class="fa fa-user-o"></i>
				City head</a></li>
				<li
				class="<?php echo (isset($_SESSION['active_tag']) && $_SESSION['active_tag']=="Departments Users")?"active":""?> Profile"><a
				href="<?php echo base_url('/users');?>"><i class="fa fa-user-o"></i>
				Local workers login</a></li>
			<?php }?>
		</ul>
	</nav>
	<?php ?>
	<script type="text/javascript">
		// var role = "<?php //echo $_SESSION['role']; ?>";
		// if(role=="Admin"){
		// }else if(role=="Patient"){
		// 	$(".Categories").remove();
		// }else if(role=="Expert"){
			
		// 	$(".Categories").remove();			
			
		// 	$(".Experts").remove();			
		// }else if(role=="Marchant"){
		// 	$(".Categories").remove();
		// }

	</script>
	<?php } ?>