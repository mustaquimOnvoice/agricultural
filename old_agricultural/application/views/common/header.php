<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title> Biodhan </title>
<!-- Auther :  Unitglo solutions pvt ltd -->
<meta name="description" content="">
<meta name="auther" content="Unitglo solutions pvt ltd">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<!-- Bootstrap CSS-->
<link rel="stylesheet"
	href="<?php echo base_url('components/')?>/css/bootstrap.min.css">
<!-- Google fonts - Roboto -->
<link
	href="<?php echo base_url("components"); ?>/select2/select2.min.css"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="<?php echo base_url('components/')?>/css/googleapis.css?family=Poppins:300,400,700">
<!-- theme stylesheet-->
<link rel="stylesheet"
	href="<?php echo base_url('components/')?>/css/style.red.css"
	id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet"
	href="<?php echo base_url('components/')?>/css/custom.css">

<!-- For localost only  -->
<link rel="stylesheet"
	href="<?php echo base_url('components/')?>/css/99347ac47f.css">

<!-- Favicon-->
<link rel="shortcut icon"
	href="<?php //echo base_url('components')?>/img/unitglo-mpyc-logo.png">
<!-- Font Awesome CDN-->
<!-- you can replace it by local Font Awesome-->
<!-- Font Icons CSS-->
<link rel="stylesheet"
	href="<?php echo base_url('components/')?>/css/icons.css">
<!-- Tweaks for older IEs-->
<script src="<?php echo base_url('components/')?>/js/jquery.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/custom.js"> </script>
<script async src="//"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-5571657337168398",
    enable_page_level_ads: true
  });
  
  var base_url='<?php echo base_url();?>'
</script>

<script type="text/javascript">
//var document_base_url = "<?php echo base_url(); ?>"
</script>
</head>
<style>
span.selection {
	display: block;
}

.select2-container .select2-selection--single {
	height: 35px;
}

.select2-dropdown.select2-dropdown--below {
	top: -24px;
}

.select2-container--default .select2-selection--single .select2-selection__rendered
	{
	line-height: 30px;
}

.alert {
	position: fixed;
	right: 20px;
	top: 20px;
	height: 45px;
	z-index: 10000;
}
</style>
<body>
	<div id="alert-pops-shadow"></div>
