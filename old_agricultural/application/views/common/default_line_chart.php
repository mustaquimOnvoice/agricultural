<script src="<?php echo base_url("/assets/js/Chart.min.js")?>"></script>
<?php $random_chart_id = (isset($chart_id))?$chart_id:random_string('alpha',10)?>
<canvas class="chartjs-render-monitor" <?=$chart_attr ?> id="<?=$random_chart_id ?>"></canvas>
<script>
var ctx_<?=$random_chart_id ?> = document.getElementById("<?=$random_chart_id ?>").getContext('2d');
var <?=$random_chart_id ?> = new Chart(ctx_<?=$random_chart_id ?>, {
	<?=(isset($type) && $type!='')?"type: '".$type."',":""?>
	data: {
        labels: [<?=isset($dataLabels)?$dataLabels:"" ?>],
        datasets: [
        <?php if (isset ( $chartData )) foreach ( $chartData as $key => $val ) {if ($key > 0) {echo ",";} ?>{
            <?=(isset($val['type']) && $val['type']!='')?"type: '".$val['type']."',":""?>
        	label: '<?=$val['setLabel'] ?>',
            <?=isset($dottedLine)&&$dottedLine?"borderDash : [5,5],":""?>
            data: [<?=$val['dataValues'] ?>],
            backgroundColor : <?=(is_array($val['backgroundColor'])?('["'.implode('","', $val['backgroundColor']).'"]'):($val['backgroundColor']))?>,
            borderColor: <?=(is_array($val['borderColor'])?('["'.implode('","', $val['borderColor']).'"]'):($val['borderColor']))?>,
            fill : <?=isset($val['fill'])?(is_bool($val['fill'])?"true":'"'.$val['fill'].'"'):"false"; ?>,
            yAxisId : "<?=$random_chart_id."_".$key?>",
            <?=isset($val['stack'])&&$horizontalStackedBar&&$groupStackedBar?"stack : '".$val['stack']."',":""?>
            <?=isset($steppedLine)&&$steppedLine!=false?("steppedLine : '".$steppedLine."',"):""?>
            <?=isset($val['interpolation'])?(($val['interpolation']=='monotone')?"cubicInterpolationMode : 'monotone'":($val['interpolation']=='linear'?"lineTension : 0":"")):""?>
            }<?php }?>]
    },
    options: {
        responsive : true,
        stacked : false,
        maintainAspectRatio : true,
        spanGaps : false,
        title: {
			display: true,
			text: '<?=isset($chartTitle)?$chartTitle:""?>'
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		<?php if (!$displayTooltipSeperate){?>
		tooltips: {
			mode: 'index',
			intersect: false,
		},
		<?php }?>
		scales: {
			<?php if ($displayXAxis){?>
			xAxes: [{
				<?=isset($horizontalStackedBar)&&$horizontalStackedBar?"stacked : true,":""?>
				display: true,
				scaleLabel: {
					display: true,
					labelString : '<?=isset($xAxisLabel)?$xAxisLabel:""?>'
				},
				ticks : {
					autoSkip: true
				}
			}],
			<?php }?>
			<?php if ($displayYAxis){?>
			yAxes: [{
				<?=isset($horizontalStackedBar)&&$horizontalStackedBar?"stacked : true,":""?>
				id: '<?=$random_chart_id."_0"?>',
				display: true,
				scaleLabel: {
					display: true,
					labelString: '<?=isset($yAxisLabel)?$yAxisLabel:""?>'
				},
                ticks: {
                    beginAtZero:<?=isset($beginAtZero)&&$beginAtZero?"true":"false"?>
                }				
			}]
			<?php }?>
		}
	}
});
function addData(chart, label, data,offset=false) {
    console.log(chart.data.datasets);
	if(offset){
		// START
		chart.data.labels.splice(0,0,label);
		chart.data.datasets.forEach((dataset,index) => {
			dataset.data.splice(0,0,data);
		});
	}else{
		// END
		chart.data.labels.push(label);
		chart.data.datasets.forEach((dataset) => {
			dataset.data.push(data);
		});
	}
	chart.update();
}
function removeData(chart,offset=false) {
	if(offset){
		// START
		chart.data.labels.splice(0,1);
		chart.data.datasets.forEach((dataset) => {
			dataset.data.splice(0,1);
		});
	}else{
		// END
		chart.data.labels.pop();
		chart.data.datasets.forEach((dataset) => {
			dataset.data.pop();
		});
	}
	chart.update();
}
function getChartData(){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var result = JSON.parse(this.responseText);
			console.log(result);
			if(result.operation=='update'){
				removeData(myChart_<?=$random_chart_id ?>);
			    addData(myChart_<?=$random_chart_id ?>,result.label,result.value,true);
			}
		}
	};
	xhttp.open("POST", arguments[0].getAttribute('form-data-url'), true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	var data_to_send = '';
	if(typeof arguments[0].getAttribute('form-data-val')==='string'){
		data_to_send += 'data='+arguments[0].getAttribute('form-data-val');
	}
	if(typeof arguments[0].getAttribute('form-data-id')==='string'){
		data_to_send += serialize(arguments[0].getAttribute('form-data-id'));
	}
	xhttp.send(data_to_send);		  
}
</script>
<!-- 
,{
				type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
				display: true,
				position: 'right',
				id: '<?=""//$random_chart_id."_1"?>',

				// grid line settings
				gridLines: {
					drawOnChartArea: false, // only want the grid lines for one axis to show up
				},
			},{
				type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
				display: true,
				position: 'left',
				id: '<?=""//$random_chart_id."_2"?>',

				// grid line settings
				gridLines: {
					drawOnChartArea: false, // only want the grid lines for one axis to show up
				},
			}
 -->