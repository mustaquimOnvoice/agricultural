<footer class="main-footer" style="background: rgba(255, 63, 63, 0.9);">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6">
				<p><?php echo date("Y")?> <?php //echo "- ".date("Y")+1; ?> &copy;  All Rights Reserved.</p>
			</div>
			<div class="col-sm-6 text-right">
				<p>
				<!-- <a href="http://unitglo.com/web/"> -->
					Design & Developed By OneVoice Transmedia <a href="http://onevoicetransmedia.com/" <font color="skyblue"></font>
					<!-- <img height="35" width="3500" alt=""
						src="<?php //echo base_url('components/')?>/img/unitglo-dev.png"> -->
						</a>
				</p>
				<div class="copyrights text-center">
			
	</div>

				<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
			</div>
		</div>
	</div>
</footer>
</div>
</div>
<!-- Javascript files-->
<script src="<?php echo base_url('components/')?>/js/tether.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/jquery.cookie.js"> </script>
<script
	src="<?php echo base_url('components/')?>/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/front.js"></script>
<script
	src="<?php echo base_url('components/')?>/select2/select2.full.min.js"></script>
<script type="text/javascript">
$(".select2").select2();
</script>
</body>
</html>