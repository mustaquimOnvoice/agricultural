<form class="POST_AJAX"
									action="" method="post" id="from_add_district_admin" name="from_add_district_admin">
								
									<div class="form-group">
										<label class="form-label">First Name</label> <input
											class="form-control"  type="text"
											id="district_admin_first_name" name="district_admin_first_name" maxlength="60"
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="add_district_firstn_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Last Name</label> <input
											class="form-control"  type="text"
											id="district_admin_last_name" name="district_admin_last_name" maxlength="60" 
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="add_district_lastn_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Mobile</label> <input
											class="form-control"  type="text"
											id="district_admin_mobile" name="district_admin_mobile" onkeypress="return isNumberKey(event)" maxlength="10" 
											  />
											  <span class="small"><em>(Min 10 digits)</em></span>
											  <span class="add_district_mob_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Email</label> <input
											class="form-control"  type="text"
											id="district_admin_email" name="district_admin_email" maxlength="60" 
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="add_district_email_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Address</label> <textarea
											class="form-control"  type="text"
											id="district_admin_address" name="district_admin_address" maxlength="1000" style="resize:none;"
											  /></textarea>
											  <span class="small"><em>(Max 1000 characters)</em></span>
											  <span class="add_district_address_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Date of birth</label> <input
											class="form-control"  type="text"
											id="district_admin_dob" name="district_admin_dob" readonly style="width:29%"  
											  />
											  
											  <span class="add_district_dob_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Designation</label> <input
											class="form-control"  type="text"
											id="district_admin_designation" name="district_admin_designation" maxlength="60" 
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="add_district_designation_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Role</label> <input
											class="form-control"  type="text"
											id="district_admin_role" name="district_admin_role" maxlength="60" 
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="district_admin_role_error error_color"></span>
									</div>
									<div class="form-group">
										<label class="form-label">Assembly</label> <input
											class="form-control"  type="text"
											id="district_admin_assembly" name="district_admin_assembly" maxlength="60" 
											  />
											  <span class="small"><em>(Max 60 characters)</em></span>
											  <span class="district_admin_assembly_error error_color"></span>
									</div>
									<div class="form-group">
										<input type="hidden" value="" id="selected_admin_district" name="selected_admin_district">
										<label class="form-label">Select district</label> 
										<?php 
										$getAllDistrict=getAllDistrictData();
										?>
										
											
											<?php
											foreach($getAllDistrict as $getAllDistrictData){
												?>
												<input type="checkbox" value="<?php echo $getAllDistrictData['districtName']?>" onchange="get_selected_district_admin();" name="get_district_admin[]"><?php echo $getAllDistrictData['districtName']?>&nbsp;
												<?php
											}
											?>
												
                                                
                                            <?php ?>
                                            <span class="add_district_error error_color"></span>
										
									</div>

                                   
									 

 									
									

									
									<button value="save_html_data" type="submit" id="add_district_admin_btn" 
										class="btn btn-outline-primary">SAVE</button>
										<a  href="<?php echo base_url();?>department/district_head" class="btn btn-outline-primary">Cancel</a>
								</form>