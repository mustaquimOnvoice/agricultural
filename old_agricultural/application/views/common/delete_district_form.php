<div id="delete_district_data_modal" class="modal fade" role="dialog" style="position: absolute;">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form role="form" name="frm_district_delete" id="frm_district_delete" method="post">
            <div class="modal-content">
                <div class="modal-header" style="display: none">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
					<p>This record will be removed from all district head</p>
					<p>Are you sure want to delete this record?</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="del_district_submit" id="del_district_submit" class="btn btn-primary">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </form>
    </div>
</div>