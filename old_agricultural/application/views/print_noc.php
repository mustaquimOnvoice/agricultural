<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<div class="page">
<table style="width: 100%;">
<tr>
	<td><img src="<?=base_url("components/img/unitglo-nagar-app.jpeg")?>" width=120></td>
	<td>
<center><font size="5"><b>अहमदनगर महापालिका, अहमदनगर </b></font><br>
<font size="3">अनधिकृत बांधकाम नियंत्रण व अतिक्रमण निर्मुलन विभाग (मुख्यालय)</font><br>
<br>

<font size="3"><b>Web site : www.amc .gov.in  &nbsp;  Email : amc_anr@rediffmail.com </b></font>
<hr style="border-width: 3px;">

</center>
</td>
<td><img src="<?=base_url("components/img/unitglo-nagar-app.jpeg")?>" width=120 style="visibility: hidden;"></td>

</tr>
<tr>
<td></td>
<td><div style="float: left;">जा.क्र.:</div><div style="float: right;">दिनांक :   &nbsp; &nbsp; &nbsp; /  <?=date("m / Y")?>  </div></td>
<td></td>
</tr>
	</table>
	<table>
<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; </td>
				<td>
					<br>
					<br>
					<br>
					
						प्रति,<br>
					   श्री......................................<br>
					   अध्यक्ष :- <?=$mandal_adheksh?> <br>
					   अहमदनगर. <br>

			   <br>
			   <table>
			   	<tr>
			   		<td>विषय : </td>
			   		<td><u>गणेशोत्सव <?=date("Y")?> </u></td>
			   	</tr>
			   		<tr>
			   			<td></td>
			   			<td>सार्वजनिक जागेवर गणेशोत्सवा निमित्त तात्पुरता मंडप उभारण्यास परवानगी मिळणेबाबत </td>
			   		</tr>
			   		<tr>
			   			<td>संदर्भ :- </td>
			   			<td>१) आपला दि. <?=date("d/m/Y",strtotime($default_date))?> रोजीचा अर्ज</td>
			   		</tr>
			   		<tr>
				   		<td></td>
				   		<td>
				   			२) सहाय्यक धर्मादाय आयुक्त, अ.नगर यांचेकडील नोंदणी दाखला <br>
				   			३) शहर वाहतुक शाखा, अ.नगर यांचे कडील पत्र जा.क्र. 100/2018 दि. <?=date("d/m/Y")?> 
				   		</td>
					
					</tr>
				</table>

			      </td>     
			<td></td>

</tr>
<tr>
	<td></td>
	<td><p style="
    text-align: justify;
">महाशय,<br>&nbsp;&nbsp;
      वरील संदर्भीय विषयान्वये आपण इकडेस सार्वजनिक जागेवर गणेशोत्सवा निमित्त मंडप उभारण्यास परवनगी मागितलेली आहे. आपल्या विनंती नुसार <?=$area?> <?=$road?> अ.नगर या ठिकाणी दि. <?=$start_date?> ते <?=$end_date?>  या कालावधीत <?=$depth?> X <?=$width?> फुट या मोजमापाचे मंडपास खालील अटीस व शर्तीस अधिन राहून परवानगी देण्यात येत आहे. </p></td>
	<td></td>

</tr>


<tr>
	<td></td>
	<td>
		<br><br>
		<table>
				<tr><td valign="top">१)</td><td valign="top"> देण्यात आलेल्या परवानगी पेक्षा जास्त मोजमापाचा मंडप उभारल्यास कायदेशीर कारवाई करण्यात येईल.</td></tr>
				<tr><td valign="top">२)</td><td valign="top"> सार्वजनिक ठिकाणी मंडप उभारताना स्थानिक रहिवाश्यांना अडथळा व उपद्रव निर्माण होता कामा नये.</td></tr>
				<tr><td valign="top">३)</td><td valign="top"> सार्वजनिक रस्त्यावरील वाहतुकीस व नागरिकांना अडथळा होता काम नये.</td></tr>
				<tr><td valign="top">४)</td><td valign="top"> सार्वजनिक जागेवर मंडप उभारताना रस्त्याचा किमान 60% व त्यापेक्षा जास्त भाग वाहतुकीस खुला राहील याची खबरदारी घेण्यात यावी.</td></tr>
				<tr><td valign="top">५)</td><td valign="top"> सार्वजनिक रस्त्यावर मंडप उभारताना सदर रस्त्यावरून ऍम्ब्युलन्स, अग्निशमन बंब इत्यादी व्यवस्थितरित्या जाऊ शकेल इतका रस्ता खुला ठेवण्यात यावा व मंडपाच्या ठिकाणी अग्निशमन व्यवस्था ठेवणे बंधनकारक आहे.</td></tr>
				<tr><td valign="top">६)</td><td valign="top"> म.न.पा. च्या सार्वजनिक मालमत्तेचे नुकसान झाल्यास ते दंडात्मक कारवाई करून आपणाकडून वसूल करण्यात येईल.</td></tr>
				<tr><td valign="top">७)</td><td valign="top"> कुठल्याही प्रकारे सार्वजनिक शांतता भंग होता कामा नये.</td></tr>
				<tr><td valign="top">८)</td><td valign="top"> उत्सवाच्या काळात कुठल्याही प्रकारची जीवित व वित्तहानी झाल्यास त्यास आपणास जबाबदार धरून कायदेशीर कारवाई करण्यात येईल.</td></tr>
				<tr><td valign="top">९)</td><td valign="top"> सदरची परवानगी हि पोलीस खात्याकडील परवानगीस अधीन राहील.</td></tr>
				<tr><td valign="top">१०)</td><td valign="top"> मा. उच्च न्यायालय मुंबई येथील जनहित याचिका क्र. 155/2011 मधील व मा.शासन यांनी दिलेल्या आदेशाची काटेकोरपणे पालन करण्याची जबाबदारी आपणावर बंधनकारक राहील. तसेच सदर ठिकाणी कोणत्याही प्रकारचे फ्लेक्सबोर्ड, होर्डिंग्स लावता येणार नाहीत.</td></tr>
				<tr><td valign="top">११)</td><td valign="top"> देण्यात आलेल्या परवानगीचा तपशील मंडपाचे दर्शनी भागावर ठळकपणे लावण्यात यावा.</td></tr>
				<tr><td valign="top">१२)</td><td valign="top"> पारंपारिक निवडणूक मार्गावरील आपण उभारलेला मंडप, आरास, देखावे, पताका विद्युत रोषणाई, झेंडे रस्त्याचे बाजूचे मंडपाची झालर इत्यादीमुळे कोणताही अडथळा होता कामा नये.</td></tr>
				<tr><td valign="top">१३)</td><td valign="top"> सदर मंडपामध्ये व परिसरातील मंडपाचे आजूबाजूचे रस्ते छायाचित्रीकरण होईल अशा योग्यरितीने किमान चा सीसीवटीव्ही कॅमेरे बसवून ते अहोरात्र सुरु ठेवण्यात यावे.</td></tr>
				<tr><td valign="top">१४)</td><td valign="top"> वरीलपैकी कुठल्याही अटी व शर्तीचा भांग झाल्यास अथवा तक्रार प्राप्त झाल्यास दिलेला परवाना त्वरित रद्द करण्यात येईल.</td></tr>
			
	</table>
	</td>
	<td></td>

</tr>
<tr>
	<td></td>
	<td><br><div><br><p style="
    text-align: center;
    padding-left: 60%;
"><br>उपअभियंता,<br>
अनधिकृत बांधकाम नियंत्रण व <br>
अतिक्रमण विभाग मुख्यालय,<br>
अहमदनगर महानगरपालिका</p><div></td>
	<td></td>
</tr>
</table>

             <br><br><br>
<center>
	<table style="width: 100%;"  class="page2">
<tr>
	<td><img src="<?=base_url("components/img/unitglo-nagar-app.jpeg")?>" width=120></td>
	<td>
<center><font size="5"><b>अहमदनगर महापालिका, अहमदनगर </b></font><br>
<font size="3">अनधिकृत बांधकाम नियंत्रण व अतिक्रमण निर्मुलन विभाग (मुख्यालय)</font><br>
<br>

<font size="3"><b>Web site : www.amc .gov.in  &nbsp;  Email : amc_anr@rediffmail.com </b></font>
<hr style="border-width: 3px;">

</center>
</td>
<td><img src="<?=base_url("components/img/unitglo-nagar-app.jpeg")?>" width=120 style="visibility: hidden;"></td>

</tr>
<tr>
<td></td>
<td><div style="float: left;">जा.क्र.:</div><div style="float: right;">दिनांक :   &nbsp; &nbsp; &nbsp; /  <?=date("m / Y")?>  </div></td>
<td></td>
</tr>
	</table>
	<br><br><br><br>
<table >
    <tr><td><b>परवाना क्रमांक </b></td><td>जा. क. <?=$application_id?></td></tr>
    <tr><td><b>परवाना कालावधी </b></td><td><?=$start_date?> ते <?=$end_date?></td></tr>
    <tr><td><b>मंडपाचे मोजमाप </b></td><td><?=$depth?> X <?=$width?>  फूट </td></tr>
    <tr><td><b>गणेश मंडळाचे नाव  </b></td><td><?=$mandal_name?></td></tr>
    <tr><td><b>मंडळाचे अध्यक्ष  </b></td><td><?=$mandal_adheksh?></td></tr>
    <tr><td><b>मोबाईल क्रमांक </b></td><td><?=$user_mobile_no?></td></tr>
</table>
</center>
</div>
<style >
.page2 {
	 page-break-before: always;
	}
	@media print{    
    button
    {
        display: none !important;
    }
    .page {
    	visibility: visible !important;
	}
}
.page {
    visibility: collapse;
}

</style>
<style>
.button {
  padding: 15px 25px;
  font-size: 24px;
  text-align: center;
  cursor: pointer;
  outline: none;
  color: #fff;
  background-color: #4CAF50;
  border: none;
  border-radius: 15px;
  box-shadow: 0 9px #999;
}

.button:hover {background-color: #3e8e41}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}
</style>
<script type="text/javascript">
	
</script>
<button class="button" onclick="window.history.back();">Back</button>
<button class="button" onclick="window.print();">Print</button>

