<div class="page login-page">
	<div class="container d-flex align-items-center">
		<div class="form-holder has-shadow">
			<div class="row">
				<!-- Logo & Information Panel-->
				<div class="col-lg-6">
					<div class="info d-flex align-items-center">
						<div class="content">
							<div class="logo">
								<img src="<?=base_url("/components/img/unitglo-indian-youth.png")?>">
								<h1>MPYC - <?=date("Y")?></h1>
							</div>
							<p> </p>
						</div>
					</div>
				</div>
				<!-- Form Panel    -->
				<div class="col-lg-6 bg-white">
					<div class="form d-flex align-items-center">
						<div class="content">
							<form id="login-form" action="" method="post">
								<div class="form-group">
									<input id="login-username" type="text" name="loginUsername"
										required="" class="input-material"> <label
										for="login-username" class="label-material">User Name</label>
								</div>
								<div class="form-group">
									<input id="login-password" type="password" name="loginPassword"
										required="" class="input-material"> <label
										for="login-password" class="label-material">Password</label>
								</div>
								<!-- dashboard -->

								<button id="login-btn" data-url="./check_login" class="btn btn-primary">Login</button>
							
								<div id="login-btn-loding"></div>
							</form>
							<!-- <small>Do
								not have an account? </small><a href="register.html"
								class="signup">Signup</a> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyrights text-center"><!-- Auther :  Unitglo solutions pvt ltd -->
			OneVoice &copy; <?php echo date("Y")?> | Design &amp; Developed By <a
							href="http://onevoicetransmedia.com/" target="_blank"><b> OneVoice Transmedia </b></a>
	</div>
</div>
<script type="text/javascript">
$(function(){
	show_message();
});
</script>
<script src="<?php echo base_url('components/')?>/js/tether.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/jquery.cookie.js"> </script>
<script
	src="<?php echo base_url('components/')?>/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url('components/')?>/js/front.js"></script>
