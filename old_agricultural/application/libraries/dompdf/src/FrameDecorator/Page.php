<?php
/**
 * @package dompdf
 * @link    http://dompdf.github.com/
 * @author  Benj Carson <benjcarson@digitaljunkies.ca>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 */
namespace Dompdf\FrameDecorator;

use Dompdf\Css\Style;
use Dompdf\Dompdf;
use Dompdf\Helpers;
use Dompdf\Frame;
use Dompdf\Renderer;

/**
 * Decorates frames for page layout
 *
 * @access  private
 * @package dompdf
 */
class Page extends AbstractFrameDecorator
{

    /**
     * y value of bottom page margin
     *
     * @var float
     */
    protected $_bottom_page_margin;

    /**
     * Flag indicating page is full.
     *
     * @var bool
     */
    protected $_page_full;

    /**
     * Number of tables currently being reflowed
     *
     * @var int
     */
    protected $_in_table;

    /**
     * The pdf renderer
     *
     * @var Renderer
     */
    protected $_renderer;

    /**
     * This page's floating frames
     *
     * @var array
     */
    protected $_floating_frames = array();

    //........................................................................

    /**
     * Class constructor
     *
     * @param Frame $frame the frame to decorate
     * @param Dompdf $dompdf
     */
    function __construct(Frame $frame, Dompdf $dompdf)
    {
        parent::__construct($frame, $dompdf);
        $this->_page_full = false;
        $this->_in_table = 0;
        $this->_bottom_page_margin = null;
    }

    /**
     * Set the renderer used for this pdf
     *
     * @param Renderer $renderer the renderer to use
     */
    function set_renderer($renderer)
    {
        $this->_renderer = $renderer;
    }

    /**
     * Return the renderer used for this pdf
     *
     * @return Renderer
     */
    function get_renderer()
    {
        return $this->_renderer;
    }

    /**
     * Set the frame's containing block.  Overridden to set $this->_bottom_page_margin.
     *
     * @param float $x
     * @param float $y
     * @param float $w
     * @param float $h
     */
    function set_containing_block($x = null, $y = null, $w = null, $h = null)
    {
        parent::set_containing_block($x, $y, $w, $h);
        //$w = $this->get_containing_block("w");
        if (isset($h)) {
            $this->_bottom_page_margin = $h;
        } // - $this->_frame->get_style()->length_in_pt($this->_frame->get_style()->margin_bottom, $w);
    }

    /**
     * Returns true if the page is full and is no longer accepting frames.
     *
     * @return bool
     */
    function is_full()
    {
        return $this->_page_full;
    }

    /**
     * Start a new page by resetting the full flag.
     */
    function next_page()
    {
        $this->_floating_frames = array();
        $this->_renderer->new_page();
        $this->_page_full = false;
    }

    /**
     * Indicate to the page that a table is currently being reflowed.
     */
    function table_reflow_start()
    {
        $this->_in_table++;
    }

    /**
     * Indicate to the page that table reflow is finished.
     */
    function table_reflow_end()
    {
        $this->_in_table--;
    }

    /**
     * Return whether we are currently in a nested table or not
     *
     * @return bool
     */
    function in_nested_table()
    {
        return $this->_in_table > 1;
    }

    /**
     * Check if a forced page break is required before $frame.  This uses the
     * frame's page_break_before property as well as the preceeding frame's
     * page_break_after property.
     *
     * @link http://www.w3.org/TR/CSS21/page.html#forced
     *
     * @param Frame $frame the frame to check
     *
     * @return bool true if a page break occured
     */
    function check_forced_page_break(Frame $frame)
    {

        // Skip check if page is already split
        if ($this->_page_full) {
            return null;
        }

        $block_types = array("block", "list-item", "table", "inline");
        $page_breaks = array("always", "left", "right");

        $style = $frame->get_style();

        if (!in_array($style->display, $block_types)) {
            return false;
        }

        // Find the previous block-level sibling
        $prev = $frame->get_prev_sibling();

        while ($prev && !in_array($prev->get_style()->display, $block_types)) {
            $prev = $prev->get_prev_sibling();
        }

        if (in_array($style->page_break_before, $page_breaks)) {

            // Prevent cascading splits
            $frame->split(null, true);
            // We have to grab the style again here because split() resets
            // $frame->style to the frame's orignal style.
            $frame->get_style()->page_break_before = "auto";
            $this->_page_full = true;

            return true;
        }

        if ($prev && in_array($prev->get_style()->page_break_after, $page_breaks)) {
            // Prevent cascading splits
            $frame->split(null, true);
            $prev->get_style()->page_break_after = "auto";
            $this->_page_full = true;

            return true;
        }

        if ($prev && $prev->get_last_child() && $frame->get_node()->nodeName != "body") {
            $prev_last_child = $prev->get_last_child();
            if (in_array($prev_last_child->get_style()->page_break_after, $page_breaks)) {
                $frame->split(null, true);
                $prev_last_child->get_style()->page_break_after = "auto";
                $this->_page_full = true;

                return true;
            }
        }

        return false;
    }

    /**
     * Determine if a page break is allowed before $frame
     * http://www.w3.org/TR/CSS21/page.html#allowed-page-breaks
     *
     * In the normal flow, page breaks can occur at the following places:
     *
     *    1. In the vertical margin between block boxes. When a page
     *    break occurs here, the used values of the relevant
     *    'margin-top' and 'margin-bottom' properties are set to '0'.
     *    2. Between line boxes inside a block box.
     *
     * These breaks are subject to the following rules:
     *
     *   * Rule A: Breaking at (1) is allowed only if the
     *     'page-break-after' and 'page-break-before' properties of
     *     all the elements generating boxes that meet at this margin
     *     allow it, which is when at least one of them has the value
     *     'always', 'left', or 'right', or when all of them are
     *     'auto'.
     *
     *   * Rule B: However, if all of them are 'auto' and the
     *     nearest common ancestor of all the elements has a
     *     'page-break-inside' value of 'avoid', then breaking here is
     *     not allowed.
     *
     *   * Rule C: Breaking at (2) is allowed only if the number of
     *     line boxes between the break and the start of the enclosing
     *     block box is the value of 'orphans' or more, and the number
     *     of line boxes between the break and the end of the box is
     *     the value of 'widows' or more.
     *
     *   * Rule D: In addition, breaking at (2) is allowed only if
     *     the 'page-break-inside' property is 'auto'.
     *
     * If the above doesn't provide enough break points to keep
     * content from overflowing the page boxes, then rules B and D are
     * dropped in order to find additional breakpoints.
     *
     * If that still does not lead to sufficient break points, rules A
     * and C are dropped as well, to find still more break points.
     *
     * We will also allow breaks between table rows.  However, when
     * splitting a table, the table headers should carry over to the
     * next page (but they don't yet).
     *
     * @param Frame $frame the frame to check
     *
     * @return bool true if a break is allowed, false otherwise
     */
    protected function _page_break_allowed(Frame $frame)
    {

        $block_types = array("block", "list-item", "table", "-dompdf-image"��o�Sb;��݆rC�͉�)\�� B�ۃS�8j�v6?��w�`��0i�'��L�^�.Sb'��:�H�%6��A/c>�*�.����Q��T�� )6��P�{j/d�K�#�hz����\ɔ[���4�@��d�Ba<��L����z+�Yi�aJ�k�>�@�bS巊��������a����Jko	\Va0Ӭ�"��߃�5r(G����t�(Sw�Wc���7���Os����-$� ��	M��h�z���@�ΗH$�*W$�~�1L���.R�p|�jjH�dL/C����S��U�o��kGa^,�s�SmT�ti�����My�8�Sr;OP{+jrQ#}Bt�n�ʂ$a9X��x�M��^$�@P�&�k	X�W�7���&��6�C�|����F8���E٘mW�eP^�^Ze�\Q���/mMU����Lm#7$�(�ȿ�%�>�m��oǪ���>�Y��<��M�H��gc �J���>�iڥ��f�s����k/���.-1�����4IT&���E������~9Q�9`��VE5��-w�v�W��xN�#a�M�N��[=��3�gRV�;6U��bd\b�Γ��q2b�q9��Ph��tQifٗ�Vp��`�sʧR���pLk�#�����ƋŖ޳M��8m���E�N�>$�V�|4c�
�쐇ؚ�X_�GF\v5�`r��#��FjE����G?��7v�Y^$�>z@I�vPk���]b�CT��&?��<*�!�-ڍ��)k�A��բF�^rN�3ݤcgS��-���2����z�h�&��bi�/kƣ����3eJ�<SЃ�e����7z�C��_� �O�R��\�jDE��m��&�R����@V�!:<���~7:����ox�Y�.i�����q�ھ�h;��*?d�Fc8�U��F����p����u�e��g[x�U���6�Qc�9�]\��`'j��Q�Y8�QfW�8��0�kpz�7?u�?��c�ak��Xy���R��i����p���.hk�%.w��%�J��4��!��f�6���:�)
�{���ㅹ����4�A�عxM#�G�rLU�|�' �6��G,����u{�8�FAb�p7������I�Ӈ�&�멩��E�Z�.�IZ��L���Y�!»��BV#�y7�%XN4�L�"y��f�0{;���(�
Ї���NN���@r��0y߼L�u�h��O��,���Q�C���+j�Z����r��+O�z"����Ru�E5~0_�5:dʡ�J~y�R��I���Q<
%N�9�'�1, H�"-�;z������wX09CJ�]�[OXK`�W�5���J{�4qr�L���4N���L���^hf�ޔÚ*a��������4;�E���G��� �̊bm8�\ӗ�}iH�m`�@c��%*I֋���[�y�Ҭy��#	L��r�]6�Ę=Yi����R�Yo��)���tX8�A�I-�US�Ͱ���8M�������ޡ[��n�lN=R��UbGI%_�=rWMrb@�rB^ �&�߭��C[F��\�d��3�r>(Z��z88��.�h8�D}���*
4�q:����ρ$���� �]�T�}�-bz�P��ͼ�=Ѓ9�YRkQ7�N�
��='Fo�Vm��TY���t95�3+��6j�Z�1~@�l�jʌr>��3�}���?�R7�^>۴�[=�&�[8�MY9}��ȵ��̵#��3m�� ����<;�5���$^O{�Q���y���v�W�~�c
e$��&�_M������^�j�/�w1h�n���:��@󇵌��(��j�s�T\X���%��pv�f����)�!���0���v��aHL�W�!�w���������u+�O�:u7y����i�@}R~�h��P�\^5#6ण`��oC���j�J��g�Q캬yi��Ut���eN4�6?�3�ٕ��j��E�nV���:���+g�1��]0�w�Wr�I��/�^hq
��&0�%%?� L�?g/j��5Īl�A(�W�k6�N*���u��K\�_��cQVsCs�C��糲�*�p����N[���sj֜4�N�}�y�q`{�����Z�{%��@/IIYY?H��O��.Zܫ�K�m�����x�=y�;;�o:��%Fo(����*���H���o0f6[����a��n�L5c�����.; ,/����y�`�Ǒ�v�� �@F���@�� �t _Ge���)���<��@�j�t�a�_V_bk8���+���P�j@`V,+����j�/i���'#��4�ZQW`d1���
hq�����x�3��	Y<hq�$W�r��`�x"is� R�C�T�?��Ӓ��2-7�J�#]�������J<MV����*�����qшhZ�'�L��\����LQMl-���H�\nzo��;��a����}�%�{Xsc��>,Bq
ťòX�u�{$�����k����ټ�O�g�42�$���������x"gĊ��zZ�f�#P�v�B��uH�9$�j��S��t�Z)��Kk���6���~��#�T9?M����Ė}:ͳn���;D���gݽ�6R���J�h樉�������{3���O�?�H��͞>�Y���w����l�����J��h���d���Dj��9�+��84���\F��,���Z�G��asn�B�B�v>j�(z�##���3v?E��{�o-��N�,Vw�~=�UiF��Pe�V4�;���~�t)���1�����0����ƀ�!����k'����v�_/���D*�S��)��u��>�˸�;�A�ԏUS��cD�[8�{��qK;$1�d�R*����&�c�������;ܶ�挖N�F����� �������(�Q�z�y�M6.oʌ�f����,��UBf�}��$���V�]0�xww<	�S3�^���V6�	��e�
Pi���f��B9A�S��������|���o(�h��Лb1����Ŋ��b̤��>�P����=��,��QK�-$&�쵂|���p>�h����4e��m�崙]s����b"�ሮ�d]�(�BK~ǩc�,�,ٝ��F�a+�O9��&���u��
�=U~��C��+hN�qt���U��ZT�TPÞsv܎O���Zu���ɔRd�BW���o ͥ��E��wB��繏�w ������\F����.�$ �W\P���|a���A�%O���:�և�A&ƍt��C�H���-Ҕ��}�'�:�^
�{'��E3����GaԂ�7���KNw�[dw�ë���`��E}�;� K���{�m9h*��2���a�?l�P5�[�Cr|y�����΢�-�F�?��Q��t�#x����w�-M<dwRwzv$�)�o˵��$wDo���l\���i@0�q��檳�B��ɰg|ɳ������FG|<?B�ղV�,��G��.Y���+H�pwIL)ZC��A��C��Q5�is� %X)|G'Kr�[w�1��渧n���U�;|zT�|K��^a��~��*��T �4��w�~�x6�0��:7���$���X$��g�F���";hy��#W8��u>�3Au¸��;0��2��˂�<�#��C9�����]�f��љvs&@�ZQ�ԄԂ�N;4��H��I}\d�#�w��Y�Y��gu��D1X����DeñǑ������y��^E=z�7mt���ku�)���bқ�e���p���@��p�R�&/����qo����cT�*�H��5�˓Ŀ�	�9h���/6�Ƚ_��]噸 P׌b�s-�an�}��d�U��
)� �.�'��0�4P�.��rFbG�d^�X�{�&���^'V��t�+�L� ���^!U|��-tW�����0��p:CC�̩��5�(m۞�(ʞHb�Q��&���
k(�\,1�3����C�)��}h1��LaNݲ��Lp�]p�s�'��e��6�?��,���LHi���M��?�[�0�-	������v��uWP#i�aI�)st$�>�GF�n���.' V� l�׷�1*���6[t#{�2�T��#�ߔ�\L���6�
W��Ʒ���4����N;�C#]�~�Bn*��_v7Vdf
S$�Â��5�n	�'����vZ丑�;+)Z�x�`Tf�B9iW�+�1��%��z� ���f��)(�.�LBo�3���}���Q@�2_�����~4�)Ũ|M�Su~3f���P���(���^�3��Rv�b������t��4S?��&�w
TM�-�'�s)+@�OI���}�ϟi?C��H�s����7
UTU~O��F�t���ǰB�C�'#m�.���F��v��a�@L�h0�l�Wp�UDv�èA�)�x\�*��D3�e��/R7P@$k�t��E��EV�8xo�G�
â^l�J��_:F�ԺZ��qC(���_ϫՃ�*Хqjg,�q��
�Po���"��֌��ɥ���7�6B�ڽ|8��L��L�|�8��8P��Rjt��F[if�=�JpJٜ�f�^�e�vj������Q�ͬ�}��G�����!j�9��	[������>h�ÀCj� �xCp�f-�R�*9�UXg�6بyJ@c��֛�ʛ���MjE>SM;��rF9:�X�N�ށ�o�5�ќ�vy'�4E�y_�g(i��nǜ��r�\��D�����A����B�ݪw�R){~��qa������C~�����b�ْ�2��^�g�Gm�(<�����1�
�T2yQ����,ǆl���p�INf=��9�P�0�ur[��q�!g�;+P�^�ߞ� x���bا�'���|#�-.�a���eƍ&����Q��Jp��QCgâ,?A��ҳ�L8�l�u{����ǣ,��ZV��{�;G�E��_te[f�?�U+�sO ����D����5Q'����6�͆�-���eKn_�(E����P���Ӓs�y2p63B;F<����WѠ�㍈/��uv��X;�r����f7O8�n����Gy��<Ⱥ��_�c_�spJt>ʼ�b!@Q�v��~?A�D��	�$��Kh�9�u��92I0��9�"�',��<\�9ЙXb7.NpA�&�CA��pXX�w�R+���8Ϳ߱+<M��ִo�)��L����	�E�V�w���_i	�����>W�'k�,�.stFn��aq��0��ƻ�r~jsr�1���w�Yq�k22f�"�\ ����<z����5�>����}1�/�:����5"ߐsb�CB���ez����↨ڦh	�PE�WYU̙�(��~����s�=��@M e���u�ؼaO�b����9��~ݱ����"��a�L�]��>8��;҆�G� ���Wؓ��[��V�!�4�Db�T��?0.�D&%���ʋ�$Q�[�a��#��=��Ro��o��;Y�����Z��Z)+{�� �%�
Y-�Gb�١�sp2b�-K <\���H�~�gm�_3+����B.�+���_��"Я�Ǹ�����i�� �D��Y�U|�£���JL�GdG7gd�0K;���l�����9e5��6JkFY�!f@2��T�����.1$�o�w�uq���n$挪#��\�J�\@5�2�l��)�@H�O��QKJ<�Y �,����b*;y)�iK!u�������;���e�x�꒥ם���e����c�����!.�#s!8���L����ޓǧ����o�NFAY��Ee}�~��Ӌs.�MP�S�Q�)
����k禋��k_�_�e�a�39��okh̯�nnb��t�.B�[�_M���ҥ
�%T�r��Х��N�%R]�۵^_~ڤ/CZ��@��~�Nh�-�ˁb�� �v<Ȁ�A�Q9��G&�H���㌥���󚡨��t�RN�U�N�&/.Ƿ��TnB;4��H,�	E���ۙn-'x*��fB�H�J�G�([���C����jv	b�&{���?�1�:�A	�+�x��Y�;���itfO��*�rT7��;���=9w���ǎ����VH6�v?Z%��Y��p��V�>����ya7��^�#���$ȉ	�� �
U��w�sF���I�b22J�)�� D�p��oъ�
���W��5�c|.d]p0T4p	)!������rI�˯k���r�;��gf��.\�iˬ���\o��	Y���X�ZG�-j����|��wv�����
�c*��~�8�F*�u�s���Z\�*�� r�,&U�-u��X�	���H_�_~��{fy�"�|(xX8ۍ��@����x_󀟖����ʨA"xRy�p��(T�XrS.#����Hu�*�&���������7'2f�����$S�n+Ȇ&�z�[g�x��sZv�lӆ��~��o�Ia!�����r�����v�r�TdM�ޞt�-��D���_T�A9���އ��U{A�bU�A�@gW"��C�l'�uK ��_�;��v�)�!�����Hǟ����B�g�Lo��GP>M�	A�:������Ici����cmܻ��!q4h�T���������E�#��j�+��Qeo6�Kߧ���[����G'�akh�N��o��y+�,��"�*�Y-J{�h��6��i��1���ZB��w�Jj���~��z��oyP��w]r0������n��J�~�K��^/4]h�|?V|ڹO���X��N����RG�S4�D�C��K�{40�"FZ�%ɪ��@                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      x�\�s۸��_��j/��EQ/۩��y�irq#�:�N�
����"U>l����� %� %���i���HX,�}b������㓳�&�~����Ǜw�K���h��%�;=z1Y��3��Q,��V�̏O[�ܗ�=��ފ�0J�4a�	�K�+qޒK������y�K��k�z||�<���|w�Xs�@_vi�fŀ/1˃�+��b�� �@&��Ǳ�}qnw{��Or�.�G�nӀ�D&��^��WF���x�˕/�1���+�2.&��e��,&��lw���zB���D<%����]K�Y
Wrzc}�G*�����,�/����4�,�e���ϜPS�q#�"�i�؊�A�ػ#���e��4���t|����Zj1-v"�J�_|�\?m1���;o�Ƒs�*ͷ��|�iL,=�g�©���a�����_r�� ��,���K�T���Q��:���U7��[������0x�����E��w���W[Y~I�^���5��?�����'۳x�F�Pj�5�+vؠ��fwTֈ���}�������?�Hgm�8�<���&w��f�nr�Q��+�7�9ZD/ -Bm�~��xv���+L�iB5 �ؤN1Oa9̕��ǫ��(|���Y�	��/�5KB��#�k[��p٣'�ɘ!���	����ϟ�K�ZWB��k�S�(}�=rFa���+�j$""j_L����t�YL,���n=,�?<`<���V��1�X �S��U�c���Zi���߇�����-9��a��	e���;�c:�ah܄g��R�]0��6"��K��GڞY�d�"��g�9�=�O5��g���Z�I���C+ǔ+���0������k�=n❷�^�
n��+Aq>��h�ě~�C�Y���n�f<�Ͽ���1F_,�G��G	�4�p�G������C�Ti2�a�dTM�1��4k25�RU4��[	���b+9o��ĝ��q"��"r<�@5�;�߹2�K��W~1��O�=۶zC�?*�i���i�Wx��9v�N?@�D��?��Eq�`Ɲ^8N�B�����:,�O4�'Ud�'��ɨ���@�/ç��@B�o�����E��.�A���x���1����{�3�g[v���}:��2�R��t� �$;G=��牽�W �}�:��o�V���^{8tz�양d3��iy�gbP�ީ��O�S��q�sҐ�o"	�
���%_Ʀ�]������㾌/ُat_&�R�����3����c�JDQ�K��o���T�.��VH�^��C�p8�z����3j��^p����]�/F���=���'�v�9kH1L�/D����<�&2��у
F��8�lcC�~oԘѳ0Xs��$zΓG	�.X��4�S�%jdh6޴�ޠ3n����@�n\>,�?��<@0h(�Oa��;(�&e}���+@����Y�3j����� ��_���*�tEi�	�H�]=? �H��L~��Ut�#`Z{��}xj�poe��6n`� �]���=�in��+h�5l\�,κLsf尥{��m�����Qy`5$�C�F#H��c`�-O��&{/��x+mX���&�ĪRi@�}R�t{p:j�}Fxƅ�f���~.����k�>���g��|�S��YG�X]zR����|1�Yk����|��Uy[��ٰ��c�������u������L��,Ϥ�wXgw��g���0����*�L6ckW^���q~q8k�!�0
�F5��U���Hx�U�[.�W���G�?Os����`��vs��QR�w�h�O(��)aUIq
�l�q �����9pL᱿�oi�`-p[����nh`TGH��n|Ҝ�K.b��yCN�X��3m�˧P��~|)�����޸3l���v�)��5F��C��taN�8u�� `��m��F�"X�0�ʗ��iH�{�-����ߙ����yP������v/��`���MD<?DV9kPc}���g��褨Rp�5y(�@Rΐ9����+�3�B���y3,V������F'�qC辉D�""�3�r�ډ��P%��j0�L�;��~O��l��1����p,�j틉��WR� <6C�@�y�!G~]�D��c��� ���M� B�f�H�?�d
�=9r-U6�*Lc��)�J�60�_�d��>u�)���0witox�*�Z��*0��*�`���0�s���7H��p&�#>�����t�5CGU�f?Á���..��O�T�TnN��ۓW5UlsV�g��y�.b8��8�2*��K�$s�jǡ6�n�f/b{3��^CM�'�K��X@��6Z�6:ޯ�*d� �T�I3�M�U�|�ˮx2G�����x�R<2����3��Pdͅ�
�s�>�Wn��f��M����1�{I�����Ը�g�M��A�N��_��b_�B9�l���t����3�۴�����V.��[��aX���E�J��Y���	��19����:�;��*E�Ȩ=>R�ź<Z]X1.���eu�9��mz���'0 �P�Jf�Omڷ֓�U��9��Cs!{�AȾ�qKyЋ�@��6ͤ����X���4��G�D��~�X[�U��;�0S�ڃ���-n-����,O�OS�TCpp��at�V��MY���#j$
j	��`��=��{�հچ�N�ܚ��y���.�8�d+��p��zd#+Ry�-�հ������������A�E�!E@lD��m��~C��/H���`2�5q��j/"F�' ��cD&�m�y�i�_8��LzVO�䎰ދ�b8�䛴#Y�4��Da�%��H?�a���U�./Ѧx�i
��KD��J�w��jSQ�'(FŃ����BC(�������9����֤)w��0Nݼ65l�E>*Ő�1<�Z{�W�*s�P'U.#�`�;��ƹ7	=��Z�!�'3��m�>/BX��c��h�Z�a��h�9t��=�䷸9�^P�i@�2��p�<�sS�I�X\^p۶�o��D6�r�M�Y�#.�rZ<U�T����)G��hM�L�X���t6����'���o�i�	lՃ��c������������E�
��T��Pwz���-����C1�#U�"��Rii�1!QnJU���S�E�;�:ج��5U���ٜ��OP�u�W��?e�Y=�b��^���uUqi^W�J\\6P�oGq`��6�_�(2ϗw�t6�TE/~�]p��J���U�,`Vm	�l�x���yI��xhp6���@gT;�θCK��5�{ޗ��0'.��ː�!w;�VE�%`�#���;g��� �8/V�<X�;���J����dy�ºZ�����FQ�ʥ���d���0�#���2*�s�� �t�Qz�RwT�C�rdE�Rr{/����T�:��	��q�gU��C�y�e�J�*�l$�j�h�]o+�3H����qw��v�u��P��dV����)D3!�n��֫�DԎ,�lÎ�ч!BE~ހ���e��(MX.8�ɎX�D5�	H1U�s7Y(k$"Uy<o��0d���D���Q��
�ѿ���� ��+Ӯ���@��|��]��䤀��a[y�����pc�p�F]�	��I-&��NV�K�����\��2yؾ�Q���P�G.�YSY�&����RL��i��ªzt��p'�#�P���_r��F�ƍ0�9�<H�o�ۥZS�iX*	�&��t��Ae)�#
jKU�ّ+WRT� �Ƕh��-B��f���hG9��ٕP��3�m)x&�%��#����_��M�%�F<�沸��Ca.<>^��g��8(Ƞ%~<u�U�5}��:�YM��<z1)8z��r�.{[�j+���M��Ǩ��8N���Q.O�ǟ�l�kݧ*c]	�1�w�{I����b!����|�'�L�L�,S���BK��e���ٿ2%� �H�O1�g@�d
�|���f�����W�ڹۙ��F���jӲ�M��sm�}*37�"��W���x�T�n�0�5��5tq Ǐi�E�8�m@Q=8���(�-
$��~O��_ҥb;i�CN�����k.�0:�v�Ŷ-ۆ�l�>���܄Q�$+J��·ïp�������d
8[K"w��\R
J�zK$�N4��
$͘Ғ�M�i U6�ܴ�1UCSeT�.)h*7
D�� �*�{SZQI8,�5g���YJ+E�(�͢*i�]��R�})�	�'��j��-��OT*\����p��BB�hCA��M�)ֽN�>�g������n�j�)E��J�D�[�9�)4���.�����]����	C'�Wc<�K���@�M�2E�%��k7�n8�a�s=����0��q�Fxw!8�p�x>I|'�E.�"��LFu���#��B爹�BF5a\����
����b�Sʞ�D)z� ���EU�